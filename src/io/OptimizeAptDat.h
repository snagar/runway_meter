#pragma once
#ifndef OPTIMIZEAPTDAT_H_
#define OPTIMIZEAPTDAT_H_

#include <cstdio>
#include <iostream>
#include <fstream>
#include <vector>
#include <list>
#include <string>


#ifdef IBM
#include "dirent.vs.h" // directory header
#else
#include "dirent.h" // directory header
#endif

#include "../core/thread/base_thread.hpp"
#include "../core/rw_data_manager.h"

using namespace rwmeter;
using namespace std;


namespace rwmeter
{
  class OptimizeAptDat : public base_thread
  {
  private:

  public:
    OptimizeAptDat();
    virtual ~OptimizeAptDat();

    static void setAptDataFolders(std::string inCustomScenery, std::string inDefaultAptDatFolder, std::string inMissionxFolder);

    ///// APT DAT Thread /////
    static thread thread_ref;
    static thread_state aptState;
    // members
    const void exec_optimize_aptdat_thread();
    const bool read_and_parse_all_apt_dat_files(thread_state * inThreadState);
    bool parse_aptdat(thread_state *inThreadState, std::string &relative_apt_dat_path);


    void stop_plugin();
    //// End Thread AptData ////////

      ///// indexing files
    //static std::map<std::string, int> mapIndexNavAids; // ICAO line in customApt file

  };


}

#endif
