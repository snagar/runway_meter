#include "OptimizeAptDat.h"

#include <fstream>
#include <chrono>
#include <cctype>

namespace rwmeter
{

  base_thread::thread_state OptimizeAptDat::aptState;
  thread OptimizeAptDat::thread_ref;
  //std::map<std::string, int> rwmeter::OptimizeAptDat::mapIndexNavAids;
}



OptimizeAptDat::OptimizeAptDat()
{
}


OptimizeAptDat::~OptimizeAptDat()
{
}

void rwmeter::OptimizeAptDat::setAptDataFolders(std::string inCustomScenery, std::string inDefaultAptDatFolder, std::string inMissionxFolder)
{
  Utils::addElementToMap(OptimizeAptDat::aptState.mapValues, rwmeter::FLD_CUSTOM_SCENERY_FOLDER_PATH, inCustomScenery);
  Utils::addElementToMap(OptimizeAptDat::aptState.mapValues, rwmeter::FLD_DEFAULT_APTDATA_PATH, inDefaultAptDatFolder); /// Resources/default scenery/default apt dat/Earth nav data/
  Utils::addElementToMap(OptimizeAptDat::aptState.mapValues, rwmeter::PROP_MISSIONX_PATH, inMissionxFolder);
}

/***************************************************/

const void rwmeter::OptimizeAptDat::exec_optimize_aptdat_thread()
{

  if (rwmeter::OptimizeAptDat::aptState.is_active)
    return;

  //  // start thread
  if (!OptimizeAptDat::aptState.is_active)
  {
    //    OptimizeAptDat::aptState.init(); // we should not init before starting thread since we are setting folders before calling the thread
    if (OptimizeAptDat::thread_ref.joinable()) // "join" previous thread before creating new thread. This should be very fast since the threaded function must have finished before reaching this line.
      OptimizeAptDat::thread_ref.join(); // joining also solved our issue with crashing xplane. error: abort() was called from "win.xpl"

    //missionx::OptimizeAptDat::mapIndexNavAids.clear();
    rwmeter::rw_data_manager::mapIndexNavAids.clear();
    rwmeter::rw_data_manager::cachedNavInfo_map.clear();

    OptimizeAptDat::thread_ref = std::thread(&OptimizeAptDat::read_and_parse_all_apt_dat_files, this, &OptimizeAptDat::aptState);
  }

}

/***************************************************/

const bool rwmeter::OptimizeAptDat::read_and_parse_all_apt_dat_files(thread_state *inThreadState)
{
  double duration = 0.0;
  unsigned int lineCounter = 0; // count the lines written to file
  auto startThreadClock = chrono::steady_clock::now();

  inThreadState->is_active = true;
  inThreadState->thread_done_work = false;
  inThreadState->abort_thread = false;

  inThreadState->startThreadStopper();


  // open scenery file
  // point to a folder
  DIR *parentDir = nullptr;
  std::string customScenery_folder = inThreadState->mapValues[FLD_CUSTOM_SCENERY_FOLDER_PATH];
  std::string default_apt_dat_folder = inThreadState->mapValues[FLD_DEFAULT_APTDATA_PATH];
  std::string plugin_folder = inThreadState->mapValues[PROP_MISSIONX_PATH];

  parentDir = opendir(customScenery_folder.c_str()); // "." is the same as current folder
  if (parentDir == nullptr) // if parentDir fail initialization
  {
    Log::logThreadMsg("[Parse Custom ini] Fail open folder information: " + customScenery_folder);
    return false; // skip
  }

  std::string customIniFile = customScenery_folder + rwmeter::FOLDER_SEPARATOR + rwmeter::XPLANE_SCENERY_INI_FILENAME; // v3.0.219.9
  std::string defaultAptDatFile = default_apt_dat_folder + rwmeter::FOLDER_SEPARATOR + "apt.dat"; // v3.0.219.12+
  std::string customAptDat = plugin_folder + rwmeter::FOLDER_SEPARATOR + "customAptDat.txt"; // v3.0.219.9
  std::string customIndexFile = plugin_folder + rwmeter::FOLDER_SEPARATOR + "customIndex.txt"; // v3.0.219.12

  std::ifstream infs;
  std::ofstream outCustAptdatFile;
  //std::ofstream outIndexFile;

  /// prepare file streams ///
  // prepare out file: customAptDat.txt file
  ios_base::sync_with_stdio(false); // v3.0.219.10
  cin.tie(nullptr); // v3.0.219.10

  outCustAptdatFile.open(customAptDat.c_str()); // can we create/open the file ?
  if (outCustAptdatFile.fail())
  {
    //XPLMDebugString((std::string("Fail to create file: ") + customAptDat + "\n").c_str());
    Log::logThreadMsg(std::string("Fail to create file: ") + customAptDat + "\n", Log::LOG_NO_CR);
    return false;
  }


  // prepare reading file: "custom_scenery.ini" file
  infs.open(customIniFile.c_str(), ios::in); // can we read the file
  if (!infs.is_open())
  {
    //XPLMDebugString((std::string("[parse Custom ini] Fail to open file: ") + customIniFile + "\n").c_str());
    Log::logThreadMsg(std::string("[parse Custom ini] Fail to open file: ") + customIniFile + "\n", Log::LOG_NO_CR);
    return false;
  }

  //// read each line and search for string "SCENERY_PACK ", the space in the end is important
  std::string line;
  inThreadState->counter = 1; // reset line counter before reading all apt.dat files
  while ((getline(infs, line)) && !(inThreadState->abort_thread))
  {

    if (line.find(rwmeter::SCENERY_PACK_.c_str()) == 0) // if line starts with "SCENERY_PACK " then should be valid
    {
      std::string relative_apt_dat_path = line.substr(rwmeter::SCENERY_PACK_.length()) + "/Earth nav data/apt.dat";

#if defined DEBUG || defined _DEBUG
      //XPLMDebugString((std::string("parsing: ") + relative_apt_dat_path + "\n").c_str());
      Log::logThreadMsg(std::string("parsing: ") + relative_apt_dat_path);
#endif
      auto start = chrono::steady_clock::now();
      /// PARSE FILE
      rwmeter::OptimizeAptDat::parse_aptdat(inThreadState, relative_apt_dat_path);

      auto end = chrono::steady_clock::now();
      auto diff = end - start;
      duration = chrono::duration <double, milli>(diff).count();
      Log::logThreadMsg("Duration: " + Utils::formatNumber<double>(duration, 3) + "ms (" + Utils::formatNumber<double>((duration / 1000), 3) + "sec), for: " + relative_apt_dat_path);
    }

  }

  /// close custom ini file
  if (infs.is_open())
    infs.close();


  // prepare reading Default "apt.dat" file
  infs.open(defaultAptDatFile.c_str(), ios::in); // can we read the file
  if (!infs.is_open())
  {
    //XPLMDebugString((std::string("[parse Custom ini] Fail to open file: ") + defaultAptDatFile + "\n").c_str());
    Log::logThreadMsg(std::string("[parse Custom ini] Fail to open file: ") + defaultAptDatFile);
    return false;
  }

  //// Anonymous Code Block
  {
#if defined DEBUG || defined _DEBUG
    //XPLMDebugString((std::string("parsing: ") + defaultAptDatFile + "\n").c_str());
    Log::logThreadMsg(std::string("parsing: ") + defaultAptDatFile);
#endif
    auto start = chrono::steady_clock::now();
    /// PARSE FILE
    rwmeter::OptimizeAptDat::parse_aptdat(inThreadState, defaultAptDatFile);

    auto end = chrono::steady_clock::now();
    auto diff = end - start;
    duration = chrono::duration <double, milli>(diff).count();
    Log::logThreadMsg("Duration: " + Utils::formatNumber<double>(duration, 3) + "ms (" + Utils::formatNumber<double>((duration / 1000), 3) + "sec), for: " + defaultAptDatFile);
  }


  ///// Flush to disk
  {
    auto start = chrono::steady_clock::now();
    for (auto airport : rwmeter::rw_data_manager::cachedNavInfo_map)
    {
      ++lineCounter;
      Utils::addElementToMap(rwmeter::rw_data_manager::mapIndexNavAids, airport.first, lineCounter);
      //outIndexFile << airport.first << lineCounter << "\n"; // should hold only Airports index lines
      for (auto nav : airport.second) // flush data into optimized apt.dat
      {

        outCustAptdatFile << nav;
        ++lineCounter;

      }

      outCustAptdatFile << '\n';
    }

    auto end = chrono::steady_clock::now();
    auto diff = end - start;
    duration = chrono::duration <double, milli>(diff).count();
    Log::logThreadMsg("\nFlush to Disk Duration: " + Utils::formatNumber<double>(duration, 3) + "ms (" + Utils::formatNumber<double>((duration / 1000), 3) + "sec)");
  }


  //// Close files
//  if (outIndexFile.is_open())
//    outIndexFile.close();

  if (outCustAptdatFile.is_open())
    outCustAptdatFile.close();

  if (infs.is_open())
    infs.close();

  auto endThreadClock = chrono::steady_clock::now();
  auto diff = endThreadClock - startThreadClock;
  duration = chrono::duration <double, milli>(diff).count();

  Log::logThreadMsg("*** Finish Parsing APT.DAT files. Duration: " +  Utils::formatNumber<double>(duration, 3) + "ms (" + Utils::formatNumber<double>((duration / 1000), 3) + "sec)  ****");

  inThreadState->is_active = false;
  inThreadState->thread_done_work = true; // we reset the thread at Mission::flc_aptdat() function

  return true;
}

/***************************************************/

bool rwmeter::OptimizeAptDat::parse_aptdat(thread_state * inThreadState, std::string & relative_apt_dat_path)
{
  std::ifstream file_aptDat;
  std::istringstream stringStream;

  ios_base::sync_with_stdio(false); // v3.0.219.10
  cin.tie(nullptr); // v3.0.219.10

  if (inThreadState->abort_thread)
    return false;

  // prepare reading file: "custom_scenery.ini" file
  file_aptDat.open(relative_apt_dat_path.c_str(), ios::in); // can we read the file
  if (!file_aptDat.is_open())
  {
    Log::logThreadMsg((std::string("[parse aptdat] Fail to open file: ") + relative_apt_dat_path ).c_str());

    //XPLMDebugString((std::string("[parse aptdat] Fail to open file: ") + relative_apt_dat_path + "\n").c_str());
    return false;
  }

  // read line by line and copy only lines with codes: if ($code eq "1" || $code eq "16" || $code eq "17" || $code eq "1300" || $code eq "100" || $code eq "101" || $code eq "102" || $code eq "56" )
  std::string line;
  char c = '\0'; // v3.0.219.10 optimizing file read
  std::string code, airportCode, word;



  /// Loop over lines

  bool flag_codeIsAirport = false; // code == 1
  int charCounter_i = 1, wordCounter_i = 1, maxWordsInLine = 0;
  std::vector <std::string> vec_splitLineAfterCode;
  rwmeter::mx_aptdat_meaning codeType = rwmeter::mx_aptdat_meaning::unknown;
  bool flag_prevCharWasSpace = false; // will help trim spaces

  bool flag_until_next_blank_line = false;
  bool flag_has1300_code = false;
  std::list<std::string> listNavInfo; listNavInfo.clear();
  std::string indexNavInfo; indexNavInfo.clear();

  while (file_aptDat.get(c) && !(inThreadState->abort_thread))
  {
  
    if (flag_until_next_blank_line && !(c == '\n'))
    {
      std::getline(file_aptDat, line); // skip line since it is not blank line
      continue;
    }
    else
      flag_until_next_blank_line = false;


    if (c == '\n')
    {
      if (flag_has1300_code)
      {
        if ( !indexNavInfo.empty() && !airportCode.empty() && !(listNavInfo.size() < 2) ) // skip Nav info if one of the parameters is empty. We should consider to flush everything at the end of the loop
        {
          Utils::addElementToMap(rwmeter::rw_data_manager::cachedNavInfo_map, airportCode, listNavInfo);
        }
#if defined DEBUG || defined _DEBUG
        else
        {
            //XPLMDebugString(std::string("\nSkipped: " + airportCode + ", list size: " + Utils::formatNumber<size_t>(listNavInfo.size()) + "\n").c_str());
            Log::logThreadMsg("\tSkipped: " + airportCode + ", list size: " + Utils::formatNumber<size_t>(listNavInfo.size())+ "\t", Log::LOG_NO_CR);
        }
#endif
      }

      // Reset info and wait for new 
      indexNavInfo.clear();
      airportCode.clear();
      listNavInfo.clear();
      flag_has1300_code = false;
    } // End handling "\n"

    if (!std::isdigit(c) && c != ' ' && c != '\n' && c != '\r')
      continue;
  
    if (charCounter_i == 1 && (c == '\n' || c == '\r')) // skip new lines
      continue;

    else if (c == ' ' || c == '\0' || file_aptDat.eof())
    {
      //// check code
      if (flag_codeIsAirport)
      {
        std::getline(file_aptDat, line);
        vec_splitLineAfterCode = mxUtils::split(mxUtils::trim(line), ' ');
        if (vec_splitLineAfterCode.size() > 3)
          airportCode = vec_splitLineAfterCode.at(3);
  
        if (!airportCode.empty() && Utils::isElementExists(rwmeter::rw_data_manager::cachedNavInfo_map, airportCode))
        {
#if defined DEBUG || defined _DEBUG
          //XPLMDebugString(std::string("dup: " + airportCode +", ").c_str());
          Log::logThreadMsg("dup: " + airportCode +", ", Log::LOG_NO_CR);
#endif
          flag_until_next_blank_line = true;
        }
          

        if (!flag_until_next_blank_line)
        {
          listNavInfo.push_back(code + rwmeter::SPACE + line + "\n");
          indexNavInfo = code + rwmeter::SPACE + line + " ~";

          //outFile << code << rwmeter::SPACE << line << "\n";
          //outIndexFile << code << rwmeter::SPACE << line << " ~" << inThreadState->counter << "\n"; // should hold only Airports index lines

          //Utils::addElementToMap(OptimizeAptDat::mapIndexNavAids, airportCode, inThreadState->counter);

          //++inThreadState->counter;
        }

      }
      else if (code.compare("16") == 0 || code.compare("17") == 0)
      {
        std::getline(file_aptDat, line);
        vec_splitLineAfterCode = mxUtils::split(mxUtils::trim(line), ' ');
        if (vec_splitLineAfterCode.size() > 3)
          airportCode = vec_splitLineAfterCode.at(3);

        if (!airportCode.empty() && Utils::isElementExists(rwmeter::rw_data_manager::cachedNavInfo_map, airportCode))
        {
          //XPLMDebugString(std::string("Skipping: " + airportCode).c_str());
          Log::logThreadMsg("Skipping: " + airportCode, Log::LOG_NO_CR);
          flag_until_next_blank_line = true;
        }

        if (!flag_until_next_blank_line)
        {
          listNavInfo.push_back(code + rwmeter::SPACE + line + "\n");
          indexNavInfo = code + rwmeter::SPACE + line + " ~";

          //outFile << code << rwmeter::SPACE << line << "\n";
          //outIndexFile << code << rwmeter::SPACE << line << " ~" << inThreadState->counter << "\n"; // should hold only Airports index lines

          //Utils::addElementToMap(OptimizeAptDat::mapIndexNavAids, airportCode, inThreadState->counter);

          //++inThreadState->counter;
        }

      }
      else if ((code.compare("1300") == 0))
      {
        // 1300  10.91196027  124.43800826 221.55 misc jets|turboprops|props|helos Cargo Ramp
        std::getline(file_aptDat, line);
        vec_splitLineAfterCode = mxUtils::split(mxUtils::trim(line), ' ');
  
        listNavInfo.push_back(code + rwmeter::SPACE + line + "\n");        
        flag_has1300_code = true;

        //outFile << code << rwmeter::SPACE << line << "\n";
        //++inThreadState->counter;
      }
      //else if (code.compare("100") == 0 || code.compare("101") == 0 || code.compare("102") == 0 /*|| code.compare("56") == 0*/)
      //{
      //  /**
      //  100 25.00 3 0 1.00 0 0 0 05R  50.12933714  014.52302572    0    0 0 0 0 0 23L  50.13387196  014.53174893    0    0 0 0 0 0
      //  102 H1  50.12860101  014.52351585 179.78 11.90 11.90 1 0 0 0.00 0
      //  101 91.44 1 08  21.31186721 -157.91575685 26  21.31191433 -157.90103531
      //  **/
      //  std::getline(file_aptDat, line);
      //  vec_splitLineAfterCode = mxUtils::split(mxUtils::trim(line), ' ');
  
  
      //  outFile << code << rwmeter::SPACE << line << "\n";
      //  ++inThreadState->counter;
      //}
      else
        std::getline(file_aptDat, line); // skip until end of line
  
      code.clear();
      flag_codeIsAirport = false;
      charCounter_i = 1;
    }
    else
    {
      if (charCounter_i == 1 && c == '1')
        flag_codeIsAirport = true;
      else if (flag_codeIsAirport && charCounter_i > 1)
        flag_codeIsAirport = false;
      else
        flag_codeIsAirport = false;
  
      code += c;
      ++charCounter_i;
    }

  } // end while

  if (file_aptDat.is_open())
    file_aptDat.close();


  return true;
}


void rwmeter::OptimizeAptDat::stop_plugin()
{
  OptimizeAptDat::aptState.abort_thread = true;
  if (OptimizeAptDat::thread_ref.joinable()) // "join" previous thread before creating new thread. This should be very fast since the threaded function must have finished before reaching this line.
    OptimizeAptDat::thread_ref.join(); // joining also solved our issue with crashing xplane. error: abort() was called from "win.xpl"

  //OptimizeAptDat::aptState.init();
}
