#include "writeLogThread.h"
#include "XPLMUtilities.h"

namespace rwmeter
{
  std::queue<std::string> writeLogThread::qLogMessages; // v3.0.217.8  
  std::queue<std::string> writeLogThread::qLogMessages_mainThread; // v3.0.221.4
  std::string writeLogThread::logFilePath;
}

rwmeter::writeLogThread::writeLogThread()
{
  
}


rwmeter::writeLogThread::~writeLogThread()
{
  //if (!qLogMessages.empty())
  //  qLogMessages.pop();
}

void rwmeter::writeLogThread::flc()
{

  // If thread work finished, we can join the thread
  if (tState.thread_done_work)
  {
    if (thread_ref.joinable()) // "join" previous thread before creating new thread. This should be very fast since the threaded function must have finished before reaching this line.
      thread_ref.join();

    tState.init();
  }
  else
  // aborting the thread does not mean to clean the messages from queue.
  if (tState.abort_thread )
  {
    if (thread_ref.joinable()) // "join" previous thread before creating new thread. This should be very fast since the threaded function must have finished before reaching this line.
      thread_ref.join();

    tState.init();
  }
  else
  // start thread
  if (!this->qLogMessages.empty() && !this->tState.is_active)
  {
    tState.init();
    if (thread_ref.joinable()) // "join" previous thread before creating new thread. This should be very fast since the threaded function must have finished before reaching this line.
      thread_ref.join(); // joining also solved our issue with crashing xplane. error: abort() was called from "win.xpl"

    tState.dataString = rwmeter::writeLogThread::logFilePath;
    thread_ref = std::thread(&writeLogThread::exec_thread, this, &tState);
  }
    

}

void rwmeter::writeLogThread::stop_plugin()
{
  tState.abort_thread = true;
  if (thread_ref.joinable()) // "join" previous thread before creating new thread. This should be very fast since the threaded function must have finished before reaching this line.
    thread_ref.join(); // joining also solved our issue with crashing xplane. error: abort() was called from "win.xpl"

  this->clear_all_messages();
}

void rwmeter::writeLogThread::init()
{
  tState.abort_thread = false;
  tState.init();
}

bool rwmeter::writeLogThread::exec_thread(thread_state *xxthread_state)
{
  static std::ofstream ofs;

  xxthread_state->is_active = true;
  xxthread_state->thread_done_work = false;
  xxthread_state->abort_thread = false;

#ifdef ENABLE_MISSIONX_LOG

  //bool successOpenFile = true;
  if (!tState.abort_thread && !(xxthread_state->dataString.empty()) )
  {
    ofs.open(xxthread_state->dataString.c_str(), std::ofstream::app); // can we create/open the file ?
    if (ofs.fail())
    {
      //XPLMDebugString((std::string("Fail to open file: ") + xxthread_state->dataString).c_str());
      rwmeter::writeLogThread::qLogMessages_mainThread.push( std::string("Fail to open file: ") + xxthread_state->dataString );
    }

  }
#endif

  auto msg = rwmeter::writeLogThread::qLogMessages.front();

  while (!tState.abort_thread && !rwmeter::writeLogThread::qLogMessages.empty() )
  {
    msg = rwmeter::writeLogThread::qLogMessages.front();

    // we write the messages to the qLogMessages_mainThread "deque" container so we will write them only durin main FLB
    if (!msg.empty())
      rwmeter::writeLogThread::qLogMessages_mainThread.push(msg);    

    if (!qLogMessages.empty())
      qLogMessages.pop();

#ifdef ENABLE_MISSIONX_LOG
    if (ofs.is_open()) // skip if file is empty or failed open the file
    {
      ///// Write to missionx.log /////

      // try to write
      if (ofs)
        ofs << msg;

      if (ofs.bad())
      {
        rwmeter::writeLogThread::qLogMessages_mainThread.push(std::string("Fail to write to log file: ") + logFilePath);
        break;
      }    
    } // end if file is open state
#endif

  } // end while loop

  if (ofs.is_open())
    ofs.close();


  xxthread_state->thread_done_work = true;

  return true;
}
