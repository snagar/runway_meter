/*
 * Log.cpp
 *
 *  Created on: Feb 19, 2012
 *      Author: snagar
 */

/**************

Updated: 24-nov-2012

Done: Nothing to change. New in NET. Not exists in 2.05


ToDo:


**************/
#include "Log.hpp"
using namespace rwmeter;

namespace rwmeter
{
  std::string Log::logFilePath;
  writeLogThread  Log::writeThread; // v3.0.217.8
}



rwmeter::Log::Log()
{
}

rwmeter::Log::~Log()
{
  // TODO Auto-generated destructor stub
}

/*  Snagar */
//void rwmeter::Log::logMsg(std::string message, bool isThread, rwmeter::format_type format, bool isHeader)
//{
//  //Log::logToFile(message, Log::LOG_INFO);
//  printToLog(message, isThread, format, isHeader);
//}
//
//void rwmeter::Log::logMsgNone(std::string message, bool isThread)
//{
//  printToLog(message, isThread, format_type::none);
//  //Log::logToFile(message, Log::LOG_ERROR);
//}
//
//void rwmeter::Log::logMsgNoneCR(std::string message, bool isThread)
//{
//  printToLog(message, isThread, format_type::none_cr);
//  //Log::logToFile(message, Log::LOG_ERROR);
//}
//
//void rwmeter::Log::logMsgErr(std::string message, bool isThread)
//{
//  printToLog(message, isThread, format_type::error);
//}
//
//void rwmeter::Log::logMsgWarn(std::string message, bool isThread)
//{
//  printToLog(message, isThread, format_type::warning);
//}
//
//void rwmeter::Log::logAttention(std::string message, bool isThread)
//{
//  printToLog(message, isThread, format_type::attention);
//}

void rwmeter::Log::logDebugBO(std::string message, bool isThread) // log debug in Build Only mode
{
#if defined DEBUG || defined _DEBUG
  printToLog( "[#debug] " + message, isThread);
#endif
}


void rwmeter::Log::logXPLMDebugString(std::string message)
{

  XPLMDebugString(message.c_str()); // not thread safe
}



// Simple formating of headers in Log
void rwmeter::Log::printHeaderToLog(std::string s, bool isThread, format_type format)
{
  printToLog(s, isThread, format, true);
}

// Simple formating for data in Log
void rwmeter::Log::printToLog(std::string s, bool isThread, format_type format, bool isHeader)
{
  Log::LOGMODE logMode = Log::LOG_INFO;

  if (isHeader)
  {
    if (format == format_type::header)
    {
      s = std::string("\n##############################################################\n") +
        "=====> " + s + "\n";
    }
    else if (format == format_type::footer)
    {
      s = std::string("\n<===== ") + s +
        "\n##############################################################\n";
    }
    else if (format == format_type::sub_element_lvl_1)
    {
      s = "\n\t==============> " + s + " <==============";
    }
    else if (format == format_type::sub_element_lvl_1)
    {
      s = "\n\t\t--------------> " + s + " <--------------";
    }
  } // end if header
  else
  {
    if (format == format_type::sub_element_lvl_1)
    {
      s = "\t" + s;
    }
    else if (format == format_type::sub_element_lvl_2)
    {
      s = "\t\t" + s;
    }
    else if (format == format_type::warning)
    {
      s = "[Warning] " + s;
    }
    else if (format == format_type::attention)
    {
      s = "!!!!!! " + s + " !!!!!!";
    }
    else if (format == format_type::none_cr)
    {
      logMode = Log::LOG_NO_CR;
    }
    else if (format == format_type::error)
    {
      s = "[ERROR] " + s;
    }
  }

  Log::logToFile(s, logMode, isThread); // none
}


void rwmeter::Log::logToFile(std::string msg, int mode, bool isThread)
{  
    std::string errStr;
    std::string out = "";

    switch (mode)
    {
    case LOG_NO_CR:
    {
      out = msg;
    }
    break;
    case LOG_INFO:
    {
      out = msg + "\n";
    }
    break;
    case LOG_ERROR:
    {
      out = "[ERROR]: " + msg + "\n";
    }
    break;
    case LOG_DEBUG:
    {
      out = "[DEBUG]: " + msg + "\n";
    }
    break;
    default:
      break;
    }

    if (isThread)
      Log::writeThread.add_message(out);
    else
      Log::logXPLMDebugString(out); // v3.0.221.15rc4 direct write to Log.txt

#ifdef ENABLE_MISSIONX_LOG
    //system_actions::write_missionx_log_file(logFilePath, out, errStr); // v3.0.217.8 DEPRECATE. Should move to write log thread
#endif

}

void rwmeter::Log::stop_mission()
{
  Log::writeThread.stop_plugin();
}
