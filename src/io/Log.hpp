/*
 * Log.h
 *
 *  Created on: Feb 19, 2012
 *      Author: snagar
 */


#ifndef LOG_H_
#define LOG_H_

#include <string>
#include <forward_list>
#include "../core/base_xp_include.h"
#include "system_actions.h"

//#include "../core/MxOptions.h"
#include "writeLogThread.h"

namespace rwmeter {
  
  typedef enum _format_type : uint8_t {
    none, // no formatting
    none_cr,
    header,
    footer,
    sub_element_lvl_1,
    sub_element_lvl_2,
    warning,
    attention,
    error
  } format_type;

class Log
{
private:
  static std::string logFilePath;
public:
  Log();
  virtual ~Log();

  static writeLogThread writeThread; // v3.0.217.8


  enum LOGMODE : uint8_t
  {
    LOG_NO_CR, LOG_INFO, LOG_DEBUG, LOG_ERROR
  };

  /*  Snagar */
  //static void logMsg(std::string message, bool isThread = false, rwmeter::format_type format = format_type::sub_element_lvl_1, bool isHeader = false);

  //static void logMsgNone(std::string message, bool isThread = false);

  //static void logMsgNoneCR(std::string message, bool isThread = false);

  //static void logMsgErr(std::string message, bool isThread = false);

  //static void logMsgWarn(std::string message, bool isThread = false);

  //static void logAttention(std::string message, bool isThread = false); // v3.0.219.10

  static void logDebugBO(std::string message, bool isThread = false); // v3.0.221.15 rc4 rares - Log in debug build only

  static void logXPLMDebugString(std::string message); // v3.0.221.9

  static void set_logFile(std::string inLogFilePath) //v3.0.217.8
  { 
    logFilePath = inLogFilePath; 
    rwmeter::writeLogThread::set_logFilePath(inLogFilePath);
  } 


  // Simple formating of headers in Log
  static void printHeaderToLog(std::string s, bool isThread = false, format_type format = format_type::header);

  // Simple formating for data in Log
  static void printToLog(std::string s, bool isThread = false, format_type format = format_type::sub_element_lvl_1, bool isHeader = false);


  static void logToFile(std::string msg, int mode, bool isThread = false);



  static void logThreadMsg(std::string message, int log_mode = LOG_INFO)
  {
    logToFile(message, log_mode, true);
  }

  static void logMsg ( std::string message, int log_mode = LOG_INFO, bool isThread = false )
  {
    logToFile( message, log_mode, isThread );
  }

  static void flc()
  {
    Log::writeThread.flc(); // manage the thread state
  }


  static void stop_mission();

};


} // end namespace
  
#endif /* LOG_H_ */

