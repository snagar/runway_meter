#include "system_actions.h"
#include <string.h>
#include <algorithm>
#include <vector>
#include "../logic/dataref_param.h"
using namespace std;

namespace missionx
{
  missionx::MxOptions missionx::system_actions::pluginOptions;
}

missionx::system_actions::system_actions()
{
}


missionx::system_actions::~system_actions()
{
}

/***
Copy file:
inSourceFilePath: obsolete file location + file name.
inTargetFilePath: obsolete destination file location + file name.
outError: Holds error message.
**/
bool missionx::system_actions::copy_file(std::string inSourceFilePath, std::string inTargetFilePath, std::string &outError)
{
  // https://gehrcke.de/2011/06/reading-files-in-c-using-ifstream-dealing-correctly-with-badbit-failbit-eofbit-and-perror/

  outError.clear();

  std::ifstream fin;
  fin.open(inSourceFilePath.c_str());
  if (fin.fail() || fin.bad())
  {
    outError = "[Error] Fail to open source file: \"" + inSourceFilePath + "\"";
    return false;
  }

  std::ofstream fout;
  fout.open(inTargetFilePath.c_str(), std::ofstream::out );
  // check open success
  if (fout.fail() || fout.bad())
  {
    outError = "[Error] Fail to find/create target file: [" + inTargetFilePath + "]. Skip operation !!!";
    return false;
  }

  // try to write
  if (fout)
  {
    std::string line; line.clear();

    while (std::getline(fin, line))
    {
      fout << line << std::endl;
      line.clear();
    }

    if (fin.bad())
    {
      Log::logMsgErr("error while reading file " + inSourceFilePath);
      return false;
    }


  } // end if fout is valid
  else
  {
    outError = "[Error]Output stream is not functioning as expected. Skipping copy command.";
    return false;
  }

  return true;
}

/*init_missionx_log_file: Reset the log file so we will write into a blank file. */
void missionx::system_actions::init_missionx_log_file(std::string inFileAndPath)
{
  std::ofstream ofs;
  ofs.open(inFileAndPath.c_str(), std::ofstream::out);

  ofs.close();

}

/*write_missionx_log_file: Write message to X-Plane log and to Mission-X log.*/
bool missionx::system_actions::write_missionx_log_file(std::string inFileAndPath, std::string &inMsg, std::string &outError)
{
  bool success = true;
  outError.clear();

  std::ofstream ofs;
  ofs.open(inFileAndPath.c_str(), std::ofstream::app);
  if (ofs.fail())
  {
    outError = "Fail to open file";
    return false;
  }

  // try to write
  if (ofs)
    ofs << inMsg;
  
  if (ofs.bad() )
  {
    success = false;
    outError = "Fail to write to logfile";
  }

  ofs.close();

  // Append to log file

  return success;
}

bool missionx::system_actions::save_datarefs_with_savepoint(std::string inFileAndPath, std::string inTargetFileAndPath, std::string & outError)
{
  // 1. open dataref
  // 2. read each line
  // 2.1 check if valid dataref (use Dataref class)
  outError.clear();
  bool success = true;
  unsigned int drefLineCounter = 0;
  unsigned int drefLineProccessed = 0;

  std::ifstream fin;
  fin.open(inFileAndPath.c_str());
  if (fin.fail() || fin.bad())
  {
    outError = "[Error] Fail to open source file: \"" + inFileAndPath + "\"";
    return false;
  }

  std::ofstream fout;
  fout.open(inTargetFileAndPath.c_str(), std::ofstream::out);
  // check open success
  if (fout.fail() || fout.bad())
  {
    outError = "[Error] Fail to find/create target file: [" + inTargetFileAndPath + "]. Skip operation !!!";
    return false;
  }

  std::vector<std::string> vecDrefLineSplit;
  // Read each line, check dataref and write to target file
  if (fout)
  {
    bool writeActionOK = true;
    std::string line; line.clear();
    std::vector <std::string> vecValidTypes = { { "int" },{ "float" },{ "double" } ,{ "int[" } ,{ "float[" } };

    std::vector <std::string> vecFilterCategories = { { "sim/aircraft/controls" },{ "sim/aircraft/gear" },{ "sim/aircraft/weight" } ,{ "sim/aircraft/specialcontrols" } ,{ "sim/cockpit/autopilot" },
    { "sim/cockpit/engine" },{ "sim/cockpit/radios" },{ "sim/cockpit/switches" } ,{ "sim/cockpit/weapons" } ,{ "sim/flightmodel/controls" },
    { "sim/flightmodel/cyclic" },{ "sim/flightmodel/failures" } ,{ "sim/flightmodel/forces" } ,{ "sim/flightmodel/misc" },
    { "sim/flightmodel/position" },{ "sim/flightmodel/transmissions" },{ "sim/flightmodel/weight" } ,{ "sim/operation/failures" } ,{ "sim/time" },
    { "sim/weather" }, { "sim/flightmodel2/controls" }, {"sim/flightmodel2/wing/"}
    }; // end vector initialization
    // { "sim/flightmodel/engine" } // DO NOT USE ENGINE
    while (writeActionOK)
    {
      // READ DATAREF KEY
      std::getline(fin, line);
      line = Utils::trim(line);

      drefLineCounter++;

      if (fin.eof()) // reached end of file
      {
        Log::logMsgNone("[Finish Filter Dataref]Lines Read: " + Utils::formatNumber<int>(drefLineCounter) + ", Processed: " + Utils::formatNumber<int>(drefLineProccessed));
        writeActionOK = false;
        //return true; // v3.0.160
      }
      else
      if (fin.good() )
      {

        // skip if empty
        if (line.empty())
          continue;

        // check if line starts with "s" "/sim/xxx"
        char firstChar = line.front();
        if ( firstChar != 's') 
          continue; // skip line

        // split line
        vecDrefLineSplit.clear();
        vecDrefLineSplit = Utils::splitString(line, "\t");

        if (vecDrefLineSplit.empty())
          continue;

        line = vecDrefLineSplit.at(0);
        bool foundKey = false;
        for (auto s : vecFilterCategories)
        {
          if (line.find(s) != std::string::npos)
          {
            foundKey = true;
            break;
          }
        }

        if (!foundKey)
          continue;

        // skip if line is not in Filtered categories
        //if (std::find(vecFilterCategories.begin(), vecFilterCategories.end(), line) == vecFilterCategories.end())
        //  continue;


        // check dataref
        bool isValid = true;

        std::string drefValue; drefValue.clear();
#ifdef MX_EXE
        if (vecDrefLineSplit.size() < 3)        
          continue;
                
        line = vecDrefLineSplit.at(0);
        std::string type = vecDrefLineSplit.at(1);
        std::string writable = vecDrefLineSplit.at(2);
        type = Utils::StringToLower(Utils::trim(type));
        writable = Utils::StringToLower(Utils::trim(writable));
        //std::string validTypes[5] = {"int","float","double","int[","float["};

        isValid = false;
        if (writable.compare("y") != 0)
          continue;

        for (auto s : vecValidTypes)
        {
          if (type.find(s) != std::string::npos)
          {            
            isValid = true;
            break;
          }
        }



#else // plugin code

        if (vecDrefLineSplit.size() < 3)
          continue;

        missionx::dataref_param dref;
        dref.key = vecDrefLineSplit.front(); // not using "setAndInitializeKey()" since it triggers the "initDatarefInfo()" function

        std::string type = vecDrefLineSplit.at(1);
        std::string writable = vecDrefLineSplit.at(2);

        isValid = dref.initDataRefInfo();
        if (isValid)
        {

          if ( writable.compare("y") != 0)
          {
            Log::logMsgNone("Dataref: [" + dref.key + "] is read only, darefFile shows: " + writable);
            continue;
          }


          // check if array and if designer defined specific cell in its name
          if (dref.arraySize > 0) // check array dataref
          {
            if (dref.evaluateArray()) // this function can change "isParamReadyToBeUsed" to FALSE hence it won't be available.
            {
              dref.arrayElementPicked = 0; // 0 = read/pick all values
              dref.readDatarefValue();
              drefValue = "{" + dref.pStrArrayValue + "}";
            }
            else
            {
              Log::logMsgNone("Fail eval array: " + dref.key);
              continue;
            }


          } // end if is array
          else
          {
            dref.readDatarefValue();
            drefValue = "{" + Utils::formatNumber<double>(dref.getValue()) +"}";
          }
          isValid = dref.flag_paramReadyToBeUsed;
        }
        else
        {
          Log::logMsgNone("Fail init: " + dref.key);
          isValid = false;
        } // if initStatic succeed
#endif

        // write to filtered file
        if (isValid)
        {
          line += "\t" + type + "\t" + writable + ((drefValue.empty())? drefValue:  "\t" + drefValue);
          fout << line << std::endl;
          if (!fout.good())
          {
            outError = "[Error] Fail to write to file: [" + inTargetFileAndPath + "] while writing it.";
            writeActionOK = false;
          }
          else
            drefLineProccessed++;
        }


      } 
      else
      {
        outError = "[Error] Fail to read from file: [" + inFileAndPath + "] while coping it.";
        writeActionOK = false;
      }

      line.clear();
      if (!writeActionOK)
      {
        return false;
      }

    } // end while loop over file lines    

    fout << fout.eof(); // manually add EOF at the end of file

  } // end if "fout" is valid
  else
  {
    outError = "[Error]Output stream is not functioning as expected. Skipping copy command.";
    return false;
  }

  Log::logMsgNone("Count DataRef.txt lines: " + Utils::formatNumber<int>(drefLineCounter));
  return success;
}

bool missionx::system_actions::read_saved_mission_dataref_file(std::string inFileAndPath, std::string & outError)
{
  // 1. open saved file
  // 2. read each line
  // 2.1 check if valid dataref (use Dataref class)
  outError.clear();
  unsigned int drefLineCounter = 0;
  std::vector<std::string> vecDrefLineSplit;
  std::vector<std::string> vecValues;
  std::string line; line.clear(); // holds the line from file
  std::string key; key.clear();
  std::string drefValue; // holds only the value of the key stored in file

#if defined DEBUG || defined _DEBUG
    Log::logMsg(">>>> Opening Saved Datarefs.");
#endif
  std::ifstream fin;
  fin.open(inFileAndPath.c_str());


  // v3.0.152
  int fileNameLength = (int)inFileAndPath.size();
  std::string displayFileNameInMessage;
  displayFileNameInMessage.clear();
    
  // v3.0.215.6 fixed a loading bug when folder string length exceed 110 characters. The last character split code did not use: "inFileAndPath.size()" and that caused a crash
  displayFileNameInMessage = QM + ((fileNameLength < 110) ? inFileAndPath : std::string(inFileAndPath.substr(0, 30)).append(" ... ").append(inFileAndPath.substr(inFileAndPath.size()-60))) + QM;
    
  if (fin.fail() || fin.bad())
  {
    outError = "[Error] Fail to open checkpoint dataref file - " + displayFileNameInMessage;
    return false;
  }


  while (std::getline(fin, line) )
  {
    if (!fin.good())
    {
      outError = "[Error] Problem reading mission savepoint dataref file - " + displayFileNameInMessage;
      return false;
    }
    
    if (fin.eof() )
    {
      break;
    }

    line = Utils::trim(line);
    drefLineCounter++;
      
    // skip if empty
    if (line.empty())
      continue;

    // check if line starts with "s" "/sim/xxx"
    char firstChar = line.front();
    if (firstChar != 's')
      continue; // skip line

    // split line
    vecDrefLineSplit.clear();
    vecValues.clear();

    vecDrefLineSplit = Utils::splitString(line, "\t");

    if (vecDrefLineSplit.empty() || vecDrefLineSplit.size() < 4)
      continue;

    // store localy the split values
    key = vecDrefLineSplit.at(MX_LOAD_KEY_POS_IN_VEC);

    drefValue = vecDrefLineSplit.at(MX_LOAD_VALUE_POS_IN_VEC); // holds value

    // clear values
    Utils::replaceCharsWithString(drefValue, "{}", "");
    drefValue = Utils::trim(drefValue);
    vecValues = Utils::splitString(drefValue, ","); // if array then we will be ready with the values

    if (vecValues.empty())
      continue;

    // create dataref_param
    // initialize key
    // if not array, then set value
    // if array then prepare array and call setarray function
    dataref_param dref(key);
    if (dref.dataRefType == xplmType_IntArray )
    {
      dref.vecArrayIntValues.clear();
      for (auto s : vecValues)
        dref.vecArrayIntValues.push_back(Utils::stringToNumber<int>(s));

#if defined DEBUG || defined _DEBUG
      std::string format = "[" + dref.key + "] [";
      for (auto s : vecValues)
        format += s + " ";

      format += "]";
      Log::logMsgNone(format);

#endif
      dataref_param::writeToDataRef(dref);

    }
    else if (dref.dataRefType == xplmType_FloatArray)
    {
      dref.vecArrayFloatValues.clear();
      for (auto s : vecValues)
        dref.vecArrayFloatValues.push_back(Utils::stringToNumber<float>(s));

#if defined DEBUG || defined _DEBUG
      std::string format = "[" + dref.key + "] [";
      for (auto s : vecValues)
        format += s + " ";

      format += "]";
      Log::logMsgNone(format); 
#endif
      dataref_param::writeToDataRef(dref);

    }
    else if (dref.dataRefType == xplmType_Int || dref.dataRefType == xplmType_Float || dref.dataRefType == xplmType_Double)
    {
      double d = Utils::stringToNumber<double>(drefValue);
      dref.setValue(d);

#if defined DEBUG || defined _DEBUG
      std::string format = "[" + dref.key + "] [" + drefValue + "]";
      Log::logMsgNone(format);
#endif
      dataref_param::writeToDataRef(dref);

      
    }
    else
    {
      Log::logMsgErr("Dataref might not be supported: " + QM + key + QM);
    }

  } // loop until eof


  return true;
} // read_saved_mission_dataref_file

std::string missionx::system_actions::getOptionFileAndPath ()
{
	char path[1024]={'\0'};
	std::string fileName = "missionx_pref_v3.xml";
  std::string fullPathName = "";

	XPLMGetPrefsPath(path);
	XPLMExtractFileAndPath(path);

	fullPathName = std::string (path) + XPLMGetDirectorySeparator() + fileName;


	return fullPathName;
}


void missionx::system_actions::load_plugin_options ()
{
  std::string fullPathName = system_actions::getOptionFileAndPath();

  // READ FROM PREF FILE
  Log::logXPLMDebugString("Loading Saved Options File: " + fullPathName + "\n");
  std::string errMsg; errMsg.clear();

  IXMLDomParser iDom;

  ITCXMLNode xMainNode = iDom.openFileHelper(fullPathName.c_str(), "OPTIONS", &errMsg);
  if ( errMsg.empty() )
  {
    system_actions::pluginOptions.loadOptions(xMainNode, errMsg ); // loadOptions is a wrapper for "loadCheckpoint" functions. We are basically reading properties
  }
  else
  {
  	Log::logXPLMDebugString(errMsg + "\n");
  	system_actions::store_plugin_options(); // save the options, but it also create if there are none
  }

}

void missionx::system_actions::store_plugin_options ()
{
	 std::string fullPathToPrefFile = system_actions::getOptionFileAndPath();


  /* Build XML in Memory */
  IXMLNode xMainNode; //=XMLNode::openFileHelper( std::string("save1.xml").c_str() );
  IXMLNode xNode;

  IXMLRenderer xmlWriter;

  xMainNode = IXMLNode::createXMLTopNode("xml", TRUE);
  xMainNode.addAttribute("version", "1.0");
  xMainNode.addAttribute("encoding", "iso-8859-1");
  xNode = xMainNode.addChild("OPTIONS");

  system_actions::pluginOptions.saveCheckpoint(xNode);

  xmlWriter.writeToFile( xMainNode, fullPathToPrefFile.c_str() );

  // test the file
  bool saveOk = true;

  std::string errMsg; errMsg.clear();

  IXMLDomParser iDom;
  //ITCXMLNode optionNode =
  iDom.openFileHelper(fullPathToPrefFile.c_str(), "OPTIONS", &errMsg);
  if ( ! errMsg.empty()  )
  {
    Utils::logMsg("[Error] " + errMsg );
    saveOk = false;
  }

  Utils::logMsg(std::string("Save options status: ") + ((saveOk)?"success." : "failed. "));

}


