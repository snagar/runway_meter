#pragma once
#ifndef WRITELOGTHREAD_H_
#define WRITELOGTHREAD_H_

#include <string>
#include <queue>

#include <sstream>
#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>
#include <stdio.h>

#include "../core/thread/base_thread.hpp"

namespace rwmeter
{

  class writeLogThread : public base_thread
  {
  public:
    writeLogThread();
    virtual ~writeLogThread();

    void flc();
    void stop_plugin();

    void add_message(std::string inMsg) 
    { 
      if (!tState.abort_thread)
        rwmeter::writeLogThread::qLogMessages.push(inMsg);     
    }

    void clear_all_messages() { 
      while (!rwmeter::writeLogThread::qLogMessages.empty())  
        rwmeter::writeLogThread::qLogMessages.pop(); 
    }

    static void set_logFilePath(std::string inFilePath) { rwmeter::writeLogThread::logFilePath = inFilePath; }
    void set_abortWrite(bool inVal) { this->tState.abort_thread = inVal; }

    static std::queue<std::string> qLogMessages_mainThread; // v3.0.221.4

  private:
    // parameters
    thread_state tState;
    static std::string logFilePath;


    // holds messages
    static std::queue<std::string> qLogMessages; // v3.0.217.8

    // members
    void init();
    bool exec_thread(thread_state * xxthread_state); // generic name for: "do your staff"

  };

}
#endif // WRITELOGTHREAD_H_
