#include "dataref_param.h"

/**************

Reviewd: 24-nov-2012

Done:
1. Renamed from "Dataref" to "dataref_param"
2. No real change in code.

ToDo:

**************/


missionx::dataref_param::dataref_param(void)
{
  MxParam();
  init();

  
}
/* ********************************************** */

missionx::dataref_param::dataref_param(std::string inKey)
{
	MxParam();
	init();
  this->setAndInitializeKey(inKey);
}


/* ********************************************** */

missionx::dataref_param::~dataref_param(void)
{
    //resetArrays(); // deprecate but need to clean arrays manually to solve a bug in cleaning class

}

/* ********************************************** */
void missionx::dataref_param::init()
{
  key.clear();
  dataRefId   = NULL;
  dataRefType = 0;
  arraySize = 0; //
  arrayElementPicked = 0; //
  arrayElementPickedTranslation = 0; //
  vecArrayIntValues.clear(); //
  vecArrayFloatValues.clear(); //

  flag_designerPickedOneElement = false; //
  flag_designerWantsToModifyAllElements = false; //

  outIntArray = nullptr;
  outFloatArray = nullptr;
}

/* ********************************************** */

void missionx::dataref_param::resetArrays() // 
{

  switch (dataRefType)
  {
  case (xplmType_IntArray):
  {
    if (this->outIntArray != nullptr ) // && (*outIntArray >= 0))
    {
      delete[] outIntArray;
      outIntArray = nullptr;
    }
  }
  break;
  case (xplmType_FloatArray):
  {
    //uintptr_t i = sizeof(outFloatArray); // debug

    if (this->outFloatArray != nullptr ) //&& (*outFloatArray >= 0.0f))
    {
      delete[] outFloatArray;
      outFloatArray = nullptr;
    }
  }
  break;
  default:
    break;
  } // end switch

}
/* ********************************************** */

bool missionx::dataref_param::initDataRefInfo()
{
  bool initOk = true;

  this->dataRefId = XPLMFindDataRef ( key.c_str() ) ;

  // if exists ( not NULL )
  if ( dataRefId )
  {
    // store datatype
    this->dataRefType = XPLMGetDataRefTypes ( dataRefId );
   

    // check if array
    if (dataRefType == xplmType_IntArray)
    {
      this->arraySize = XPLMGetDatavi(this->dataRefId, NULL, 0, 0);
      if (this->arraySize > 0)
      {
        outIntArray = new int[arraySize];
        std::memset(outIntArray, 0, sizeof(int)*arraySize); // initialize memory array
      }
      else
        outIntArray = nullptr;
    }
    else if (dataRefType == xplmType_FloatArray)
    {
      this->arraySize = XPLMGetDatavf(this->dataRefId, NULL, 0, 0);
      if (this->arraySize > 0)
      {
        outFloatArray = new float[arraySize];
        std::memset(outFloatArray, 0, sizeof(float)*arraySize); // initialize memory array

      }
      else
        outFloatArray = nullptr;
    }

  } // end if refID is not good
  else
  {
    initOk = false;

    errReason = "[UserDataRef] dataref:" + key +", was not added. Check spelling.";
#ifdef DEBUG_LOGIC
    sprintf (LOG_BUFF, "\n[UserDataRef] dataref: %s, was not added. Check spelling.", key.c_str() );
    XPLMDebugString ( LOG_BUFF );
#endif

  }

  // From MxParam class
  this->flag_paramReadyToBeUsed = initOk;

  return initOk;
} // addUserDataRef

  /* ********************************************** */

void missionx::dataref_param::setAndInitializeKey ( std::string inValue )
{
  this->key = inValue;
  initDataRefInfo();
}

/* ********************************************** */

XPLMDataRef missionx::dataref_param::getDataRefID() 
{
  return this->dataRefId;
}

/* ********************************************** */

XPLMDataTypeID missionx::dataref_param::getDataRefType()
{
  return this->dataRefType;
}

/* ********************************************** */

bool missionx::dataref_param::evaluateArray()
{
  if (this->arraySize > 0)
  {
    int arrayCell = 0;
    bool isArrayValid = true; // always valid unless prooved otherwise

//#if __GNUC__ < 5
    int intValue = 0;
    std::string errStr; errStr.clear();
    std::string str = getName(errStr);
    if (str.empty())
      str = Utils::extractLastFromString(this->key, "/"); // name = key without the path

    size_t lastRightBracket = 0;
    size_t lastLeftBracket = 0;

    if (str.empty()) // fix crash when calling "string.back()" when string is empty
      return false;

    if ( str.back() == ']' ) // if there is a right bracket
    {
      lastRightBracket = str.find_last_of(']');

      // find last right bracket and pick string between the brackets
      lastLeftBracket = str.find_last_of('[');
      if ( lastLeftBracket != std::string::npos) // if found left
      {
        std::string strNumber = str.substr((lastLeftBracket + 1), lastRightBracket - (lastLeftBracket+1));
        // check if string is a number
        //if (Utils::isStringNumber<int>(intValue, strNumber, std::dec))
        if (Utils::is_digits(strNumber))
        {          
          intValue = Utils::stringToNumber<int>(strNumber);
          arrayCell = intValue; // Utils::stringToNumber<int>(strNumber);
          if (arrayCell >= 0 && arrayCell <= this->arraySize)
          {
            isArrayValid = true;
            flag_designerPickedOneElement = true;
            flag_designerWantsToModifyAllElements = false;
          }
          else
          { // array element is not in the valid range
            isArrayValid = false;
            this->errReason = "[DREF ARRAY ERROR] Array is not in a valid range.";
          }

        }
        else
        { // not a number
          isArrayValid = false;
          this->errReason = "[DREF ARRAY ERROR] Element value is not a valid number.";
        }

      } // end leftBracket exists

    } // end right bracket exists
    else
    { // dataref key is FULL array

      isArrayValid = true;
      flag_designerPickedOneElement = false;
      flag_designerWantsToModifyAllElements = true;

      arrayCell = 0;
    }
//#else
//    std::smatch match;
//    std::regex pattern(mxconst::MX_REGEX_ARRAY_AT_THE_END_OF_STRING);
//
//
//    if ( regex_search(this->uqName, match, pattern) ) // if found matches
//    {
//      int pos = match.position(0); // we only want first match
//      std::string strArray(match[0].first, match[0].second); // get the
//      Log::logMsg("Original String " + this->uqName + ", array string: " + strArray + ", position: " + Utils::formatNumber<int>(pos)); // debug
//
//      pattern.assign (mxconst::MX_REGEX_INTEGER);
//      // extract the number from the array string
//      if  (regex_search(strArray, match, pattern) )
//      {
//        std::string strNumber; strNumber.clear();
//
//        for (auto c : match)
//          strNumber.append(c);
//
//        arrayCell = Utils::stringToNumber<int>(strNumber);
//        if (arrayCell >= 0 && arrayCell <= this->arraySize)
//        {
//          isArrayValid = true;
//          flag_designerPickedOneElement = true;
//          flag_designerWantsToModifyAllElements = false;
//
//        }
//        else
//        { // mark dataref as invalid
//          isArrayValid = false;
//        }
//
//      } // end second match size
//
//    } // end first match size
//    else 
//    { // dataref key is FULL array
//
//      isArrayValid = true;
//      flag_designerPickedOneElement = false;
//      flag_designerWantsToModifyAllElements = true;
//
//      arrayCell = 0;
//    }
//
//#endif

    if (isArrayValid)
    {
     this->arrayElementPicked = arrayCell;
     this->arrayElementPickedTranslation = ((arrayCell == 0) ? 0 : (arrayCell - 1)); // make sure array translation is not negative number
    }
    else
    {
      if ( errReason.empty())
        this->errReason = mxconst::MX_ERR_WRONG_ARRAY_NUMBER;

      this->flag_paramReadyToBeUsed = false;

       //this->invalid_param_reason = mxconst::MX_ERR_WRONG_ARRAY_NUMBER;
       //this->setIsParamReadyToUse(false);
    }
  } // end if is array
  // end if array


  return flag_paramReadyToBeUsed;
}

/* ********************************************** */

void missionx::dataref_param::fill_IntArrayIntoVector()
{
  if (!(this->dataRefType == xplmType_IntArray))
  {
    Log::logMsgErr("Not Int Array Type. Skipping...");
    return;
  }

  // fill vector with values.
  this->vecArrayIntValues.clear();
  this->pStrArrayValue.clear(); // 
  XPLMGetDatavi(dataRefId, outIntArray, 0, arraySize);

  for (int i = 0; i < arraySize; i++)
  {
    vecArrayIntValues.push_back(outIntArray[i]);
    pStrArrayValue.append(Utils::formatNumber<int>(outIntArray[i]));
    if (i < (arraySize - 1))
      pStrArrayValue.append(",");
  }
    
}

/* ********************************************** */

void missionx::dataref_param::fill_FloatArrayIntoVector()
{
  if (!(this->dataRefType == xplmType_FloatArray))
  {
    Log::logMsgErr("Not Float Array Type. Skipping...");
    return;
  }
  // fill vector with values.
  this->vecArrayFloatValues.clear();
  this->pStrArrayValue.clear(); // v2.1.29.4
  int realSize = XPLMGetDatavf(dataRefId, outFloatArray, 0, arraySize);  

  for (int i = 0; i < arraySize; i++)
  {
    vecArrayFloatValues.push_back(outFloatArray[i]);
    pStrArrayValue.append(Utils::formatNumber<float>(outFloatArray[i]));
    if (i < (arraySize - 1))
      pStrArrayValue.append(",");
  }
    
}

/* ********************************************** */

void missionx::dataref_param::readDatarefValue()
{
  this->pStrArrayValue.clear();

  if (this->flag_paramReadyToBeUsed)
  {
    // decide which kind of data need to fetch
    switch (dataRefType)
    {
    case xplmType_Int:
    {
      this->setValue((double)XPLMGetDatai(this->dataRefId));
    }
    break;
    case xplmType_Float:
    {
      this->setValue((double)XPLMGetDataf(this->dataRefId));
    }
    break;
    case (xplmType_Float | xplmType_Double):
    {
      this->setValue(XPLMGetDatad(this->dataRefId));
    }
    break;
    case (xplmType_IntArray):
    {
      if ( flag_paramReadyToBeUsed && this->arrayElementPicked > 0)
      {
        XPLMGetDatavi(this->dataRefId, outIntArray, 0, this->arraySize);
        this->setValue((double)outIntArray[this->arrayElementPickedTranslation]);
      }
      else if (flag_paramReadyToBeUsed && this->arrayElementPicked == 0)
      {
        fill_IntArrayIntoVector();
        if (vecArrayIntValues.empty())
        {
          flag_paramReadyToBeUsed = false;
        }
        else
        {
          // initialize pValue with first value from vector
          this->setValue((double)vecArrayIntValues.at(0) );
        }
      }
    }
    break;
    case (xplmType_FloatArray):
    {
      if (flag_paramReadyToBeUsed && this->arrayElementPicked > 0)
      {
        XPLMGetDatavf(this->dataRefId, outFloatArray, 0, this->arraySize);
        this->setValue((double)outFloatArray[this->arrayElementPickedTranslation]);
      }
      else if (flag_paramReadyToBeUsed && this->arrayElementPicked == 0)
      {
        fill_FloatArrayIntoVector();
        if (vecArrayFloatValues.empty())
        {
          flag_paramReadyToBeUsed = false;
        }
        else
        {
          // initialize with first value from vector
          this->setValue((double)vecArrayFloatValues.at(0));
        }
      }
    }
    break;
    default:
      break;

    } // switch
  }
} // readDatarefValue

/* ********************************************** */

void missionx::dataref_param::storeCoreAttribAsProperties()
{
  setStringProperty(ATTRIB_DREF_KEY, this->key); //
}

/* ********************************************** */

void missionx::dataref_param::applyPropertiesToDref()
{
  std::string err; err.clear();
  std::string key;
  assignStringPropertyToAttribute(ATTRIB_DREF_KEY, key, err);
  this->setAndInitializeKey(key);
}

/* ********************************************** */

void missionx::dataref_param::saveCheckpoint(IXMLNode & inParent)
{
  storeCoreAttribAsProperties();

  IXMLNode xDref = inParent.addChild(ELEMENT_DATAREF.c_str());
  xDref.addAttribute(ATTRIB_NAME.c_str(), getName().c_str());
  mxProperties::saveCheckpoint(xDref);

#ifdef MX_EXE
  Log::logMsgWarn("In console mode, there are no Datarefs. Only in plugin mode");
#endif // 

}

/* ********************************************** */

bool missionx::dataref_param::loadCheckpoint(ITCXMLNode & inParent, std::string & outErr)
{
  mxProperties::loadCheckpoint(inParent, outErr);

  // apply properties
  this->applyPropertiesToDref();

  return this->flag_paramReadyToBeUsed; // unique to Dataref
}

/* ********************************************** */

void missionx::dataref_param::writeToDataRef(dataref_param &inDref)
{
  XPLMDataRef dataRefId = inDref.dataRefId;
  XPLMDataTypeID dataRefType = inDref.dataRefType;

  if (dataRefId) // if exists
  {
    
    switch (dataRefType)
    {
    case xplmType_Int:
    {
      XPLMSetDatai(dataRefId, (int)inDref.getValue());
    }
    break;
    case xplmType_Float:
    {
      XPLMSetDataf(dataRefId, (float)inDref.getValue());
    }
    break;
    case (xplmType_Float | xplmType_Double):
    {
      XPLMSetDatad(dataRefId, inDref.getValue());
    }
    break;
    case (xplmType_IntArray): // can only return the value of specific array and not the whole array. arrayElementPicked must be defined
    {
      inDref.fill_IntArrayIntoVector(); // v3.0.221.10

      if (inDref.vecArrayIntValues.empty())
        return; // skip


      if (inDref.flag_designerWantsToModifyAllElements) // v3.0.221.10 assign value to all array based on one value
      {
        int vecSize = (int)inDref.vecArrayIntValues.size();
        for (int i1 = 0; i1 < vecSize; ++i1)
        {
          inDref.vecArrayIntValues.at(i1) = (int)inDref.getValue();
        }

      }

      int *iValuesArray = &inDref.vecArrayIntValues[0];
      XPLMSetDatavi(inDref.dataRefId, iValuesArray, 0, (int)inDref.vecArrayIntValues.size());
            
      Log::logMsg("Setting value into INT array");
    }
    break;
    case (xplmType_FloatArray): // can only return the value of specific array and not the whole array. arrayElementPicked must be defined
    {
      inDref.fill_FloatArrayIntoVector(); // v3.0.221.10

      if (inDref.vecArrayFloatValues.empty())
        return; // skip

      if (inDref.flag_designerWantsToModifyAllElements) // v3.0.221.10 assign value to all array based on one value
      {
        int vecSize = (int)inDref.vecArrayFloatValues.size();
        for (int i1 = 0; i1 < vecSize; ++i1)
        {
          inDref.vecArrayFloatValues.at(i1) = (float)inDref.getValue();
        }
        
      }


      float *fValuesArray = &inDref.vecArrayFloatValues[0];
      XPLMSetDatavf(inDref.dataRefId, fValuesArray, 0, (int)inDref.vecArrayFloatValues.size());

      //Log::logMsg(std::string ("Setting value into FLOAT array. ") + inDref.key ); // debug
    }
    break;
    default:
    {
      Log::logMsg("Can't handle this Dataref Datatype!!! ");
    }
    break;
    }// end switch
  } // end if
} // end writeToDataRef


/* ********************************************** */

/* ********************************************** */

/* ********************************************** */



/* Data of a type the current XPLM doesn't do.
xplmType_Unknown                         = 0

A single 4-byte integer, native endian.
,xplmType_Int                             = 1

A single 4-byte float, native endian.
,xplmType_Float                           = 2

A single 8-byte double, native endian.
,xplmType_Double                          = 4

An array of 4-byte floats, native endian.
,xplmType_FloatArray                      = 8

An array of 4-byte integers, native endian.
,xplmType_IntArray                        = 16

A variable block of data.
,xplmType_Data                            = 32
*/
