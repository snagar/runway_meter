#ifndef MXPARAM_H_
#define MXPARAM_H_

#pragma once

#include"../data/mxProperties.hpp"
using namespace missionx;

namespace missionx
{

/*
 MxParam represent parameters in x-plane, which should only be "double" based.
*/
class MxParam : public missionx::mxProperties
{
public:
  MxParam() { init(); };
  ~MxParam() {};


  using missionx::mxProperty::getValue;

  std::string errReason; // any initialization error should be written to this variable
  std::string pStrArrayValue; // Will hold full array values delimeted by comma ","
  bool        flag_paramReadyToBeUsed; // ALSO use for dataref array that might not be set correctly. FALSE = not usable

  // members
  void init()
  {
    setValue(0.0); // set property value to double initialization.
    errReason.clear();
    flag_paramReadyToBeUsed = false;
  }

  double getValue()
  {
    return getValue<double>();
  }

private:
//  int         pType;

};

} // missionx

#endif