#ifndef MISSIONDATAREF_H_
#define MISSIONDATAREF_H_
#pragma once

/**************

ToDo:

**************/

#include "XPLMDataAccess.h"
#include <iostream>
#include <string>
#include <stdio.h>
#include <vector>


#include "MxParam.hpp"
#include "../core/Utils.h"
#include "../io/Log.hpp"

using namespace std;
using namespace missionx;
using namespace mxconst;


namespace missionx
{
// REMOVED REGEX since we use a simpler code for cross platform compatibility
//#if LIN && __GNUC__ < 5
//	#include <regex.h> // v2.1.29.4
//#else
//	#include <regex> // v2.1.29.4
//#endif


#include "../core/xx_mission_constants.hpp"


class dataref_param : public MxParam
{
private:
  void init();
public:
  dataref_param(void);
  dataref_param(std::string inKey);
  virtual ~dataref_param(void);

  std::string     key;
  XPLMDataRef     dataRefId;
  XPLMDataTypeID  dataRefType;
  //int             dataRefIsWritable; // treat as boolean 

  // core member
  bool            initDataRefInfo();

  //Read and assign dataref value, automatic resolving data type.  //You should call getValue() after this function.
  void readDatarefValue(); 


  // ARRAYS
  /*  evaluate against only 1 element */
  bool            flag_designerPickedOneElement; // v3.0.221.10 renamed from isElementPickedInArray // means that designer is only interested in 1 element value from the array
  /*  evaluate against ALL elements */                                          /*  evaluate against only 1 element */
  bool            flag_designerWantsToModifyAllElements; // v3.0.221.10 renamed from isElementNotPickedInArray // means the designer wants to test against ALL values in the array
  int             arraySize; // size of array
  unsigned int    arrayElementPicked; // which part of the array to fetch
  unsigned int    arrayElementPickedTranslation; // Translate array number to C++ array. (usually: "arrayElementPicked-1")


  int             *outIntArray; // fill with data when reading from XPLMGetDatavi()
  float           *outFloatArray; // fill with data when reading from XPLMGetDatavf()
  vector <float>  vecArrayFloatValues; // will hold the values from the dataref array for float.  
  vector <int>    vecArrayIntValues; // will hold the values from the dataref array for integer.  

  // members  
  void            setAndInitializeKey ( std::string inValue );
  XPLMDataRef     getDataRefID();
  XPLMDataTypeID  getDataRefType();

  bool evaluateArray();  // check designer defined correct array settings and set correct parameters
  void resetArrays(); // delete the array due to re-construct array parameter. Should be called when plane is changed or "stop/disable plugin" or "reset mission"
  void fill_IntArrayIntoVector();
  void fill_FloatArrayIntoVector();
  


  // checkpoint
  void storeCoreAttribAsProperties();
  void applyPropertiesToDref();

  void saveCheckpoint(IXMLNode &inParent);
  bool loadCheckpoint(ITCXMLNode &inParent, std::string &outErr);

  static void writeToDataRef(dataref_param &inDref);

// XPLMDataTypeID = int value, casting of inValue will be defined by the type sent
//  bool  setValue ( XPLMDataTypeID inType, V inValue);


     /* Data of a type the current XPLM doesn't do.                                 
     xplmType_Unknown                         = 0

      A single 4-byte integer, native endian.
    ,xplmType_Int                             = 1

      A single 4-byte float, native endian.
    ,xplmType_Float                           = 2

      A single 8-byte double, native endian.
    ,xplmType_Double                          = 4

      An array of 4-byte floats, native endian.
    ,xplmType_FloatArray                      = 8

      An array of 4-byte integers, native endian.
    ,xplmType_IntArray                        = 16

      A variable block of data.
    ,xplmType_Data                            = 32
    */

};


}


#endif /* MISSIONDATAREF_H_ */
