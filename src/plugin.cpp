#include "plugin.h"
#include "core/base_xp_include.h"
#include "core/xx_rwmeter_constants.hpp"
#include "core/Timer.hpp"
#include "core/Utils.h"
#include "rwm.h" // main class

using namespace rwmeter;

namespace rwmeter
{
  rwmeter::RWM rwm;

  float pluginCallback(float inElapsedSinceLastCall, float inElapsedTimeSinceLastFlightLoop, int inCounter, void* inRefcon);
  int   drawCallback_missionx(XPLMDrawingPhase inPhase, int inIsBefore, void* inRefcon);
  void  rwmeterMenuHandler(void* inMenuRef, void* inItemRef);

  void rwmMenuHandler(void * inMenuRef, void * inItemRef);
  /* ******** Commands ************************** */

  // command members

}

/* *************************************************** */
// Mandatory function. START
PLUGIN_API int XPluginStart(char * outName, char * outSig, char * outDesc)
{
    
  XPLMEnableFeature("XPLM_USE_NATIVE_PATHS", 1);
  // v3.0.219.7
 
  std::memset(LOG_BUFF, '\0', LOG_BUFF_SIZE); // First time initialization of LOG_BUFF so it will have a concrete set of memory to work on.
  Timer t1;
  Timer::start(t1,0,"XPluginStart Timer");


  // folder data


  // Register Plugin
  std::string plugin_name = "runway meter v" + std::string(PLUGIN_VER_MAJOR) + "." + std::string(PLUGIN_VER_MINOR) + "." + std::string(PLUGIN_REVISION);
  std::string plugin_sig  = "rwmeter (snagar.dev@gmail.com)";
  std::string plugin_desc = "Provide simple runway length cues so simmer might see the distance between touchdown and full stop";

#ifdef IBM
  strncpy_s(outName, 255, plugin_name.c_str(), sizeof(plugin_name) * sizeof(std::string));
  strncpy_s(outSig , 255, plugin_sig.c_str() , sizeof( plugin_sig) * sizeof(std::string));
  strncpy_s(outDesc, 255, plugin_desc.c_str(), sizeof(plugin_desc) * sizeof(std::string));
#else
  std::strncpy(outName, plugin_name.c_str() , 255);
  std::strncpy(outSig , plugin_sig.c_str()  , 255 );
  std::strncpy(outDesc, plugin_desc.c_str() , 255);
#endif
  // END Register Plugin

  // register callbacks
  XPLMRegisterFlightLoopCallback(pluginCallback, -1, NULL);
  //XPLMRegisterDrawCallback(drawCallback_missionx, xplm_Phase_Objects, 0, NULL);



  // MENU
  int rwm_menu = XPLMAppendMenuItem(XPLMFindPluginsMenu(), outName, (void *)rwmeter::rwm_const::rwmMenu_enum::RW_MENU_ENTRY, 1);
  rwmeter::rwm_const::rwmMenuEntry = XPLMCreateMenu(outName, XPLMFindPluginsMenu(), rwm_menu, &rwmMenuHandler, NULL);






  Timer::wasEnded(t1);
//  Log::logMsgNone("\n");
  Log::logMsg(Timer::to_string(t1));


  Log::logXPLMDebugString(">>>>>>>>>>>> END Loading Runway Meter <<<<<<<<<<<<\n");

  return 1;
}

/* *************************************************** */
// Mandatory Function STOP
PLUGIN_API void XPluginStop(void)
{



  

 
  // abort Log writeMessage
  Log::stop_mission(); // v3.0.217.8
  XPLMDebugString("sleep 1 sec");
  std::this_thread::sleep_for(std::chrono::seconds(1));
  


   XPLMDebugString("[rwmeter] Plug-in STOPped");
}

/* *************************************************** */
// Mandatory Function ENABLE
PLUGIN_API int XPluginEnable(void)
{
  // register callbacks
  XPLMRegisterFlightLoopCallback(pluginCallback, -1, NULL);
  // deprecated according to Ben Supnik request. Draw callback will be differed when we toggle the UI for the first time
  XPLMRegisterDrawCallback(drawCallback_missionx, xplm_Phase_Objects, 0, NULL);


  return 1; // important so callback will continue
}

/* *************************************************** */
// Mandatory Function DISABLE
PLUGIN_API void XPluginDisable(void)
{
  //debug
  Log::logMsg("[rwmeter] Plug-in Disabling");
  // unregister callbacks
  XPLMUnregisterFlightLoopCallback(pluginCallback, NULL);
  XPLMUnregisterDrawCallback(drawCallback_missionx, xplm_Phase_Objects, 0, 0);
  XPLMUnregisterDrawCallback(drawCallback_missionx, xplm_Phase_Window, 0, 0);
  XPLMUnregisterDrawCallback(drawCallback_missionx, xplm_Phase_LocalMap3D, 0, 0);


  // abort Log writeMessage
  Log::stop_mission(); // v3.0.217.8

  //debug
  Log::logMsg("[rwmeter] Plug-in Disabled");
}

/* *************************************************** */
PLUGIN_API void XPluginReceiveMessage(XPLMPluginID inFrom, intptr_t inMsg, void * inParam)
{
#ifdef DEBUG

 // Log::logMsg ("[PluginMsg]Message Sent: " + Utils:: );
#endif

  switch (inMsg)
  {
  case XPLM_MSG_AIRPORT_LOADED:
  case XPLM_MSG_SCENERY_LOADED:
  {

  Log::logDebugBO("rwmeter XPLM_MSG_SCENERY_LOADED"); // debug

      // prepare Cue points 
      //rw_data_manager::resetCueSettings();
      //rw_data_manager::flc_cue(cue_actions_enum::cue_action_first_time); // v3.0.202a
      //// re-calculate 3D objects local + re-position + Cue Points
      //rw_data_manager::refresh3DInstancesAndPointLocation();


  }
    break;
  default:
    break;
  } // switch
}

/* *************************************************** */

namespace rwmeter
{
  void rwmeterMenuHandler(void * inMenuRef, void * inItemRef)
  {
    // Main Menu Dispatcher that calls the function that creates each Widget

    switch (((intptr_t)inItemRef))
    {
 
    default:
      break;
      //mission.rwmMenuHandler(inMenuRef, inItemRef);
    }

  }

  void rwmMenuHandler(void * inMenuRef, void * inItemRef)
  {
  }

  /* *************************************************** */

  /* *************************************************** */
  
  /* *************************************************** */
  float pluginCallback(float inElapsedSinceLastCall, float inElapsedTimeSinceLastFlightLoop, int inCounter, void * inRefcon)
  {

    //mission.flc();

    return 1.0f;
  }

  /* *************************************************** */
  int drawCallback_missionx(XPLMDrawingPhase inPhase, int inIsBefore, void * inRefcon)
  {

    // for each draw callback phase we should call different draw function
    //mission.drawCallback(inPhase, inIsBefore, inRefcon);

    return 1;
  }

  /* *************************************************** */

  /* *************************************************** */
 
  /* *************************************************** */
   /* *************************************************** */
  /* *************************************************** */
  /* *************************************************** */
  /* *************************************************** */
  /* *************************************************** */
  /* *************************************************** */

}
