#ifndef RWM_H_
#define RWM_H_

#pragma once

#include "core/base_xp_include.h"
#include "core/rwm_const.h"
#include "core/rw_data_manager.h"

namespace rwmeter
{

  class RWM
  {
  public:
    RWM();
    virtual ~RWM();
  };


}
#endif