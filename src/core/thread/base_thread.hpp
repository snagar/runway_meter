#ifndef BASETHREADH_
#define BASETHREADH_

#pragma once

/******
The class represents thread, and control information on it


**/

#include <iostream>       // std::cout
#include <map>       // std::cout
#include <thread>         // std::thread
#include <chrono> // v3.0.219.12
#include <ctime> // v3.0.219.12

#include "../MxUtils.h"
//#include "../../data/mxProperties.hpp"

namespace rwmeter
{

class base_thread
{
public:
  base_thread() {};
  ~base_thread() {};

  typedef struct _thread_state
  {
    // Chrono
    std::chrono::time_point<std::chrono::steady_clock> timer;
    std::string duration_s;

    std::atomic<bool> is_active;// = false; // does thread() running/active
    std::atomic<bool> abort_thread; // = false; // does the plugin exit, thus aborting thread actions
    std::atomic<bool> thread_done_work; // = false; // true/false. Set to true only when Class::run_thread job finish execution

    std::string dataString; // string data if needed for the thread, like path.
    std::map<std::string, std::string> mapValues;
    std::atomic<int> counter; // will be used for counting long loop operations like apt.dat optimizations

    //mxProperties pipeProperties;
    //missionx::mx_random_thread_wait_state_enum thread_wait_state; // v3.0.221.3

    std::atomic<mx_random_thread_wait_state_enum> thread_wait_state;

    void init()
    {
      is_active = false;
      abort_thread = false;
      thread_done_work = false;
      dataString.clear();
      mapValues.clear();
      counter = 0;

      startThreadStopper();
      //pipeProperties.clear();
    }

    void startThreadStopper()
    {
      timer = std::chrono::steady_clock::now();
    }

    std::string getDuration()
    {
      std::chrono::time_point<std::chrono::steady_clock> end = std::chrono::steady_clock::now();      
      double duration = (double)std::chrono::duration_cast<std::chrono::seconds>(end-timer).count();

      duration_s = mxUtils::formatNumber<double>((duration));
      return duration_s;
    }

  } thread_state;
 
  std::thread thread_ref; // will hold pointer to the current running thread

};

}
#endif // BASETHREADH_
