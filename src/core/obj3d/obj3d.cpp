#include "obj3d.h"

namespace missionx
{
  XPLMDataRef obj3d::fps_dref;
}

void missionx::obj3d::initPathCycle()
{
  //mvStat.noOfPointsInPath = (int)path.size();
  mvStat.noOfPointsInPath = (int)deqPoints.size();
  mvStat.currentPointNo = 0;
  mvStat.hasReachedLastPoint = false;
  
  nextPoint(); // determind the first point to be the target
  //base_target_status.isStarted = true; // deprecated v3.0.207.1
}

missionx::obj3d::obj3d()
{
  this->obj3dType = obj3d::static_obj;
  this->displayDefaultObjectFileOverAlternate = true;
  this->isInDisplayList = false;

  this->setBoolProperty(PROP_SCRIPT_COND_MET_B, true);
  this->file_and_path.clear();

  this->g_object_ref = NULL;
  this->g_instance_ref = NULL;

  dr.structSize = sizeof(dr);

  //this->path.clear(); // v3.0.202
  this->deqPoints.clear(); // v3.0.213.7
  lastPointTargetUsed = 0; // v3.0.207.1

  fps_dref = XPLMFindDataRef("sim/operation/misc/frame_rate_period"); // frame_rate_period - fps

  isScriptCondMet = false; // v3.0.209.2
}


missionx::obj3d::~obj3d()
{
}


void missionx::obj3d::load_cb(const char * real_path, void * ref)
{
  XPLMObjectRef * dest = (XPLMObjectRef *)ref;
  if (*dest == NULL)
  {
    *dest = XPLMLoadObject(real_path);
  }

}

void missionx::obj3d::create_instance()
{
  if (this->g_object_ref)
  {
    if (this->g_instance_ref) // v3.0.207.2 Try to solve instance vanish after providing a new location. In this code, we will create new instance every frame.
      XPLMDestroyInstance(this->g_instance_ref);

    this->g_instance_ref = XPLMCreateInstance(this->g_object_ref, NULL);
  }

#if defined (_DEBUG) || defined (DEBUG)
  if (this->g_instance_ref) // debug
    Utils::logMsg("Instance created... " + mxconst::UNIX_EOL);
#endif
}

void missionx::obj3d::fetchFpsInfo()
{
  mvStat.fps = XPLMGetDataf(fps_dref);
                                                //    if ( mvStat.fps == 0.0f || mvStat.fps == NULL )
  if (mvStat.fps == 0.0f) // prevent divide by zero
    mvStat.fps = 1.0f;

  mvStat.fps = 1.0f / mvStat.fps;
  
  mvStat.deltaTime = mvStat.currentTimeElapsed - mvStat.oldTimeElapsed;
}


void missionx::obj3d::calcPosOfMovingObject()
{  
  static double elevInMeter = 0.0;
  //static double groundElevation = 0.0; // TODO: Consider moving calculation to "flc()" function instead of draw function for optimization.

  //fetchFpsInfo(); // v3.0.207.3 removed for performance optimization
  missionx::Timer::wasEnded(mvStat.timer);

  this->mvStat.deltaTime = missionx::Timer::getDeltaBetween2TimeFragments(this->mvStat.timer);
  if (this->mvStat.deltaTime > 0.0f && this->mvStat.fps > 0.0f)
  {
    mvStat.currentTimeElapsed = mvStat.timer.getSecondsPassed();
    this->mvStat.timeOnVector = ((1 / this->mvStat.fps) + mvStat.currentTimeElapsed) / this->mvStat.secondsToReachTarget; // secondsToReachTarget was timeToReachTarget
  }

  this->mvStat.time_was_advanced_by_draw_function = true;
  this->setCoordinateOnVector(this->mvStat.pointFrom, this->mvStat.pointTo, this->mvStat.timeOnVector);
  this->displayCoordinate.calcSimLocalData();


  //this->mvStat.pointCurrent.lat = this->displayCoordinate.lat;
  //this->mvStat.pointCurrent.lon = this->displayCoordinate.lon;

  //groundElevation = UtilsGraph::getTerrainElevInMeter_FromPoint(this->displayCoordinate, probeResult); // TODO: Consider moving calculation to "flc()" function instead of draw function for optimization.
  elevInMeter = this->getElevInMeter();
  
  if (elevInMeter == 0 || elevInMeter < mvStat.groundElevation)
  	this->findAndSetTerrainElevation_to_DisplayCoordination();

}



void missionx::obj3d::setCoordinateOnVector(missionx::Point & pointFrom, missionx::Point & pointTo, float time)
{
  static float calc_against_heading;
  calc_against_heading = (pointTo.adjust_heading == 0.0f) ? (float)pointFrom.getHeading() : pointTo.adjust_heading; // this make sure that heading won't change if adjust is Zero

#if defined (_DEBUG) || defined (DEBUG)  
  std::string name = this->getName();
#endif
  //this->displayCoordinate.lat = (pointTo.lat - pointFrom.lat) * time + pointFrom.lat;
  //this->displayCoordinate.lon = (pointTo.lon - pointFrom.lon) * time + pointFrom.lon;
  this->displayCoordinate.setLat( (pointTo.getLat() - pointFrom.getLat()) * time + pointFrom.getLat() );
  this->displayCoordinate.setLon( (pointTo.getLon() - pointFrom.getLon()) * time + pointFrom.getLon() );
  this->displayCoordinate.setElevationMt( (pointTo.getElevationInMeter() - pointFrom.getElevationInMeter()) * time + pointFrom.getElevationInMeter() );

  //this->displayCoordinate.heading = (pointTo.heading - pointFrom.heading) * time + pointFrom.heading;
  this->displayCoordinate.setHeading ( ( calc_against_heading - pointFrom.getHeading()) * time + pointFrom.getHeading()); // v3.0.207.5 calculate relative to the adjust heading value
  this->displayCoordinate.setPitch((pointTo.getPitch() - pointFrom.getPitch()) * time + pointFrom.getPitch());
  this->displayCoordinate.setRoll( (pointTo.getRoll() - pointFrom.getRoll()) * time + pointFrom.getRoll() );


}


void missionx::obj3d::calculateMovement_per_flc()
{
  mvStat.shouldWeRenderObject = false; // we always init with this value

  mvStat.groundElevation = UtilsGraph::getTerrainElevInMeter_FromPoint(this->displayCoordinate, mvStat.probeResult); // v3.0.207.3
  

  if (!mvStat.hasReachedLastPoint)
  {
    // fetch FPS info
    fetchFpsInfo(); // we fetch this every draw callback, so maybe we should deprecate this line

    if (this->isInDisplayList) // if we already display the 3D Object  // former isRendered (v2.x)
    {
     
      // first time timer initialization, we use it to calculate movement on vector
      if ( !this->mvStat.waitTimer.isRunning() //  getState() != missionx::mx_timer_state::timer_running 
           && mvStat.timer.getState() == missionx::mx_timer_state::timer_not_set ) // check if timer started
      {
        missionx::Timer::start(mvStat.timer, 0, "obj3d_timer_" + this->getName()); // run continuesly 
        mvStat.lastZuluStartDraw = 0.0f;
        //mvStat.pointCurrent.clone(mvStat.pointFrom); // pointFrom decided in "nextPoint()" function.
      }

      // workaround in the else of this Condition caused by double call or off screen pauses like moving screen or weather widget setup etc...
      if (mvStat.time_was_advanced_by_draw_function) 
      {
        mvStat.time_was_advanced_by_draw_function = false; // v2.1.0 a15 // reset until next draw will advance timer
        mvStat.shouldWeRenderObject = true; // flag for instance display decition

        if (!mvStat.hasReachedPointTo)
        {

          if (mvStat.currentTimeElapsed && !mvStat.isFirstTime)
          {
            //this->displayCoordinate.clone(mvStat.pointCurrent); // not sure this is needed, since we set coordination every frame
            //mvStat.pointCurrent.clone(this->displayCoordinate); // v3.0.207.2 Copy display coordination into current




            mvStat.hasReachedPointTo = ((fabs(mvStat.pointTo.getLat() - this->displayCoordinate.getLat()) < fabs(mvStat.pointTo.getLat() - mvStat.prevPoint.getLat())) &&
              (fabs(mvStat.pointTo.getLon() - this->displayCoordinate.getLon()) < fabs(mvStat.pointTo.getLon() - mvStat.prevPoint.getLon()))
              ) ? false : true;

#ifdef DEBUG_MOVE
          	double val1,val2,val3,val4;
          	val1 = fabs(mvStat.pointTo.getLat() - this->displayCoordinate.getLat());
          	val2 = fabs(mvStat.pointTo.getLat() - mvStat.prevPoint.getLat());
          	val3 = fabs(mvStat.pointTo.getLon() - this->displayCoordinate.getLon());
          	val4 = fabs(mvStat.pointTo.getLon() - mvStat.prevPoint.getLon());

          	Utils::logMsg("(pointTo.lat-display.lat)=" + Utils::formatNumber<double>(val1) + ", (pointTo.lat-prev.lat)="  Utils::formatNumber<double>(val2) + ((val1 < val2)? "[t]" : "[f]" ) + " | " + "(pointTo.lon-display.lon)=" + Utils::formatNumber<double>(val3) + ", (pointTo.lon-prev.lon)=" + Utils::formatNumber<double>(val4) + ((val3 < val4)? "[t]" : "[f]" ) );

          	Utils::logMsg(std::string(" - hasReachedPointTo: ") + ((mvStat.hasReachedPointTo)? "<< TRUE >>" : "FALSE"  ) + "\n" );

#endif
//            mvStat.hasReachedPointTo = ((fabs(mvStat.pointTo.lat - mvStat.pointCurrent.lat) < fabs(mvStat.pointTo.lat - mvStat.prevPoint.lat)) &&
//              (fabs(mvStat.pointTo.lon - mvStat.pointCurrent.lon) < fabs(mvStat.pointTo.lon - mvStat.prevPoint.lon))
//              ) ? false : true;


            mvStat.prevPoint.setLat( this->displayCoordinate.getLat() );
            mvStat.prevPoint.setLon( this->displayCoordinate.getLon() );
            mvStat.prevPoint.setElevationMt(this->displayCoordinate.getElevationInMeter());
            mvStat.prevPoint.timeToWaitOnPoint_sec = this->displayCoordinate.timeToWaitOnPoint_sec;


          } // any change in time ?

        } // end test if reached destination


        /// Handle Reached target point
        if (mvStat.hasReachedPointTo)
        {
#ifdef DEBUG_MOVE
          Utils::logMsg("\nMoving to nextPoint()\n"); // debug
#endif
          mvStat.prevPoint.clone(this->displayCoordinate); // duplicated code
                                                             // move to next point
          this->nextPoint(); // v3.0.207.3

          mvStat.currentTimeElapsed = 0.0f;
          mvStat.timeOnVector = 0.0f; // v2.1.0 a15


        }
        // v3.0.207.2 DEPRECATED "else" since displayCoordinate is always refreshed every draw frame
        //else
        //{
        //  this->displayCoordinate.clone(mvStat.pointCurrent);

        //} // end else if

        mvStat.isFirstTime = false;
      }
      else
      {
        mvStat.shouldWeRenderObject = true; // workaround - should we RENDER ?
      }
    } // if any time elapsed from lat iteration
    else
    {
      mvStat.shouldWeRenderObject = false;
   
    } // end if is in display list (rendered)


  }

}

void missionx::obj3d::calcNewCourseBetweenTwoPointsOnVector()
{
  std::string err;
  double distance = 0.0f;
  err.clear();
  
  distance = this->mvStat.pointFrom.calcDistanceBetween2Points(this->mvStat.pointTo, &err);
  if (!err.empty())
    distance = 0.0;

  mvStat.distanceFromTo_ft = distance * (missionx::nm2meter*missionx::meter2feet); //  (float)Utils::nmToFeet(&distance);

  mvStat.secondsToReachTarget = ((float)mvStat.distanceFromTo_ft / ((mvStat.pointFrom.getSpeedFts() == 0.0f) ? 0.000000000001f : mvStat.pointFrom.getSpeedFts())); // seconds to reach destination  

#ifdef DEBUG_MOVE
  sprintf(LOG_BUFF, "[Moving3D calc] new pointFrom.lat: %f, new pointFrom.lon: %f", mvStat.pointFrom.getLat(), mvStat.pointFrom.getLon());
  Utils::logMsg(LOG_BUFF);
  sprintf(LOG_BUFF, "[Moving3D calc] new pointTo.lat: %f  , new pointTo.lon: %f", mvStat.pointTo.getLat(), mvStat.pointTo.getLon());
  Utils::logMsg(LOG_BUFF);
  //sprintf(LOG_BUFF, "[Moving3D calc]Distance in Feet: %f  , Time To reach in feet to sec speed: %f", mvStat.distanceFromTo, mvStat.timeToReachTarget);
  //Utils::logMsg(LOG_BUFF);
#endif
}

void missionx::obj3d::nextPoint()
{
  // 1. Check if isReachedDestination is true
  // 2. if NOT isReachedDestination is true then, check noOfPoints.
  //    If size-currentStartPointNo > 1 then we can take the value from currentStartPointNo & (currentStartPointNo+1)
  //    if less then 1, then we will take currentStartPointNo & 0 if cycle
  //if ( isCycling )
  //  nextPointCounter = 0;
  //else
  //  nextPointCounter++;

#if defined _DEBUG || defined DEBUG
  Log::logMsg("[MovingObject3D nextPoint]start for 3D Object: >>>> " + this->getName() + " <<<<" + ", Counter: " + Utils::formatNumber<int>(mvStat.currentPointNo));
#endif

  mvStat.lastZuluStartDraw = 0.0f;
  mvStat.timeOnVector = 0.0f; 

  if ( (this->getIsPathNeedToCycle()) || (!this->getIsPathNeedToCycle() && !this->mvStat.hasReachedLastPoint)) // process IfoIf path need cycle or obj3d did not reached last point
  {

    //itPathEnd = this->path.end(); // v3.0.207.2
    itPathEnd = this->deqPoints.end(); // v3.0.213.7

    if (this->g_instance_ref) // is object visible
    {


      if (mvStat.noOfPointsInPath == 1)
      {
        mvStat.isMoving = false; // v3.0.207.4 flag object as not moving since only one point
        if (mvStat.isFirstTime)
        {
          this->itPath = this->deqPoints.begin(); // v3.0.213.7 // deprecated this->path.begin(); // should point to the starting point

          mvStat.pointFrom.clone((*itPath));
          mvStat.pointTo.clone(mvStat.pointFrom);
//          mvStat.pointCurrent.clone(mvStat.pointFrom); // v3.0.207.3
          mvStat.prevPoint.clone(mvStat.pointFrom);
          mvStat.isInRecursiveState = false;
          calcNewCourseBetweenTwoPointsOnVector();
          mvStat.hasReachedLastPoint = true;

          mvStat.isFirstTime = false;
        }
        mvStat.pointFrom.timeToWaitOnPoint_sec = 0.0f;
        mvStat.pointFrom.calcSimLocalData(); // v3.0.207.2
        this->displayCoordinate.clone(mvStat.pointFrom); // where to place 3D Object

      }// check next point not overflow
      else if (mvStat.noOfPointsInPath > 1/* && (mvStat.noOfPointsInPath - mvStat.currentPointNo > 1)*/)
      {      
        // get the current location, instead of using the vector. Less jerky when transition place
        if (mvStat.isFirstTime || mvStat.isInRecursiveState)
        {
          this->itPath = this->deqPoints.begin();  // v3.0.213.7 // replaced this->path.begin(); // should point to the starting point
          mvStat.isFirstTime = false;

          if (mvStat.isInRecursiveState)
            mvStat.pointFrom.clone(this->displayCoordinate); // v3.0.207.3 for smooth transition.
          else
            mvStat.pointFrom.clone((*itPath)); // = new PointXY();

          //mvStat.pointFrom.setSpeedInFts(mvStat.pointTo.getSpeedFts()); // the speed from the Target point // v3.0.207.2 commented out line so speed will be taken from point values.
          // deprecated in v3.0.207.3 - I think it is not needed
          //mvStat.pointFrom.pitch = this->displayCoordinate.pitch; //  mvStat.pointTo.pitch; // pitch from data file  // v3.0.207.2 data should be taken from Displayed object for better transition
          //mvStat.pointFrom.roll = this->displayCoordinate.roll; // mvStat.pointTo.roll; // roll from data file // v3.0.207.2 data should be taken from Displayed object for better transition
          //mvStat.pointFrom.heading = this->displayCoordinate.heading; // mvStat.pointTo.heading; // heading from data file // v3.0.207.2 data should be taken from Displayed object for better transition
          // end deprecation in v3.0.207.3

          //mvStat.pointFrom.timeToWaitOnPoint = mvStat.pointTo.timeToWaitOnPoint; // how much time to wait on new Point // v3.0.207.2 TODO: need to evaluate if needed

          mvStat.pointFrom.calcSimLocalData();
        }
        else
        {
          mvStat.prevPoint.clone(this->displayCoordinate); // v3.0.207.3
          mvStat.pointFrom.clone(this->displayCoordinate); 
        }

        //this->displayCoordinate.clone(mvStat.pointFrom); // NO NEED to add this since we clone these values in next evaluation

      } // end if more than 1 point in path
      else
      {
        // Start from the beginning
        mvStat.isMoving = false; // v3.0.207.4 flag object as not moving since only one point
        this->itPath = this->deqPoints.begin(); // v3.0.213.7 // replaced  this->path.begin();
        mvStat.pointFrom.clone((*itPath));
      }


      /////////////////////////////////////////////////////
      /// Set pointTo and calculate base moving info
      /////////////////////////////////////////////////////
      if (!mvStat.hasReachedLastPoint)
      {
        
        //isInRecursiveState = object need to cycle between points
        if (mvStat.isInRecursiveState)
        {
          mvStat.currentPointNo = 0;
          mvStat.isInRecursiveState = false;
        }
        else
        {
          ++mvStat.currentPointNo;
          ++itPath;  // next point for next iteration.
        }

        // validate we have not reached end
        if (itPathEnd == itPath)
          mvStat.hasReachedLastPoint = true;
        else
        {
          mvStat.pointTo.clone((*itPath)); // store pointTo

          // Calculate new statistic info ( S V T )
          calcNewCourseBetweenTwoPointsOnVector(); // duplicated in this function
          mvStat.hasReachedPointTo = false;
          mvStat.isMoving = true; // v3.0.207.4 flag object as moving

          // set wait timer
          if (mvStat.pointFrom.timeToWaitOnPoint_sec == 0.0f)
            mvStat.waitTimer.reset();
          else
          {
            mvStat.waitTimer.reset();
            missionx::Timer::start(mvStat.waitTimer, mvStat.pointFrom.timeToWaitOnPoint_sec, "Obj3d_Wait_Timer_" + this->getName()); // Setting Wait Time Values
          }


          // init timer ifoif timer is running.
          if (mvStat.timer.isRunning() && !mvStat.waitTimer.isRunning())
          {
            missionx::Timer::start(mvStat.timer, 0, "Obj3d_timer_" + this->getName());

            mvStat.lastZuluStartDraw = 0.0f; //mvStat.timer->getZuluStartTime();
            mvStat.timeOnVector = 0;
          }

          mvStat.prevPoint.setLat( mvStat.pointFrom.getLat() );
          mvStat.prevPoint.setLon( mvStat.pointFrom.getLon() );

          this->displayCoordinate.clone(mvStat.pointFrom);
        } // end if point is not the last one


      } // end if not hasReachedLastPoint
      //else // reached last point in "path"

      if (mvStat.hasReachedLastPoint) // v3.0.207.3 we check again since it might be tht we just discovered we reached last point from previous logic.
      {
        // Do not change status to complete if it is a MOVING_TARGET type
        if ( this->getIsPathNeedToCycle() /* mvStat.isInRecursiveState*/ && mvStat.noOfPointsInPath > 1)
        {
          //Utils::logMsg ("[Moving3D nextPoint] Cycle 5" ); // debug
          mvStat.isInRecursiveState = true;
          initPathCycle(); // This is a recursive, might cause an issue.
          mvStat.isInRecursiveState = false;
        }
        else if (/*base_target_status.isStarted &&*/ !mvStat.isInRecursiveState) // if started at least 1 iteration
        {
          //Utils::logMsg ("[Moving3D nextPoint] Not Cycle 6" ); // debug
          mvStat.hasReachedLastPoint = true;
          mvStat.isMoving = false; // v3.0.207.4 flag object as not moving since only one point
        }

        this->displayCoordinate.clone(mvStat.pointFrom); // v3.0.207.2 // where to place 3D Object

      } // end if reached last point in path

    } // end if rendering instance

  } // end if next point need to be calculated


// end nextPoint
} 

missionx::Point & missionx::obj3d::getCurrentCoordination()
{
  return this->displayCoordinate;
}


double missionx::obj3d::getLat()
{
  return this->displayCoordinate.getLat();
}


double missionx::obj3d::getLong()
{
  return this->displayCoordinate.getLon();
}


double missionx::obj3d::getElevInFeet()
{
  return this->displayCoordinate.getElevationInFeet();
}


double missionx::obj3d::getElevInMeter()
{
  return this->displayCoordinate.getElevationInMeter();
}

std::string missionx::obj3d::getPropKeepUntilLeg()
{
  static std::string err;
  static std::string result;
  result.clear();

  // v3.0.221.15rc5 add LEG compatibility
  if (this->hasProperty(mxconst::ATTRIB_KEEP_UNTIL_LEG)) 
    result = this->getPropertyValue(mxconst::ATTRIB_KEEP_UNTIL_LEG, err);  
  if (result.empty()) // v3.0.221.15rc5 add LEG compatibility
    result = this->getPropertyValue(mxconst::ATTRIB_KEEP_UNTIL_GOAL, err);

  if (!err.empty())
    Log::logMsg(err);

  return result;
}

std::string missionx::obj3d::getPropLinkTask()
{
  static std::string err;
  static std::string result;
  result.clear();

  result = this->getPropertyValue(mxconst::ATTRIB_LINK_TASK, err);
  if (!err.empty())
    Log::logMsg(err);

  return result;
}

std::string missionx::obj3d::getPropLinkToObjectiveName()
{
  static std::string err;
  static std::string result;
  result.clear();

  result = this->getPropertyValue(mxconst::PROP_LINK_OBJECTIVE_NAME, err);
  if (!err.empty())
    Log::logMsg(err);

  return result;
}

bool missionx::obj3d::getIsPathNeedToCycle()
{
  static std::string err;
  static bool result;
  result = this->getPropertyValue<bool>(mxconst::ATTRIB_CYCLE, err);
#if defined (_DEBUG) || defined (DEBUG)  
  if (!err.empty())
    Log::logMsgErr(err);
#endif

  return result;
}

bool missionx::obj3d::getHideObject()
{
  static std::string err;
  static bool result;
  result = this->getPropertyValue<bool>(mxconst::ATTRIB_HIDE, err);
#if defined (_DEBUG) || defined (DEBUG)  
  if (!err.empty())
    Log::logMsgErr(err);
#endif

  return result;
}

void missionx::obj3d::findAndSetTerrainElevation_to_DisplayCoordination()
{
  static std::string err;
  static XPLMProbeResult result = xplm_ProbeError;
  static double elev_mt;

  elev_mt = 0.0;

  double above_ground_ft_prop = 0.0;
  if (this->hasProperty(mxconst::ATTRIB_ELEV_ABOVE_GROUND_FT))
    above_ground_ft_prop = this->getPropertyValue<double>(mxconst::ATTRIB_ELEV_ABOVE_GROUND_FT, err);

  elev_mt = UtilsGraph::getTerrainElevInMeter_FromPoint(this->displayCoordinate, result);

  this->displayCoordinate.setElevationMt(elev_mt);
  if (above_ground_ft_prop != 0.0)
    this->displayCoordinate.setElevationFt(this->displayCoordinate.getElevationInFeet() + above_ground_ft_prop); // add "above ground" elevation

  this->displayCoordinate.elevWasProbed = true;

#ifdef DEBUG_MOVE // defined (_DEBUG) || defined (DEBUG)  
    Utils::logMsg("Terrain Probed, elevation [" + mxUtils::formatNumber<double>(this->displayCoordinate.getElevationInMeter()) + "mt | " + mxUtils::formatNumber<double>(this->displayCoordinate.getElevationInFeet()) + "ft]" +  mxconst::UNIX_EOL);
#endif

  
}


void missionx::obj3d::positionInstancedObject()
{

  static float dummy = 0.0f;
  std::string instName = this->getInstanceName();

  this->displayCoordinate.calcSimLocalData(); // just in case

  // prepare XPLMDrawInfo_t
  this->dr.structSize = sizeof(dr);
  this->dr.x = (float)this->displayCoordinate.local_x;
  this->dr.y = (float)this->displayCoordinate.local_y;
  this->dr.z = (float)this->displayCoordinate.local_z;
  this->dr.pitch    = (float)this->displayCoordinate.getPitch();
  this->dr.heading  = (float)this->displayCoordinate.getHeading();
  this->dr.roll     = (float)this->displayCoordinate.getRoll();

  
  // Show Instance
  if (this->g_instance_ref)
  {
    XPLMInstanceSetPosition(this->g_instance_ref, &this->dr, &dummy);
#ifdef DEBUG_MOVE // defined (_DEBUG) || defined (DEBUG)  
    Log::logMsg("Positioned: " + instName + ", " + this->displayCoordinate.to_string_xy() + ", h: " + Utils::formatNumber<float>(this->dr.heading) + ", r: " + Utils::formatNumber<float>(this->dr.roll) + ", p: " + Utils::formatNumber<float>(this->dr.pitch));
#endif
  }



}

//void missionx::obj3d::positionInstancedObjectTest()
//{
//  static float dummy = 0.0f;
//  std::string instName = this->getInstanceName();
//
//  this->displayCoordinate.calcSimLocalData(); // just in case
//
//  drTest.x -= 0.1f;
//
//  this->dr.structSize = sizeof(dr);
//  this->dr.x = drTest.x;
//  this->dr.y = drTest.y;
//  this->dr.z = drTest.z;
//  this->dr.pitch = drTest.pitch;
//  this->dr.heading = drTest.heading;
//  this->dr.roll = drTest.roll;
//
//  // Show Instance
//  if (this->g_instance_ref)
//  {
//    XPLMInstanceSetPosition(this->g_instance_ref, &this->dr, &dummy);
//#if defined (_DEBUG) || defined (DEBUG)
//    Log::logMsg("Positioned: " + instName + ", " + this->displayCoordinate.to_string_xy());
//#endif
//  }
//}

//void missionx::obj3d::applyInstanceSpecificProperties(missionx::mxProperties & inProperties)
//{
//  static std::string err;
//  err.clear();
//
//
//  // loop over all properties and apply over 
//
//  this->overrideProperties(inProperties);
//
//  this->displayCoordinate = this->getStartLocationAttributes(); // apply new location info into displayCoordinate
//
//#if defined (DEBUG) || defined (_DEBUG)
//  Log::logMsg("Applied specific attributes to instance: " + this->getInstanceName()); // debug
//#endif
//}

//void missionx::obj3d::setIsInDisplayList(bool inVal)
//{
//    this->isInDisplayList = inVal;  
//}

//bool missionx::obj3d::getIsInDisplayList()
//{
//  return this->isInDisplayList;
//}

std::string missionx::obj3d::getInstanceName()
{
  std::string outError;
  std::string val = this->getPropertyValue(mxconst::ATTRIB_INSTANCE_NAME, outError);
  if (!outError.empty())
    Log::logMsgErr("[Error getInstName]" + outError);

  return val;
}

bool missionx::obj3d::isPlaneInDisplayDistance(missionx::Point &inPlanePoint)
{
  std::string err;
  double condDist = this->getPropertyValue<double>(ATTRIB_DISTANCE_TO_DISPLAY_NM, err);
  double dist = Point::calcDistanceBetween2Points(inPlanePoint, this->displayCoordinate);
 
  return (condDist >= dist);
}

missionx::Point missionx::obj3d::getStartLocationAttributes()
{
  static std::string err;
  err.clear();
  missionx::Point p; // (this->getPropertyValue<double>(ATTRIB_LAT, err), this->getPropertyValue<double>(ATTRIB_LONG, err), this->getPropertyValue<double>(ATTRIB_ELEV_FT, err));
  p.setLat( this->getPropertyValue<double>(ATTRIB_LAT, err) );
  p.setLon( this->getPropertyValue<double>(ATTRIB_LONG, err) );
  p.setElevationFt(this->getPropertyValue<double>(ATTRIB_ELEV_FT, err));

  p.setHeading( (float)this->getPropertyValue<double>(ATTRIB_HEADING_PSI, err) );
  p.setPitch((float)this->getPropertyValue<double>(ATTRIB_PITCH, err));
  p.setRoll((float)this->getPropertyValue<double>(ATTRIB_ROLL, err) );

  p.storeData();
  p.calcSimLocalData();

  return p;
}


void missionx::obj3d::storeCoreAttribAsProperties()
{
  // store plane location
  this->setStringProperty(mxconst::PROP_CURRENT_LOCATION, this->displayCoordinate.format_point_to_savepoint() );
  this->setIntProperty(mxconst::ATTRIB_OBJ3D_TYPE, this->obj3dType);
  this->setBoolProperty(mxconst::PROP_DISPLAY_DEFAULT_OBJECT_FILE_OVER_ALTERNATE, this->displayDefaultObjectFileOverAlternate); // v3.0.200

}


void missionx::obj3d::applyPropertiesToLocal()
{
  static std::string pValue;
  static std::string err;
  pValue.clear();
  err.clear();

  // obje3d type
  if (this->hasProperty(ATTRIB_OBJ3D_TYPE))
  {
    obj3d::obj3d_type mType = (obj3d::obj3d_type)(this->getPropertyValue<int>(ATTRIB_OBJ3D_TYPE, err) );    
    this->obj3dType = mType;
  }     


  // displayDefaultObjectFileOverAlternate
  if (this->hasProperty(PROP_DISPLAY_DEFAULT_OBJECT_FILE_OVER_ALTERNATE))
    this->displayDefaultObjectFileOverAlternate = this->getPropertyValue<bool>(PROP_DISPLAY_DEFAULT_OBJECT_FILE_OVER_ALTERNATE, err);
  else
    this->displayDefaultObjectFileOverAlternate = true;


  // read special data for static obj3d type 
  if (this->obj3dType == obj3d::static_obj) // obj3d::moving_obj)
  { 
    // coordinations from properties
    this->startLocation = getStartLocationAttributes();
    this->displayCoordinate.clone (startLocation); // set current coordinate to LAT/LONG/ELEV + Heading/Pitch/Roll    
  }
  else // v3.0.202  // obj3d::moving_obj
  {
    // v3.0.207.2: start location point is taken from "attributes" if static, and from "path" if moving. For moving, if path is empty then we take from attributes.
    if (this->deqPoints.empty()) // v3.0.213.7 // replaced (this->path.empty())
      this->startLocation = getStartLocationAttributes(); // moving object receive display attribute from other properties (TBD)
    else
    {
      this->startLocation = this->deqPoints.at(0); // v3.0.213.7 // replaced this->path.at(0); // get start location from PATH container and not the attributes in the XML data file
    }
  } // end handling type of 3D Object

  // cond_script attribute outcome
  if (this->hasProperty(PROP_SCRIPT_COND_MET_B))
    this->isScriptCondMet = this->getIsScriptCondMetPropertyValue(); // returned value of PROP_SCRIPT_COND_MET_B
  else
    this->isScriptCondMet = true;
}


void missionx::obj3d::saveCheckpoint(IXMLNode & inParent)
{  
  this->storeCoreAttribAsProperties();

  IXMLNode xChild = inParent.addChild(ELEMENT_OBJ3D.c_str());
  xChild.addAttribute(ATTRIB_NAME.c_str(), getName().c_str());
  // store properties
  obj3d::mxProperties::saveCheckpoint(xChild);


  missionx::Points::saveCheckpoint(xChild); // v3.0.213.7 save path points
  //IXMLNode xPath = xChild.addChild(mxconst::ELEMENT_PATH.c_str());
  //if (!xPath.isEmpty())
  //{
  //  //for (auto p : this->deqPoints) // v3.0.213.7 // replaced  (auto p : this->path)
  //  //  p.saveCheckpoint(xPath);
  //}


  // save instance data
  if (this->g_instance_ref)
  {
    // 1. save path
    // 2. save moving data (pointFrom, pointTo, currentPointNo, instance_name)
    // prepare instance information    
    this->instProperties.setStringProperty(mxconst::ATTRIB_NAME, this->getInstanceName());
    this->instProperties.setIntProperty(mxconst::PROP_CURRENT_POINT_NO, this->mvStat.currentPointNo);
    this->instProperties.setBoolProperty(mxconst::PROP_LOADED_FROM_CHECKPOINT, true); // v3.0.213.7 For templates and static objects it is not important. For instances we want to know that data is from checkpoint so we won't need to initialize ALL parameters from default.

    IXMLNode xInstanceElement = xChild.addChild(mxconst::PROP_INSTANCE_DATA_ELEMENT.c_str());
    xInstanceElement.addAttribute(mxconst::ATTRIB_NAME.c_str(), this->getInstanceName().c_str());
    // add instance properties to it before point data
    this->instProperties.saveCheckpoint(xInstanceElement); // add properties element


    // add instance point locations
    IXMLNode xPointFrom = xInstanceElement.addChild(mxconst::PROP_POINT_FROM.c_str());
    this->mvStat.pointFrom.saveCheckpoint(xPointFrom);
    IXMLNode xPointTo = xInstanceElement.addChild(mxconst::PROP_POINT_TO.c_str());
    this->mvStat.pointTo.saveCheckpoint(xPointTo);
    // add instance last display point
    IXMLNode xPointCurrent = xInstanceElement.addChild(mxconst::PROP_CURRENT_LOCATION.c_str());
    this->displayCoordinate.saveCheckpoint(xPointCurrent);

  }
  
}


bool missionx::obj3d::loadCheckpoint(ITCXMLNode & inParent, std::string & outErr)
{
  bool loadSuccess = true;
  loadSuccess = mxProperties::loadCheckpoint(inParent, outErr);
  
  // read path - holds the list of points if any
  missionx::Points::loadCheckpoint(inParent, outErr); // TODO: make sure that all attributes are read by this function and not just lat/long/elev

  // read instance properties
  // 1. check if has instance name stored
  // 2. read instance properties
  // 3. read pointFrom, pointTo, currentPointNo, instance_name
  // 4. (?) load timer information (?) I think we should be able to calculate the timer based on the 2 points (to and current location = S, + speed) S = V*T => T=S/V
  if (!(this->getPropertyValue(ATTRIB_INSTANCE_NAME, outErr).empty()))
  {
    ITCXMLNode xInstanceElement = inParent.getChildNode(mxconst::PROP_INSTANCE_DATA_ELEMENT.c_str());
    if (!xInstanceElement.isEmpty())
    {
      ITCXMLNode xPoint;
      this->instProperties.loadCheckpoint(xInstanceElement, outErr);


      ITCXMLNode xPointFrom = xInstanceElement.getChildNode(mxconst::PROP_POINT_FROM.c_str());
      if (!xPointFrom.isEmpty())
      {
        this->mvStat.pointFrom.readChildPoint(xPointFrom, outErr);
      }
        

      ITCXMLNode xPointTo = xInstanceElement.getChildNode(mxconst::PROP_POINT_TO.c_str());
      if (!xPointTo.isEmpty())
      {
        this->mvStat.pointTo.readChildPoint(xPointTo, outErr);
      }
        

      //ITCXMLNode xPointCurrent = xInstanceElement.getChildNode(mxconst::PROP_CURRENT_LOCATION.c_str());
      //if (!xPointCurrent.isEmpty())
      //  this->displayCoordinate.loadCheckpoint(xPointFrom, outErr);

    }
    
  }



  // apply properties
  this->applyPropertiesToLocal();

  return loadSuccess;
}


std::string missionx::obj3d::to_string()
{
  std::string format; format.clear();

  format = "3D Object: " + this->getName() + "\"" + UNIX_EOL;
  size_t length = format.length();
  format += std::string("").append(length, '=') + UNIX_EOL;

  format += mxProperties::to_string() + UNIX_EOL;

  return format;
}



