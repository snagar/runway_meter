#ifndef TIME_H_
#define TIME_H_

#pragma once

/*******
TimeFragment.h class should only hold the current X-Plane time and no more once it was initialized.
All calculation on miliseconds and delta time, should be done in Timer Class.
*/

#include <string>
#include "XPLMUtilities.h"
#include "XPLMDataAccess.h"

#include "Utils.h"
#include "rwm_const.h"
#include <time.h>
#include <chrono>
#include <ctime>

using namespace std;

namespace rwmeter
{
  // define state of timer // v3.0.207.1 // deprecated since we use rwmeter::mx_timer_state
  //typedef enum _time_status
  //{
  //  time_not_set,
  //  time_set
  //} mx_time_status;

  /**
  TimeFragment class stores current time information in X-Plane (a snapshot of the time) this is not clock information.
  It stores:
  1. timePassedFromSimStart: how many seconds passed from sim start
  2. zuluTime_sec: seconds from zulu midnight
  3. dayInYear: days from start of year.
  4. os_clock: OS clock (store seconds not time)
  5. os_clock_end: OS clock (store seconds not time)
  */
  class TimeFragment
  {
  private:
    const rwmeter::rwm_const drefConst;
//    std::chrono::time_point<std::chrono::steady_clock> os_clock_end;
    
  public:
    using nano_s = std::chrono::nanoseconds;
    using micro_s = std::chrono::microseconds;
    using milli_s = std::chrono::milliseconds;
    using seconds = std::chrono::seconds;
    using minutes = std::chrono::minutes;
    using hours = std::chrono::hours;
    
    // timer core attributes
    float timePassedFromSimStart; // how many seconds passed from the start of the sim
    float zuluTime_sec; // holds current iteration zulu seconds from midnight.
    int   dayInYear; // store days from start of year.

    // Chrono
    std::chrono::time_point<std::chrono::steady_clock> os_clock;
    std::chrono::milliseconds deltaOsClock_milli;


    void clone(TimeFragment &in_time_fragment)
    {
      this->timePassedFromSimStart = in_time_fragment.timePassedFromSimStart;
      this->zuluTime_sec = in_time_fragment.zuluTime_sec;
      this->dayInYear = in_time_fragment.dayInYear;

      this->os_clock = in_time_fragment.os_clock;
    }

    // Operators ///////////

    void operator=(TimeFragment &in_time_fragment) { clone(in_time_fragment); }


#ifdef MX_EXE
    float operator- (TimeFragment &inTime)
    { 
      auto duration = chrono::duration_cast<chrono::milliseconds>(this->os_clock - inTime.os_clock).count();
      return (float)duration/1000; // milliseconds to seconds
    }
#else
    // return (now - then) = (currentDayInYear-thenDayInYear)*(secondsInDay_86400) + nowSecondsFromMidnight - thenSecondsFromMidnight     
    float operator- (TimeFragment &inTime) { 
      float delta_time = 0.0f;
      float current_seconds = this->dayInYear * rwmeter::SECONDS_IN_1DAY + this->zuluTime_sec;
      float begin_seconds = inTime.dayInYear * rwmeter::SECONDS_IN_1DAY + inTime.zuluTime_sec;

      //float current_seconds = (this->dayInYear < inTime.dayInYear)? (rwmeter::DAYS_IN_YEAR_365 + this->dayInYear) * rwmeter::SECONDS_IN_1DAY + this->zuluTime_sec: this->dayInYear*rwmeter::SECONDS_IN_1DAY + this->zuluTime_sec;
      // try to fix cases where zulu time has advanced to next day but local is still on previous day
      if (this->zuluTime_sec < inTime.zuluTime_sec)
      {
        if ( this->dayInYear <= inTime.dayInYear )
          ++this->dayInYear;

        current_seconds = this->dayInYear* rwmeter::SECONDS_IN_1DAY + this->zuluTime_sec;
      }


      if (current_seconds < begin_seconds && ( (begin_seconds - current_seconds) < rwmeter::SECONDS_IN_1DAY))
      {
        current_seconds += rwmeter::SECONDS_IN_1DAY;
      }
      
      delta_time = (current_seconds - begin_seconds < 0.0f)? 0.5f : current_seconds - begin_seconds; // we won't return minus value, we will return fragment of a second just in case. Not sure this is a good idea.
            
      //return   (float)((this->dayInYear - inTime.dayInYear)*rwmeter::SECONDS_IN_1DAY) +  this->zuluTime_sec - inTime.zuluTime_sec; 
      return delta_time;
    }
#endif
    
    // Currently return the delta time in milliseconds between two osClock (fragment time)
    static float getOsDurationBetween2TimeFragments(TimeFragment &t2, TimeFragment &t1)
    {
      //std::chrono::time_point<std::chrono::steady_clock> os_clock = std::chrono::steady_clock::now(); // fetch NOW
      auto duration = chrono::duration_cast<chrono::milliseconds>(t2.os_clock - t1.os_clock).count();
      return (float)duration / 1000; // milliseconds to seconds
    }

    static float getOsDurationBetweenNowAndStart(TimeFragment &inStartTimeFragment)
    {
      std::chrono::time_point<std::chrono::steady_clock> os_clock = std::chrono::steady_clock::now(); // fetch NOW
      auto duration = chrono::duration_cast<chrono::milliseconds>(os_clock - inStartTimeFragment.os_clock).count();
      return (float)duration / 1000; // milliseconds to seconds
    }

    // initialize class. There is no default contractor
    static void init(TimeFragment &outTime)
    {
#ifdef MX_EXE
      outTime.os_clock = std::chrono::steady_clock::now();
      auto m = chrono::duration_cast<chrono::milliseconds>(outTime.os_clock.time_since_epoch()).count();
      auto mili = chrono::duration_cast<chrono::milliseconds>(std::chrono::steady_clock::now().time_since_epoch()).count();
      //std::time_t t = last_midnight();
      outTime.zuluTime_sec = 0; // (float)t;
      outTime.dayInYear = 1; // dummy day in year
#else
      outTime.os_clock = std::chrono::steady_clock::now();
      //std::chrono::milliseconds duration = std::chrono::duration_cast<milli_s>(outTime.os_clock).count();
      outTime.zuluTime_sec = XPLMGetDataf(outTime.drefConst.dref_zulu_time_sec_f);
      outTime.dayInYear = XPLMGetDatai(outTime.drefConst.dref_local_date_days_i); // this is a problem since we pick zulu time but local day, and sometime they do not reflect correctly.

#endif

    }

    float getTimePassedSec()
    {
      return zuluTime_sec;        
    }

  };

}

#endif
