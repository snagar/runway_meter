#ifndef DATA_MANAGER_H_
#define DATA_MANAGER_H_

#pragma once

#include <queue>
#include<set>

//#include "../logic/dataref_param.h"
#include "../io/Log.hpp"
#include "../io/OptimizeAptDat.h"
#include "Timer.hpp"

using namespace rwmeter;
//using namespace mxconst;

namespace rwmeter
{


/**
rw_data_manager holds ALL STATIC information that we want to share with the plugin, example: tasks, objective, folder information, global settings, seed ext parameters etc.
It sored the information in static maps, and has key functions to add tasks/objectives and goals to maps.
*/

  // set of commands that need to be execute from "plugin" or "flc" phase since we don't want to interrupt flow of code
  typedef enum _flc_commands : uint8_t
  {
    start_rwmeter,
    disable_aptdat_optimize_menu,
    enable_aptdat_optimize_menu, 
    gather_random_airport_mainThread, 
    stop_rwmeter
  } mx_flc_pre_command;

  //typedef enum _mission_state : uint8_t
  //{
  //  mission_undefined = 0,
  //  mission_stopped = 2,
  //  mission_loaded = 10,
  //  mission_loaded_from_savepoint = 20,
  //  mission_running = 30,
  //  mission_completed_success = 40,
  //  mission_completed_failure = 50,
  //  stop_all_asynch_processes = 60,
  //  process_points = 65,
  //  mission_aborted = 70
  //} mx_mission_state_enum;


  class TimeLaps
  {
  private:
    int   start_local_date_days_i; // store the day before starting timelap    
    float local_time_sec_f;
    float zulu_time_sec_f;
    static rwm_const dc;

    int cycleCounter;


  public:
    virtual ~TimeLaps() {};

    bool flag_isActive;
    float seconds_per_lap_f;
    float total_time_to_add_in_seconds; // total seconds that we need to cover. "total_time_to_add_in_seconds = desired_local_time - current_local_time"
    float expected_time_after_addition_in_seconds; // total_time_to_add_in_seconds + local_time_seconds. We should not touch this value during iteration since at the end we will decide if to add local day.
    float total_passed; // how much time passed so far. Increased by "seconds_per_lap" every second.
    //int   overall_laps_time_in_sec; // designer provide the seconds and we divide and initialize "seconds_per_lap = (overall_laps_time_in_sec / total_time_to_add_in_seconds)"
    int   cycles; // cycles = overall_laps_time_in_sec / total_time_to_add_in_seconds. How many seconds the timer will have to run.

    Timer timer;

    int futureDayOfYearAfterTimeLapse; // after timelapse to hours change day of year. "-1" means do not change.

    TimeLaps() {
      this->flag_isActive = false;
      this->expected_time_after_addition_in_seconds = 0;
      this->seconds_per_lap_f = 0.0f;
      this->total_time_to_add_in_seconds = 0.0f;
      this->total_passed = 0.0f;
      //this->overall_laps_time_in_sec = 0;
      this->cycles = 0;

      this->timer.reset();

      this->start_local_date_days_i = 0;
      this->local_time_sec_f = 0.0f;
      this->zulu_time_sec_f = 0.0f;

      this->cycleCounter = 0;

      this->futureDayOfYearAfterTimeLapse = -1;
    };

    void reset()
    {
      this->flag_isActive = false;
      this->expected_time_after_addition_in_seconds = 0;
      this->seconds_per_lap_f = 0.0f;
      this->total_time_to_add_in_seconds = 0.0f;
      this->total_passed = 0.0f;
      //this->overall_laps_time_in_sec = 0;
      this->cycles = 0;
      this->timer.reset();

      this->start_local_date_days_i = 0;
      this->local_time_sec_f = 0.0f;
      this->zulu_time_sec_f = 0.0f;

      this->cycleCounter = 0;

      this->futureDayOfYearAfterTimeLapse = -1;
    }

    std::string set_date_and_hours(std::string inDateAndTime); // 
    std::string timelap_add_minutes(int inMinutesToAdd, int inHowManyCycles); // how many minutes to add and in how many seconds to do that. We will calculate the seconds to pass per cycle.
    void flc_timelap(); // test timer and progress UTC time + local_day accordingly


    std::string timelapse_to_local_hour(int inFutureLocalHour, int inFutureMinutes, int inHowManyCycles); // provide the local hour and minutes you want to progress and in how fast. We will calculate the seconds to pass per cycle.

    
};



/////////////////////////////////////////////////////
/////////////////////////////////////////////////////
/////////////////////////////////////////////////////
/////////////////////////////////////////////////////
/////////////////////////////////////////////////////


class rw_data_manager
{
private:
  
public:
  rw_data_manager();
  virtual ~rw_data_manager();

  // draw callback flag
  static bool needDrawCallBack; // Ben Supnik asked for this https://developer.x-plane.com/2018/01/plugins-do-not-register-drawing-callbacks-that-do-not-draw/

  // VR Related
  static XPLMDataRef g_vr_dref; // holds if in VR mode
  static void flc_vr_state();
  static int prev_vr_is_enabled_state; // store last loopback state of the VR

  // TimeLaps
  static rwmeter::TimeLaps timelaps;
  static std::string set_local_time(int inHours, int inMinutes, int inDayOfYear_0_364);

  // APT DAT OPTIMIZATION FLAGS
  static bool flag_apt_dat_optimization_is_running;
  static bool flag_generate_engine_is_running;
  static std::map<std::string, int> mapIndexNavAids;
  static std::map< std::string, std::list<std::string> > cachedNavInfo_map;
  // core attributes



private:
  
}; // end class

}
#endif
