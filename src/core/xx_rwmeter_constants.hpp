#ifndef XX_MY_CONSTANTS_HPP
#define XX_MY_CONSTANTS_HPP
#pragma once

#include "base_c_includes.h"
#include <deque>
#include "XPLMDisplay.h"
#include "XPLMDataAccess.h"
#include "vr/mxvr.h"

namespace rwmeter
{

#define PLUGIN_VER_MAJOR "0"
#define PLUGIN_VER_MINOR "1"
#if defined(DEBUG) || defined (_DEBUG)
  #define PLUGIN_REVISION  "1"
#else
  #define PLUGIN_REVISION  "1"
#endif

  const static std::string APP_NAME = "Runway Meter";

  const static std::string PLUGIN_DIR_NAME = "rwmeter";
  const static std::string PLUGIN_VER = "100";

  const static std::string TEXTURE_DIR_NAME = "bitmap";


  // GLOBAL CONSTANTS
  const static double PI = 3.1415926535897932384626433832795; // from windows calculator
  const static double PI2 = 6.283185307179586476925286766559; // FOR DROPPING calculation


  //static double Gravity=-9.81;

  const static float EARTH_RADIUS_M = 6378137.0f;


  const float RadToDeg = (float)(180.0f / PI); // 57.295779513082320876798154814105
  const float DegToRad = 1.0f / RadToDeg; // 0.01745329251994329576923690768489
  const float feet2meter = 0.3048f; // 1.0f / 3.28083f;
  const float meter2feet = 3.28083f;
  const float meter2nm = 0.000539957f;
  const float nm2meter = 1852.0f;
  const float nm2km = 1.852f;
  const float km2nm = 0.539957f;
  const float OneNmh2meterInSecond = 0.5144444f; // 

  const float kmh2fts = 0.911344f; // v3.0.202
  const float fts2kmh = 1.09728f; // v3.0.202


  const int   OUT_OF_BOUNDING_ALERT_TIMER_SEC = 30; //30 sec. alert will broadcast every 30 seconds
  const float MISSIONX_DOUBLE_CLICK = 0.9f;

  const static std::string EMPTY_STRING = "";
  //const static std::string condStr[6] = { "lt;","lte;","=", "!=", "gt;","gte;" };
  const static std::string MSG_SEP = ";----------------;;";
  const static std::string PREF_FILE_NAME = "rwmeter_pref.xml";

  //const static int BRIEFER_DESC_CHAR_WIDTH = 70;

  // Global Char Buffer for Login
  const int LOG_BUFF_SIZE = 2048;
  static char LOG_BUFF[LOG_BUFF_SIZE];


  // 21638.7 Nautical Miles; 40075.0 km  24901.5 miles
  const static float EQUATER_LEN_NM = 21638.7f;
  const static float EARTH_AVG_RADIUS_NM = 3440.07019148103f;


  const static intptr_t DAYS_IN_YEAR_365 = 365;
  const static intptr_t SECONDS_IN_1HOUR_3600 = 3600;
  const static intptr_t SECONDS_IN_1MINUTE = 60;
  const static intptr_t SECONDS_IN_1DAY = 86400;
  const static intptr_t HOURS_IN_A_DAY_24 = 24;

  const double NEARLY_ZERO = 0.0000000001; // dataref_manager

  const static unsigned int NUM_CIRCLE_POINTS = 359; // v3.0.202a
  
  /////////////// Added from Mission-X mxconst

  const static std::string XPLANE_SCENERY_INI_FILENAME = "scenery_packs.ini";
  const static std::string SCENERY_PACK_ = "SCENERY_PACK ";

  const static std::string MX_TRUE="true"; 
  const static std::string MX_YES="yes"; 
  const static std::string MX_FALSE="false"; 
  const static std::string MX_NO ="no";


  const static std::string UNIX_EOL = "\n"; // used in Unix like OSes for end of line
  const static std::string WIN_EOL = "\r\n"; // Windows end of line
  const static std::string QM = "\""; // Quotation Mark
  const static std::string SPACE = " "; // SPACE
  const static std::string FOLDER_SEPARATOR = "/"; // folder separator

  //const static std::string FLD_MISSIONX_DATA_PATH = "missionx_data_path"; // v3.0.213.7 deprecated - not in use anymore
  const static std::string FLD_MISSIONX_SAVE_PATH = "missionx_save_path";
  const static std::string FLD_MISSIONX_LOG_PATH = "missionx_log_path";
  const static std::string FLD_MISSIONX_SAVEPOINT_PATH = "missionx_savepoint_path";
  const static std::string FLD_MISSIONX_SAVEPOINT_DREF = "missionx_savepoint_dref";
  const static std::string FLD_MISSIONS_ROOT_PATH = "missions_root_folder"; // currently "custom scenery/missionx"
  const static std::string FLD_RANDOM_TEMPLATES_PATH = "missionx_random_templates_folder_path"; // v3.0.217.1 currently "Resources/plugin/missionx/templates"
  const static std::string FLD_RANDOM_MISSION_PATH = "missionx_random_mission_folder_path"; // v3.0.217.1 crrently "custom scenery/missionx/random"
  const static std::string FLD_CUSTOM_SCENERY_FOLDER_PATH = "xplane_custom_scenery_folder_path"; // v3.0.219.9 currently "custom scenery"
  const static std::string FLD_DEFAULT_APTDATA_PATH = "xplane_default_aptdata_folder_path"; // v3.0.219.9 currently "xp11/Resources/default scenery/default apt dat/"
  const static std::string PROP_MISSIONX_PATH = "missionx_path";

  ////// ENUMS //////

  typedef enum _mx_btn_colors : uint8_t
  {
    white,
    yellow,
    red,
    green,
    blue,
    purple,
    orange,
    black
  } mx_btn_colors;

  typedef enum _random_thread_wait_state : uint8_t
  {
    not_waiting = 0, // we need this if designer used wrong string for channel
    waiting_for_plugin_callback_job,
    finished_plugin_callback_job
  } mx_random_thread_wait_state_enum;


  typedef enum _cue_actions : uint8_t
  {
    cue_action_none,
    cue_action_first_time ,
    cue_action_refresh 
  }cue_actions_enum;

  typedef enum _message_channel_type : uint8_t
  {
    no_type = 0, // we need this if designer used wrong string for channel
    comm = 1, // comm is also mxpad channel
    background = 2
    //pad = 3
  } mx_message_channel_type_enum;


  // describe message state and progress.
  // Some of the stages are internal and very short.
  typedef enum _missionx_message_state : uint8_t
  {
    msg_undefined,
    msg_not_broadcast, // loaded but not dispatched into message pool yet
    msg_in_pool,
    msg_text_is_ready,
    msg_prepare_channels,
    msg_channels_need_to_finish_loading,
    msg_is_ready_to_be_played,
    msg_broadcast_once,
    msg_broadcast_few_times
  } mx_message_state_enum;


  typedef enum _mxWindowActions : uint8_t
  {
    ACTION_NONE, // initialize action
    ACTION_TOGGLE_WINDOW, // only change window state (show / hide ) should not modify layer.
    ACTION_TOGGLE_BRIEFER, // v3.0.201 when a command is called then it means we want to see GOAL/LEG info and such. Layer should be goal_info for running
    ACTION_TOGGLE_BRIEFER_OPTIONS, // v3.0.203.5 display Options window in Briefer
    ACTION_HIDE_WINDOW, // v3.0.160
    ACTION_SHOW_WINDOW, // v3.0.201
    ACTION_CANCEL, // v3.0.141
    ACTION_CLOSE_WINDOW, // v3.0.145
    ACTION_QUIT_MISSION, // v3.0.145
    ACTION_LOAD_MISSION, // v3.0.141
    ACTION_CREATE_SAVEPOINT, // v3.0.151
    ACTION_LOAD_SAVEPOINT, // v3.0.151
    ACTION_START_MISSION, // v3.0.141
    ACTION_START_RANDOM_MISSION, // v3.0.219.1
    ACTION_TOGGLE_MAP, // v3.0.201
    ACTION_TOGGLE_INVENTORY, // v3.0.213.2
    ACTION_COMMIT_TRANSFER, // v3.0.213.2
    ACTION_TOGGLE_MISSION_GENERATOR, // v3.0.217.1
    ACTION_GENERATE_RANDOM_MISSION, // v3.0.217.2
    ACTION_SET_LEG_INFO, // v3.0.221.15rc5 support leg // v3.0.221.6 in VR mode we won't close Mission-X when closing inventory or map, instead we will set goal info layer
    ACTION_SET_MISSION_LIST, // v3.0.221.6 in VR mode we won't close Mission-X when closing Mission Generator.
    ACTION_TOGGLE_BRIEFER_MXPAD // v3.0.221.7 in VR mode 

  } mx_window_actions; // for all windows. some actions are specific and some shared
  
  // types of cue triggers, this will help with GL color selecting. I hope to also have combinations of colors
  typedef enum _cue_types : uint8_t
  {
    cue_none    = 0,
    cue_trigger = 1, // yellow
    cue_task = 5, // green
    cue_mandatory_task = 7, // magenta
    cue_message = 10, // black
    cue_inventory = 12, // orange
    cue_script = 15 // orange

  } mx_cue_types;

  typedef enum _mx_property_type : uint8_t
  { 
    UNKNOWN = 0, 
    BOOL = 1, 
    CHAR = 2, 
    INT = 3, 
    FLOAT = 4, 
    DOUBLE = 5, 
    STRING = 6 
  } mx_property_type; // v3.0.217.2 moved from mxProperty

  typedef enum _mx_aptdat_meaning : uint8_t
  { 
    unknown = 0,
    unknown_n = 1, 
    unknown_s = 2, 
    unknown_d = 3,
    icao = 10,
    airport_header=11,
    lat=15,
    lon=16,
    heading=17,
    name=20,
    description=21,
    ramp=22,
    runway=24,
    helipad=25, // kind of runway. same format
    code=30
  } mx_aptdat_meaning; // v3.0.217.2 moved from mxProperty



  typedef struct _mxMouseState
  {
    int x = 0;
    int y = 0;
    XPLMMouseStatus mouseStatus = 0;
    bool buttonPressed = false;

    void reset()
    {
      x = 0;
      y = 0;
      mouseStatus = 0;
      buttonPressed = false;
    }
  }mx_mouse_state;


  typedef struct _messageLine_struct
  {
    std::string label;
    std::string label_position;
    std::string label_color;

    std::string message_text; // original message without splitting
                              //std::vector<std::string> textLines; // split message
    std::deque<std::string> textLines;

    _messageLine_struct()
    {
      init();
    }

    void init()
    {
      label.clear();
      label_position.clear();
      label_color.clear();
      message_text.clear();
      textLines.clear();
    }
    
  } messageLine;
  // end struct


  typedef struct _mx_location_3d_objects // v3.0.213.7 moved from read_mission_file class, to use in Point class too.
  {
    std::string distance_to_display_nm, keep_until_leg, cond_script;
    std::string lat, lon, elev, elev_above_ground;
    std::string heading, pitch, roll, speed; // v3.0.202
    std::string adjust_heading; // v3.0.207.5

    _mx_location_3d_objects()
    {
      lat = lon = elev = elev_above_ground = EMPTY_STRING;
      distance_to_display_nm = keep_until_leg = cond_script = EMPTY_STRING;
      heading = pitch = roll = speed = EMPTY_STRING;
      adjust_heading.clear(); // v3.0.207.5
    }
  } mx_location_3d_objects;


} // end missionx namespace

//namespace mxconst
//{
////#include <string>
////using namespace std;
//    
//    const static std::string UNIX_EOL = "\n"; // used in Unix like OSes for end of line
//    const static std::string WIN_EOL = "\r\n"; // Windows end of line
//    const static std::string QM = "\""; // Quotation Mark
//    const static std::string SPACE = " "; // SPACE
//    const static std::string FOLDER_SEPARATOR = "/"; // folder separator
//    const static std::string FOLDER_METADATA_NAME   = "briefer"; // v3.0.x name of the mandatory folder that the mission file/s will reside
//    const static std::string FOLDER_RANDOM_MISSION_NAME   = "random"; // v3.0.217.3 name of the random folder
//    const static std::string RANDOM_MISSION_DATA_FILE_NAME   = "random.xml"; // v3.0.217.3 name of the random folder
//
//    //static const int BRIEFERINFO_SENTENCE_LENGTH = 150;
//
//// General
//    const static std::string nmTRUE   = "1";
//    const static std::string nmFALSE  = "0";
////    const static std::string EMPTY_STRING  = "";
//    const static std::string COMMA = ","; //
//    const static std::string RND_SEP       = "|"; //
//    const static std::string UNDERSCORE    = "_"; //
//
//
//// Mission Data - XML
//    const static std::string GLOBAL_SETTINGS   ="global_settings";
//
//    //const static std::string DYNAMIC_OBJECTS    ="dynamic_objects";
//    const static std::string INVENTORY          ="inventory";
//    const static std::string REFUEL_LOCATIONS   ="refuel_locations";
//
//    const static std::string ELEMENT_REFUEL_ZONE        ="refuel_zone";
//    const static std::string ELEMENT_REFUEL_SETTINGS    ="refuel_settings";
//    const static std::string ELEMENT_REFUEL_RULES       ="refuel_rules"; // 
//    const static std::string ELEMENT_FUEL_TANK          ="tank";
//    const static std::string ATTRIB_FUEL_IN_TANK  ="fuel_in_tank_kg"; // refuel
//    const static std::string ATTRIB_MAX_FUEL_CAPACITY_IN_TANK ="max_fuel_capacity"; // refuel
//
//
//    const static std::string ATTRIB_METAR_FILE_NAME = "metar_file_name"; // 
//
//
//   
//    //const static std::string ATTRIB_DISPLAY_ON  = "display_on"; // 
//
//
////    const static std::string ATTRIB_MINUTES = "minutes"; // 
//
//
//
//
//    const static std::string ATTRIB_STEP_CAN_BE_SKIPPED    = "can_be_skipped"; // allow user to skip a step and flag it as success
//    //const static std::string ATTRIB_FORCE                  = "force";
//    //const static std::string ATTRIB_STEP_MAX_REPEAT        = "max_repeat";
//    //const static std::string ATTRIB_CORRECT_TARGET_ID      = "correct_target_id"; //  // relate to "pick" target
//    //const static std::string ATTRIB_PATH_DISPLAY_OPTIONS   = "path_disp_option"; //  // path helper
//    //const static std::string ATTRIB_PATH_OBS_CORRECTION    = "obs_correction"; //  // path helper
//    //const static std::string ATTRIB_PATH_MIN_DISTANCE_TO_CALC   = "min_distance_to_calc"; //  // path helper
//
//
//    // weather
//    //const static std::string ELEMENT_STEP_WEATHER        = "weather"; // step
//    //const static std::string ELEMENT_STEP_WEATHER_ZONE   = "weather_zone"; // step
//    //const static std::string ELEMENT_STEP_WEATHER_CLOUD  = "cloud"; // step
//    //const static std::string ELEMENT_STEP_WEATHER_WIND   = "wind"; // step
//    //const static std::string ELEMENT_STEP_WEATHER_SEA    = "sea"; // step
//
//    //const static std::string ATTRIB_USE_REAL_WEATHER      = "use_real_weather"; // weather
//    //const static std::string ATTRIB_OUTER_BOUND_NM        = "outer_bound_nm"; // weather
//    //const static std::string ATTRIB_INNER_BOUND_NM        = "inner_bound_nm"; // weather
//    //const static std::string ATTRIB_VIS_IN_METERS         = "vis_in_meters"; // weather
//    //const static std::string ATTRIB_RATE_OF_WEATHER_CHANGE   = "rate_of_weather_change"; // weather
//    //const static std::string ATTRIB_RAIN_PCT              = "rain_pct"; // weather
//    //const static std::string ATTRIB_THUNDER_STORM_PCT     = "thunder_storm_pct"; // weather
//    //const static std::string ATTRIB_CLOUD_BASE_FT         = "cloud_base_ft"; // weather
//    //const static std::string ATTRIB_CLOUD_TOP_FT          = "cloud_top_ft"; // weather
//    //const static std::string ATTRIB_LAYER                 = "layer"; // weather
//
//    //const static std::string ATTRIB_WIND_ALT_FT           = "wind_alt_ft"; // wind
//    //const static std::string ATTRIB_WIND_DIR_DEG          = "wind_dir_deg"; // wind
//    //const static std::string ATTRIB_WIND_SPEED_KT         = "wind_speed_kt"; // wind
//    //const static std::string ATTRIB_WIND_SHEAR_DIR_DEG    = "wind_shear_dir_deg"; // wind
//    //const static std::string ATTRIB_WIND_SHEAR_SPEED_KT   = "wind_shear_spd_kt"; // wind
//    //const static std::string ATTRIB_WIND_TURBULENCE       = "turbulence"; // wind - turbulance units, LR internal scale factor.
//    //const static std::string ATTRIB_WIND_TURBULENCE_PCT = "wind_turbulence_pct"; // wind
//    //const static std::string ATTRIB_SEA_WAVE_AMP              = "wave_amp"; // sea
//    //const static std::string ATTRIB_SEA_WAVE_LENGTH           = "wave_length"; // sea
//    //const static std::string ATTRIB_SEA_WAVE_SPEED            = "wave_speed"; // sea
//    //const static std::string ATTRIB_SEA_WAVE_DIR              = "wave_dir"; // sea
//    //const static std::string ATTRIB_SEA_BAROMETER           = "bar_sea_lvl"; // sea
//    //const static std::string ATTRIB_SEA_TEMP_LVL_C        = "temp_sea_lvl_c"; // sea
//    //const static std::string ATTRIB_SEA_DEW_POINT_C_LVL   = "dew_point_sea_lvl_c"; // sea
//
//
//    
//
//
//    //const static std::string DEFAULT_RND_MIN_MAX_ZONES = "1"; // 
//    //const static std::string DEFAULT_RND_MIN_MAX_BOUND_NM = "1|20"; // the weather effect area in nautical miles // v2.1.29.8 modified to 1|20
//    //const static std::string DEFAULT_RND_MIN_MAX_VISUAL_MT = "500|30000"; // visibility
//    //const static std::string DEFAULT_RND_MIN_MAX_RAIN_PCT = "0.0|0.1"; // rain percent
//    //const static std::string DEFAULT_RND_MIN_MAX_THUNDER_PCT = "0.0|0.1"; // thunders percent
//    //const static std::string DEFAULT_RND_MIN_MAX_CLOUD_LAYERS = "0|3"; // how many cloud layers, 0-3
//    //const static std::string DEFAULT_RND_MIN_MAX_CLOUD_LAYER_BASE_TOP = "500|40000"; // how many cloud layers, 0-3
//    //const static std::string DEFAULT_RND_WIND_LAYERS = DEFAULT_RND_MIN_MAX_CLOUD_LAYERS; // how many wind layers 0-3 - we should be dippendent on cloud layers
//    //const static std::string DEFAULT_RND_WIND_MIN_MAX_ALT_FT = DEFAULT_RND_MIN_MAX_CLOUD_LAYER_BASE_TOP; // height of wind layer - we should be dippendent on cloud layers
//    //const static std::string DEFAULT_RND_WIND_MIN_MAX_DIRECTION_DEG = "0|359"; // 
//    //const static std::string DEFAULT_RND_WIND_MIN_MAX_SPEED_KT = "0|25"; // // should be some dippendency between layers
//    //const static std::string DEFAULT_RND_WIND_MIN_MAX_TURB_UNIT = "0|3"; // 
//    //const static std::string DEFAULT_RND_WIND_MIN_MAX_TURB_PCT = "0|0.1"; // 
//    //const static std::string DEFAULT_RND_WIND_MIN_MAX_SHEAR_DIR_DEG = "0|359"; // 
//    //const static std::string DEFAULT_RND_WIND_MIN_MAX_SHEAR_SPEED_KT = "0.0|10.0"; // 
//    //const static int         FIRST_ZONE_ID = 0; // 
//
//
//    // step rules
//    //const static std::string ELEMENT_EVENT                     = "event";
//    //const static std::string ELEMENT_EVENT_MSG                 = "msg";
//    //const static std::string ELEMENT_ATTACHED_EVENT            = "attached_event";
//
//
//    const static std::string ELEMENT_CDATA  ="CDATA"; // "#cdata-section"
//    const static std::string ATTRIB_OPEN_CDATA  ="<![CDATA["; // "#cdata-section"
//
//    // OBJ 3D
//    //const static std::string ELEMENT_3D_MOVE = "move_obj3d";
//    //const static std::string VALUE_MOVE = "move";
//    //const static std::string ELEMENT_3D_CONDITIONS1  = "conditions_3d";
//    //const static std::string ELEMENT_3D_LOCATION1  = "location_3d";
//    //const static std::string ELEMENT_3D_PATH1  = "path_3d";
//
//    //const static std::string ATTRIB_3D_DISPLAY_IN_STEPID    = "display_in_step_id";
//    //const static std::string ATTRIB_3D_IMMEDIATE_DISPLAY    = "immediate_display";
//    //const static std::string ATTRIB_3D_KEEP_UNTIL_STEPID    = "keep_until_step_id";
//
//    const static std::string VALUE_3D_ALWAYS    = "-- always --"; // this translates to ZERO
//
//
//
//
//    // Triggers
//    //const static std::string ELEMENT_STEP_TRIGGER_ZONE   = "trigger_zone";
//    //const static std::string ELEMENT_STEP_TRIGGER_FLIGHT_EVENT   = "flight_event";
//    //const static std::string ELEMENT_STEP_TRIGGER_RULES  = "trigger_rules"; //
//    //const static std::string ELEMENT_STEP_TRIGGER_LOGIC  = "trigger_logic"; // 2.1.27
//    //const static std::string ELEMENT_ELEV      ="elev"; //
//    //const static std::string ELEMENT_POINT_REF ="point_ref"; // 
//    //const static std::string ELEMENT_SLOPE     ="slope"; //
//    //const static std::string ELEMENT_TIMER     ="timer"; // use in delayed feedback message and in triggerzone based time Consider replace this with "ATTRIB_TIMER_SEC"
//    //const static std::string ELEMENT_TIMER_MSG ="timerMsg"; // 
//    //const static std::string ATTRIB_TIMER_SEC       = "timer_sec"; //  used to delay message in delayed feedback message. Not to mix with ATTRIB_MSG_SECONDS_TO_PLAY which is how much time to broadcast the message
//    //const static std::string ATTRIB_TIMER_COUNT_SEC = "time_to_count_sec"; // 
//    //const static std::string ATTRIB_TIMER_COUNT_MIN = "time_to_count_min"; // 
//    //const static std::string ATTRIB_ZULU_HOUR="zulu_hour"; // 
//    //const static std::string ATTRIB_ZULU_MIN="time_to_count_min"; // 
//
//
//    // inventory
//    //const static std::string ELEMENT_STORE               ="store"; //
//    //const static std::string ELEMENT_IN_STORE            ="item"; //
//    //const static std::string ATTRIB_STORE_DIST_OPEN   ="distance_to_open_nm"; //
//    //const static double  DEFAULT_STORE_DIST_OPEN      = 0.05; //
//    //const static std::string ATTRIB_BR_NAME = "br_name"; // barcode
//    //const static std::string ATTRIB_AMOUNT  = "amount";
//
//    //const static std::string ATTRIB_STORE_NAME  = "store_name";
//    //const static std::string ATTRIB_ELEMENT_NAME   = "item_name";
//    //const static std::string ATTRIB_DISPLAY_NAME  = "display_name";
//    //const static std::string ATTRIB_PRICE         = "price";
//
//
//    // EDITOR Attribute
//    //const static std::string ATTRIB_EID = "eid";
//    //const static std::string ATTRIB_EID_NAME = "eid_name";
//    //const static std::string ATTRIB_EID_MASTER_ID = "eid_master_id";
//
//
//    // Common Attributes
//    const static std::string ATTRIB_ID = "id";
//    const static std::string ATTRIB_TITLE = "title";
//    //const static std::string ATTRIB_AUTOMSG = "auto_msg";
//    //const static std::string ATTRIB_GROUPID = "group_id";
//    //const static std::string ATTRIB_3D_FORCE_ON_GROUND = "force_ground"; // v2.1.28.1 should we use probe or not
//    //const static std::string ATTRIB_NEED2LAND = "need_to_land";
//    //const static std::string ATTRIB_SUCCESS_DISTANCE = "success_distance_nm";
//    //const static std::string ATTRIB_SUCCESS_ELEV_RELATIVE = "success_elev_relative_ft";
//    const static std::string ATTRIB_OPTIONAL = "optional";
//
//    const static std::string ATTRIB_STARTING_ICAO = "starting_icao";
//
//    // Settings
//    const static std::string ATTRIB_FILENAME_ON_REACH   = "file_name_on_reach";
//    const static std::string ATTRIB_STEP_SETTINGS_RESTRICT_REFUEL  = "restrict_refuel_location";
//
//
//    // LOGIC
//    const static std::string ELEMENT_DATAREF         = "dataref"; //
//    const static std::string ELEMENT_PARAM           = "param"; // 
//    const static std::string ELEMENT_PARAM_EXPR      = "param_expression"; // 
//    const static std::string ELEMENT_PARAM_CHECK     = "param_check"; // 
//    const static std::string ELEMENT_SET_PARAM       = "set_param"; // 
//    const static std::string ATTRIB_VALUE         = "value"; // 
//    const static std::string ATTRIB_RETURN        = "return"; // 
//    const static std::string ATTRIB_RETURN_NO     = "number"; // 
//
//    const static std::string ATTRIB_DREF_NAME     = "dr_name";
//    //const static std::string ATTRIB_LOGIC_COND    = "cond";
//    const static std::string ATTRIB_LOGIC_TEST_VALUE   = "test_value";
//    const static std::string ATTRIB_LOGIC_NEW_VALUE    = "new_value";
//    const static std::string ATTRIB_PARAM_LOGIC_LVALUE    = "lValue";
//    const static std::string ATTRIB_PARAM_LOGIC_RVALUE    = "rValue";
//
//    const static std::string ELEMENT_DREF_CHECKSET         = "dref_check_set"; //
//    const static std::string ELEMENT_INV_CHECKSET          = "inv_check_set"; //
//    const static std::string ATTRIB_CHECK_RESULT_EVENT  = "result_event";
//    const static std::string ATTRIB_CHECK_ELSE_EVENT    = "else_event";
//    const static std::string ATTRIB_TEST_PERIOD_SEC     = "test_period_sec"; // // How much time does checkSet should be true before evaluating to true
//    const static std::string ATTRIB_DISPLAY_HINT        = "display_hint"; // use in checkset when starting evaluating logic. Uses the cue 3d message. mostly useful with checkset with "time_to_true_sec" attribute.
//    const static std::string ATTRIB_DISPLAY_HINT_TITLE  = "hint_title"; // short string to display to simmer.
//
//    const static std::string COND_EQUAL                 = "=";
//    const static std::string COND_NOT_EQUAL             = "!=";
//    const static std::string COND_GREATER_DISPLAY       = ">";
//    const static std::string COND_GREATER_VALUE         = "gt;";
//    const static std::string COND_GREATER_EQUAL_DISPLAY = ">=";
//    const static std::string COND_GREATER_EQUAL_VALUE   = "gte;";
//    const static std::string COND_LESS_DISPLAY          = "<";
//    const static std::string COND_LESS_VALUE            = "lt;";
//    const static std::string COND_LESS_EQUAL_DISPLAY    = "<=";
//    const static std::string COND_LESS_EQUAL_VALUE      = "lte;";
//
//    const static std::string ELEMENT_LOGIC_IF           = "if";
//    const static std::string ELEMENT_LOGIC_IF_PARAM     = "if_param"; // 
//    const static std::string ELEMENT_LOGIC_THEN         = "then";
//    const static std::string ELEMENT_LOGIC_FOR_SUCCESS  = "logic_for_success";
//    const static std::string ELEMENT_LOGIC_APPLY        = "logic_apply";
//
//    // payload
//    const static std::string ATTRIB_WEIGHT      = "weight";    
//    const static std::string ATTRIB_OPERATION   = "operation";
//    const static std::string ATTRIB_IS_TOTAL    = "is_total";
//    const static std::string ATTRIB_IS_PERCENT  = "is_percent";
//
//    const static std::string ATTRIB_TIMEADJ_ADDMIN = "add_minutes";
//
//
//
//    const static std::string TRIGGER_ACTION_ENTER   = "enter";
//    const static std::string TRIGGER_ACTION_TIMER   = "timer";
//    const static std::string TRIGGER_ACTION_RESTRICT= "restrict";
//    const static std::string TRIGGER_ACTION_BOUND   = "bound"; // good for leave onLeave
////    const static std::string TRIGGER_ACTION_LEAVE   = "leave"; // should replace bound
//
//
//    //// EVENTS //////
//
//    const static std::string EVENT_ON_ENTER         = "onEnter"; //
//    const static std::string EVENT_ON_LEAVE         = "onLeave"; //
//    const static std::string EVENT_ON_SUCCESS       = "onSuccess"; //
//    const static std::string EVENT_ON_FAIL          = "onFail"; //
//    const static std::string EVENT_ON_HIT           = "onHit"; //
//    const static std::string EVENT_ON_HIT_FAIL      = "onHitFailed"; //
//    const static std::string EVENT_ON_SELF          = "onSelf"; //
//    const static std::string EVENT_ON_DROP          = "onDrop"; //
//    const static std::string EVENT_ON_TIME_ELAPSED  = "onTimeElapsed"; // 
//
//    const static std::string ELEMENT_INV_ACTION        = "inv_action"; //
//    const static std::string OPER_REMOVE            = "remove"; //
//    const static std::string OPER_ADD               = "add"; //
//
//
//
//
//    //////////// DEFAULTS //////////////////
//    const static std::string DEFAULT_STEP_MSG_TYPE    = "mission"; //
//    const static std::string DEFAULT_SUPPORTED_MISSION      = "200"; //
//    const static std::string DEFAULT_SUCCESS_DISTANCE_NM    = "0.03"; //
//    const static std::string DEFAULT_SUCCESS_DISTANCE_MTR   = "10"; // radius 10 meter
//    const static std::string DEFAULT_DECIMAL_ZERO           = "0.00"; //
//    const static std::string DEFAULT_INT_ZERO           = "0"; //
//    const static std::string DEFAULT_DECIMAL_SOUND_VOLUME   = "0.4"; // for slider, need to convert to (value*100) as int
//    const static std::string DEFAULT_DOUBLE_DISTANCE_MSG = "1.0"; // for slider, need to convert to (value*100) as int
//
//
//
//    const static std::string DEFAULT_ELEV_FT    = "100.00"; //
//    const static std::string DEFAULT_FUEL_IN_TANK  = "100"; // Refuel // v0.54.1
//    const static std::string DEFAULT_MAX_FUEL_CAPACITY_IN_TANK  = "1000"; // Refuel // v0.54.1
//
//    // v3.0.213.4
//    const static int MAX_ITEM_IN_INVENTORY = 15; // used in em_reset_inventory_item_table() function.
//
//    //const static int NO_ACTION_ON_TABLE = -1; // used in em_generic_tab_row_dml() function.
//    //const static int NO_LIMIT_ROWS_ON_TABLE = 0; // used in em_generic_tab_row_dml() function.
//    //const static int PAYLOAD_TAB_ROWS_LIMIT = 4; // used in on_tableStepPayload_cellChanged() function.
//    //const static int MESSAGE_TAB_ROWS_LIMIT = 5; // used in on_tableStepPayload_cellChanged() function.
//    //const static int TRIGGER_TAB_ROWS_LIMIT = 8; // used in on_btnStepAddTrigger_clicked() function.
//    //const static int TRIGGER_POINT_TAB_ROWS_LIMIT = 20; // used in on_stepTriggerPointsTab_cellActivated() function.
//    //const static int STEP_CREATION_LIMIT = 50; // used in on_stepTriggerPointsTab_cellActivated() function.
//    //const static int LIMIT_STORES_NUMBER = 50; // used in on_btnAddStorege_clicked() function.
//    //const static int LIMIT_DATAREF_ITEMS = 50; // used in em_reset_dataref_widget() function.
//    //const static int LIMIT_CHECKSET_ITEMS = 50; // used in em_reset_dataref_widget() function.
//    //const static int LIMIT_LOGICAL_COND_ITEMS = 10; // used in em_reset_dataref_widget() function.
//    //const static int LIMIT_TEST_LOGIC = 10; // used in trig_adv_dialog.em_reset_trig_adv_widget() function.
//    //const static int LIMIT_INV_ACTIONS = 5; // used in trig_adv_dialog.em_reset_trig_adv_widget() function.
//    //const static int LIMIT_3D_OBJECTS = 150; // used in trig_adv_dialog.em_reset_trig_adv_widget() function.
//    //const static int I_DEFAULT_FIRST_ROW_IN_EMPTY_TABLE = 0; // used in em_generic_tab_row_dml function.
//    //const static int LIMIT_WEATHER_SYSTEMS = 5; // used in weather_dialog.
//    //const static int LIMIT_WEATHER_ZONES = 3; // used in weather_dialog.
//    //const static int LIMIT_WEATHER_LAYERS = 3; // used in weather_dialog.
//    //const static int LIMIT_AREA_TARGET = 100; // 2.1.27 - used when reading mission file "areaTaget"
//
//    //const static int LAST_STEP_NO = 9999; //used in xm.em_INJECT_EID
//    //const static int NEGATIVE_NO = -1; //used em_link_between_elements()
//    //const static int DEFAULT_EID_NO = -1; //used em_TableWidgetItem.init()
//
//    //const static int MAX_DROP_TARGET = 4; // used in drop_object in event_dialog.cpp.
//
//
//    //const static double DEFAULT_TRIGGER_LENGTH_NM       = 0.02; // for radius rule under trigger_zone
//    //const static int    DEFAULT_REFUEL_RAD_ZONE_LENGTH_METER       = 80; // for radius rule under refuel zone
//
//    const static std::string XML_NODE_SEPERATOR           = " ========================== "; //
//
//
//    const static std::string MINUS_ONE    = "-1"; //
//    const static std::string ZERO         = "0"; //
//    const static std::string ONE          = "1"; //
//    const static std::string TWO          = "2"; //
//    const static std::string THREE        = "3"; //
//    const static std::string FOUR         = "4"; //
//    const static std::string FIVE         = "5"; //
//    const static std::string SIX          = "6"; //
//    const static std::string SEVEN        = "7"; //
//    const static std::string EIGHT        = "8"; //
//    const static std::string NINE         = "9"; //
//    const static std::string TEN = "10"; //
//    const static std::string TWENTY = "20"; //
//    const static std::string THIRTY = "30"; //
//    const static std::string FORTY = "40"; //
//    const static std::string FIFTY = "50"; //
//
//    const static std::string TITLE_GROUPBOX_CHECKSET_CONDITION  = "What exchange need to be done for: "; //
//    const static std::string TITLE_GROUPBOX_CHECKSET_LOGIC_CONDITION  = "What are the evaluation needed for: "; //
//    const static std::string OPTIONS_TRUE_FALSE_DISPLAY = ",true,false";
//    const static std::string OPTIONS_TRUE_FALSE_VALUE   = ",1,0";
//    const static std::string OPTIONS_SUCCESS_OR_SELF_OPTIONS   = "onSelf,onSuccess";
//
//
//    const static std::string CLOUD_TYPE1 = "clear";
//    const static std::string CLOUD_TYPE2 = "cirrus";
//    const static std::string CLOUD_TYPE3 = "few cumulus";
//    const static std::string CLOUD_TYPE4 = "scattered cumulus";
//    const static std::string CLOUD_TYPE5 = "broken cumulus";
//    const static std::string CLOUD_TYPE6 = "overcast cumulus";
//    const static std::string CLOUD_TYPE7 = "stratus";
//
//    const static int CLOUD_TYPE1_NO = 0; // clear
//    const static int CLOUD_TYPE2_NO = 1; // cirrus
//    const static int CLOUD_TYPE3_NO = 2; // few cumulus
//    const static int CLOUD_TYPE4_NO = 3; // scattered
//    const static int CLOUD_TYPE5_NO = 4; // broken 
//    const static int CLOUD_TYPE6_NO = 5; // overcast
//    const static int CLOUD_TYPE7_NO = 6; // stratus
//    
//    const static int MAX_CLOUD_AND_WIND_LAYERS = 3; // v2.1.29.8 max cloud layers
//
//    // consider depricate if we do not use    
//    //const static std::string ALL_CLOUD_TYPES = std::string(COMMA) + CLOUD_TYPE1 + COMMA + CLOUD_TYPE2 + COMMA + CLOUD_TYPE3 + COMMA + CLOUD_TYPE4 + COMMA + CLOUD_TYPE5 + COMMA + CLOUD_TYPE6 + COMMA + CLOUD_TYPE7; //  [clear, Few Cumulus, Cumulus Scattered ,Cumulus Broken ,Cumulus Overcast ,Stratus ]    
//    const static std::string DEFAULT_RND_CLOUD_TYPES = CLOUD_TYPE1 + RND_SEP + CLOUD_TYPE2 + RND_SEP + CLOUD_TYPE3 + RND_SEP + CLOUD_TYPE4 + RND_SEP + CLOUD_TYPE5 + RND_SEP + CLOUD_TYPE6 + RND_SEP + CLOUD_TYPE7; // v2.1.29.8  [clear, Few Cumulus, Cumulus Scattered ,Cumulus Broken ,Cumulus Overcast ,Stratus ] 
//
//
//    const static int _1SEC         = 1000; //
//    const static int _2SEC         = 2000; //
//    const static int _3SEC         = 3000; //
//    const static int _4SEC         = 4000; //
//    const static int _5SEC         = 5000; //
//
//    const static std::string STATS_LANDING = "Landing"; // "L"; // "Landing"
//    const static std::string STATS_TAKEOFF = "takeoff"; //"T"; // "takeoff"
//
//    const static std::string GL_WIDGET_BITMAP_END = "bitmap_end";
//    const static std::string GL_WIDGET_STATS      = "stats";
//    const static std::string GL_WIDGET_MAP2D      = "map2d";
//    const static std::string GL_WIDGET_CUE_MESSAGE = "cue_message";
//    const static std::string GL_WIDGET_QUICK_BAR = "float_quickbar";
//
//    // Regular Expression constants
//    const static std::string MX_REGEX_ARRAY_AT_THE_END_OF_STRING   = "\\[\\d+\\]$"; // a string that has array like ending
//    const static std::string MX_REGEX_INTEGER = "\\d+"; // number in a string
//    const static std::string MX_ERR_WRONG_ARRAY_NUMBER = "Wrong array number, check parameter settings!"; // ERROR when dataref array represent wrong array size
//
//    const static std::string MX_STEP_STATUS_SKIP_END_DESC     = "Skipped the Step."; // // Status description
//    const static std::string MX_STEP_STATUS_END_SUCCESS_DESC  = "Step End Success."; // // Status description
//    const static std::string MX_STEP_STATUS_END_FAIL_DESC     = "Step End Fail."; // // Status description
//
//    const static std::string MX_MSG_SKIP     = "You can skip this step."; // //
//
//    // external script constants
//    //const static std::string EXT_PRE_STEP_SCRIPT_NAME  = "pre_step.bas";
//    //const static std::string EXT_POST_STEP_SCRIPT_NAME = "post_step.bas";
//
//    // V3.0.0 /// V3.0.0 /// V3.0.0 /// V3.0.0 /// V3.0.0 ///// V3.0.0 /// V3.0.0 /// V3.0.0 /// V3.0.0 /// V3.0.0 ///
//    // V3.0.0 /// V3.0.0 /// V3.0.0 /// V3.0.0 /// V3.0.0 ///// V3.0.0 /// V3.0.0 /// V3.0.0 /// V3.0.0 /// V3.0.0 ///
//    // V3.0.0 /// V3.0.0 /// V3.0.0 /// V3.0.0 /// V3.0.0 ///// V3.0.0 /// V3.0.0 /// V3.0.0 /// V3.0.0 /// V3.0.0 ///
//
//    const static std::string MISSION_ROOT_DOC = "MISSION";
//    const static std::string TEMPLATE_ROOT_DOC = "TEMPLATE";
//    const static std::string MAPPING_ROOT_DOC = "MAPPING";
//    const static std::string MISSION_ROOT_SAVE_DOC = "SAVE";
//    const static std::string ATTRIB_VERSION = "version"; // MISSION
//    const static std::string ATTRIB_MISSION_DESIGNER_MODE = "designer_mode"; // MISSION
//    const static std::string MX_FILE_SAVE_EXTENTION = ".savepoint.sav"; // MISSION //
//    const static std::string MX_FILE_SAVE_DREF_EXTENTION = ".dataref.txt"; // MISSION //
//    const static std::string ATTRIB_TYPE = "type"; // trigger // refule_zone
//    const static std::string ATTRIB_ELEV_MAX_FT = "elev_max_ft"; // trigger elevation
//    const static std::string ATTRIB_RE_ARM = "rearm"; // v3.0.219.1 used in trigger. Will be set when leaving trigger
//
//    const static std::string ELEMENT_DESIGNER = "designer";
//    const static std::string ATTRIB_FORCE_LEG_NAME = "force_leg_name"; // v3.0.221.15rc5 add LEG support. global settings - designer
//    const static std::string ATTRIB_FORCE_GOAL_NAME = "force_goal_name"; // global settings - designer
//
//    // Time
//    const static std::string ATTRIB_TIME_HOURS = "hours"; // global settings
//    const static std::string ATTRIB_TIME_HOURS_LOCAL = "hours_local"; // v3.0.219.7 global settings
//    const static std::string ATTRIB_TIME_LOCAL_HOURS = "local_hours"; // v3.0.223.1 global settings
//    const static std::string ATTRIB_TIME_MIN = "min"; // global settings
//    const static std::string ELEMENT_START_TIME = "start_time";
//    const static std::string ATTRIB_TIME_DAY_IN_YEAR = "day_in_year"; // global settings
//
//    // Mission Description
//    const static std::string ELEMENT_MISSION_INFO = "mission_info"; // Holds information for mission briefing
//    const static std::string ATTRIB_MISSION_IMAGE_FILE_NAME = "mission_image_file_name"; // mission briefing
//    const static std::string ATTRIB_TEMPLATE_IMAGE_FILE_NAME = "template_image_file_name"; // template image
//    const static std::string ATTRIB_PLANE_DESC = "plane_desc"; // mission briefing
//    const static std::string ATTRIB_ESTIMATE_TIME = "estimate_time"; // mission briefing
//    const static std::string ATTRIB_DIFFICULTY = "difficulty"; // mission briefing
//    const static std::string ATTRIB_WEATHER_SETTINGS = "weather_settings"; // weather briefing    
//    const static std::string ATTRIB_SCENERY_SETTINGS = "scenery_settings"; // scenery briefing 
//    const static std::string ATTRIB_WRITTEN_BY = "written_by"; // scenery briefing
//    const static std::string ATTRIB_OTHER_SETTINGS = "other_settings"; // weather briefing
//    const static std::string ATTRIB_SHORT_DESC = "short_desc"; //short description
//
//    // Save checkpoint
//    const static std::string ELEMENT_PROPERTIES = "properties"; // properties, for checkpoint
//    const static std::string ELEMENT_SAVE = MISSION_ROOT_SAVE_DOC; // for checkpoint
//    const static std::string PROP_MISSION_PROPERTIES = "mission_properties"; // for checkpoint
//    const static std::string PROP_MISSION_FOLDER_PROPERTIES = "mission_folder_properties"; // for checkpoint
//
//    // FOLDER PROPERTIES
//    const static std::string ELEMENT_FOLDERS = "folders"; //
//    const static std::string ATTRIB_MISSION_PACKAGE_FOLDER_PATH = "mission_package_folder_path"; // v3.0.213.7 renamed  from "missions_root_folder_name" to "MISSION_PACKAGE_FOLDER_PATH" for better readability
//    const static std::string ATTRIB_SOUND_FOLDER_NAME = "sound_folder_name"; //
//    const static std::string ATTRIB_OBJ3D_FOLDER_NAME = "obj3d_folder_name"; // 
//    const static std::string ATTRIB_METAR_FOLDER_NAME = "metar_folder_name"; // 
//    const static std::string ATTRIB_SCRIPT_FOLDER_NAME = "script_folder_name"; // 
//
//
//    const static std::string PROP_XPLANE_INSTALL_PATH = "xplane_install_path";
//    const static std::string PROP_XPLANE_PLUGINS_PATH = "xplane_plugins_path";
//    const static std::string PROP_MISSIONX_BITMAP_PATH = "missionx_bitmap_path"; // v3.0.118
//    //const static std::string FLD_MISSIONX_DATA_PATH = "missionx_data_path"; // v3.0.213.7 deprecated - not in use anymore
//    const static std::string FLD_MISSIONX_SAVE_PATH = "missionx_save_path";
//    const static std::string FLD_MISSIONX_LOG_PATH = "missionx_log_path";
//    const static std::string FLD_MISSIONX_SAVEPOINT_PATH = "missionx_savepoint_path";
//    const static std::string FLD_MISSIONX_SAVEPOINT_DREF = "missionx_savepoint_dref";
//    const static std::string FLD_MISSIONS_ROOT_PATH  = "missions_root_folder"; // currently "custom scenery/missionx"
//    const static std::string FLD_RANDOM_TEMPLATES_PATH = "missionx_random_templates_folder_path"; // v3.0.217.1 currently "Resources/plugin/missionx/templates"
//    const static std::string FLD_RANDOM_MISSION_PATH   = "missionx_random_mission_folder_path"; // v3.0.217.1 crrently "custom scenery/missionx/random"
//    const static std::string FLD_CUSTOM_SCENERY_FOLDER_PATH   = "xplane_custom_scenery_folder_path"; // v3.0.219.9 currently "custom scenery"
//    const static std::string FLD_DEFAULT_APTDATA_PATH   = "xplane_default_aptdata_folder_path"; // v3.0.219.9 currently "xp11/Resources/default scenery/default apt dat/"
//
//    const static std::string ATTRIB_NAME = "name";
//    const static std::string ATTRIB_UNIQUE_ELEMENT_NAME = "unique_element_name"; // v3.0.217.4
//    const static std::string ELEMENT_DESC = "desc";
//
//
//    // METAR
//    const static std::string ELEMENT_METAR = "metar"; //
//    const static std::string ATTRIB_INJECT_METAR_FILE = "inject_metar_file"; // v3.0.224.1 used in message element and will be called once text message has been broadcasted.
//    const static std::string XPLANE_METAR_FILENAME = "METAR.rwx"; //
//    const static std::string ATTRIB_FORCE_CUSTOM_METAR_FILE = "force_custom_metar_file"; // bool value "0 = no/false; 1 = yes/true
//    
//
//    // map 2d
//    const static std::string ELEMENT_MAP = "map"; //
//
//
//
//    const static std::string ATTRIB_MANDATORY = "mandatory";
//    const static std::string PROP_TASK_STATE = "task_state";
//
//    const static std::string ATTRIB_MAP_WIDTH = "map_width";
//    const static std::string ATTRIB_MAP_HEIGHT = "map_height";
//    const static std::string ATTRIB_MAP_FILE_NAME = "map_file_name";
//
//    const static std::string ELEMENT_EMBEDED_SCRIPTS = "embedded_scripts";
//    const static std::string ELEMENT_FILE = "file";
//    const static std::string ELEMENT_INCLUDE_FILE = "include_file";
//    const static std::string ATTRIB_INCLUDE_FILE  = ELEMENT_INCLUDE_FILE; // for the sake of readability
//    const static std::string ELEMENT_SCRIPTLET = "scriptlet";
//    const static std::string ELEMENT_SHARED_VARIABLES = "shared_variables"; // v3.0.202a // root of all shared variables
//    const static std::string ELEMENT_VAR = "var"; // v3.0.202a // represent a shared variable element
//    const static std::string ATTRIB_INIT_VAL = "init_val"; // v3.0.202a // represent a shared variable element
//
//    const static std::string ELEMENT_SCRIPT_GLOBAL_STRING_PARAMS = "global_string_params";
//    const static std::string ELEMENT_SCRIPT_GLOBAL_NUMBER_PARAMS = "global_number_params";
//    const static std::string ELEMENT_SCRIPT_GLOBAL_BOOL_PARAMS   = "global_bool_params";
//
//
//    const static std::string ELEMENT_LOGIC = "logic";
//    const static std::string ELEMENT_XPDATA= "xpdata";
//    const static std::string ELEMENT_TASKS = "tasks";
//    const static std::string ELEMENT_TASK = "task";
//    //const static std::string PROP_TASK_TEMPLATES  = "task_templates";
//
//    const static std::string ATTRIB_BASE_ON_TRIGGER = "base_on_trigger"; // trigger name that will evaluate the task from physical perspectives (can have logical aspects too).
//    const static std::string ATTRIB_BASE_ON_SCRIPT  = "base_on_script"; // script will evaluate validity of task or other action. Not meant for physical location. 
//    const static std::string ATTRIB_BASE_ON_COMMAND  = "base_on_command"; // v3.0.221.10 Holds command path
//    const static std::string ATTRIB_EVAL_SUCCESS_FOR_N_SEC  = "eval_success_for_n_sec"; // Flag task as success only if its conditions are met for at least N seconds 
//    const static std::string ATTRIB_FORCE_EVALUATION = "force_evaluation"; // renamed in v3.0.205.3 from continues_evaluation // "always_evaluate"; // Precede "eval_success_for_n_sec". Even it task was flaged as success, it will continue to evaluate its conditions until Objective will be flaged as success. 
//    const static std::string ATTRIB_CUMULATIVE_TIMER_FLAG = "cumulative_timer_flag"; // v3.0.221.11 boolean - flag if timer should be cumulative or not. Example: when evaluating seconds for success, you are not allowed to leave the area of effect. It will reset the timer. Cumulative will "just" pause and then unpause the timer.
//
//
//    const static std::string ELEMENT_OBJECTIVES = "objectives";
//    const static std::string ELEMENT_OBJECTIVE = "objective";
//    const static std::string ELEMENT_LINK_TO_TASK  = "link_to_task";
//    const static std::string ATTRIB_TASK_NAME = "task_name";
//    const static std::string ATTRIB_ALIAS_NAME = "alias_name";
//    const static std::string ATTRIB_DEPENDS_ON_TASK = "depends_on_task"; // depends on a task in same objective    
//    //const static std::string ATTRIB_OVERRIDE_WITH_TRIGGER = "override_with_trigger"; // override trigger name that will replace the original trigger, only in that objectivce.
//    //const static std::string ATTRIB_OVERRIDE_WITH_SCRIPT  = "override_with_script"; // override script that will replace original one, only in that objective.
//
//    const static std::string ELEMENT_GPS = "gps"; // v3.0.217.4 holds GPS points for generated mission and maybe to regular missions too
//    const static std::string ATTRIB_DESTINATION_ENTRY = "destination_entry"; // v3.0.219.7 add GPS/FMS current destination flying to.
//
//    const static std::string ELEMENT_GOALS = "goals";
//    const static std::string ELEMENT_GOAL  = "goal";
//    const static std::string ELEMENT_FLIGHT_PLAN = "flight_plan"; // v3.0.221.15rc4 will replace <goals>
//    const static std::string ELEMENT_LEG  = "leg"; // v3.0.221.15rc4 will replace <goal>. represent a leg in a flight plan
//    const static std::string ELEMENT_SPECIAL_LEG_DIRECTIVES  = "special_leg_directives"; // v3.0.221.15rc5 replaces ELEMENT_SPECIAL_GOAL_DIRECTIVES
//    const static std::string ELEMENT_SPECIAL_GOAL_DIRECTIVES  = "special_goal_directives"; // v3.0.221.8
//    const static std::string ELEMENT_LINK_TO_OBJECTIVE = "link_to_objective";
//    const static std::string ELEMENT_LINK_TO_TRIGGER = "link_to_trigger";
//    const static std::string ELEMENT_START_LEG_MESSAGE = "start_leg_message";
//    const static std::string ELEMENT_START_GOAL_MESSAGE = "start_goal_message";
//    const static std::string PROP_START_LEG_MESSAGE_FIRED = "start_leg_message_fired"; // v3.0.221.15rc5 replaced PROP_START_GOAL_MESSAGE_FIRED // v3.0.213.7 add to goal after message was broadcasted, so won't fire after load checkpoint
//    //const static std::string PROP_START_GOAL_MESSAGE_FIRED = "start_goal_message_fired"; // v3.0.213.7 add to goal after message was broadcasted, so won't fire after load checkpoint
//    const static std::string ELEMENT_END_LEG_MESSAGE   = "end_leg_message"; // v3.0.221.15rc5 replaced  ELEMENT_END_GOAL_MESSAGE
//    const static std::string ELEMENT_END_GOAL_MESSAGE   = "end_goal_message"; // compatibility
//    const static std::string ELEMENT_PRE_LEG_SCRIPT    = "pre_leg_script";
//    const static std::string ELEMENT_PRE_GOAL_SCRIPT    = "pre_goal_script";
//    const static std::string PROP_PRE_LEG_SCRIPT_FIRED    = "pre_leg_script_fired"; // v3.0.221.15rc5 replaced PROP_PRE_GOAL_SCRIPT_FIRED // v3.0.213.7 add to goal after script was fired, so won't fire after load checkpoint
//    //const static std::string PROP_PRE_GOAL_SCRIPT_FIRED    = "pre_goal_script_fired"; // v3.0.213.7 add to goal after script was fired, so won't fire after load checkpoint
//    const static std::string ELEMENT_POST_LEG_SCRIPT   = "post_leg_script";
//    const static std::string ELEMENT_POST_GOAL_SCRIPT   = "post_goal_script";
//    const static std::string ATTRIB_NEXT_LEG = "next_leg";
//    const static std::string ATTRIB_NEXT_GOAL = "next_goal";
//
//    
//    const static std::string ELEMENT_BRIEFER = "briefer";
//    const static std::string ATTRIB_STARTING_LEG = "starting_leg"; // v3.0.221.15rc5 replaced ATTRIB_STARTING_GOAL
//    const static std::string ATTRIB_STARTING_GOAL = "starting_goal"; // compatibility
//    const static std::string ELEMENT_LOCATION_ADJUST = "location_adjust";
//    const static std::string ATTRIB_LAT = "lat";
//    const static std::string ATTRIB_LONG = "long";
//    const static std::string ATTRIB_ELEV_FT = "elev_ft"; // under trigger
//    const static std::string ATTRIB_HEADING_PSI = "heading_psi"; // psi - The true heading of the aircraft in degrees from the Z axis - OpenGL coordinates
//    const static std::string ATTRIB_ADJUST_HEADING = "adjust_heading"; // v3.0.207.5 +/- values to adjust heading of moving object
//    const static std::string ATTRIB_PITCH = "pitch"; //
//    const static std::string ATTRIB_ROLL = "roll"; //
//    const static std::string ATTRIB_STARTING_SPEED_KTS = "starting_speed_kts"; //
//    const static std::string ATTRIB_STARTING_SPEED_MT_SEC = "starting_speed_mt_sec"; // starting speed in meters per seconds
//    const static std::string ATTRIB_PAUSE_AFTER_LOCATION_ADJUST = "pause_after_location_adjust";
//    const static std::string PROP_CURRENT_LOCATION = "current_location";
//    const static std::string PROP_POINT_DATA = "point_data";
//
//
//    // 3D Objects // v3.0.200
//    const static std::string PROP_OBJECTS_ROOT = "objects_root"; // savepoint
//    const static std::string PROP_OBJECTS_INSTANCES = "objects_instances"; // savepoint
//
//    const static std::string ELEMENT_OBJECT_TEMPLATES = "object_templates";
//    const static std::string ELEMENT_OBJ3D = "obj3d";
//    const static std::string ELEMENT_PATH = "path"; // v3.0.207.1 renamed to "path". Holds points of movement path
//    const static std::string ATTRIB_CYCLE = "cycle"; // v3.0.207.1 cycle path ?
//    const static std::string PROP_POINT_FROM  = "point_from"; // v3.0.213.7  obj3d.pointFrom
//    const static std::string PROP_POINT_TO    = "point_to";     // v3.0.213.7  obj3d.pointTo
//    const static std::string PROP_CURRENT_POINT_NO = "current_point_no"; // v3.0.213.7 obj3d.currentPointNo
//    const static std::string PROP_INSTANCE_DATA_ELEMENT = "instance_data"; // v3.0.213.7 obj3d instance element for checkpoint
//    const static std::string PROP_INSTANCE_SPECIAL_PROPERTIES = "instance_special_properties"; // v3.0.213.7 obj3d instance element for checkpoint
//    const static std::string PROP_GOAL_INSTANCES_SPECIAL_DATA_ELEMENT = "goal_instances_special_data_element"; // v3.0.213.7 Will use after load checkpoint to apply on loaded instances (if any)
//    const static std::string PROP_LOADED_FROM_CHECKPOINT = "loaded_from_checkpoint"; // v3.0.213.7 we will add this to Goal elements, so we will know that instance objects should be read from the instance data in the checkpoint file.
//    const static std::string PROP_GOAL_MAP2D = "goals_maps"; // v3.0.213.7 we will add this to Goal elements, so we will know that instance objects should be read from the instance data in the checkpoint file.
//
//
//    const static std::string ELEMENT_LOCATION = "location"; // lat long elev_ft
//    const static std::string ELEMENT_TILT = "tilt"; // heading pitch roll
//    const static std::string ELEMENT_DISPLAY_OBJECT = "display_object"; // goal element. Inform plugin to add new instance for speicifc obj3d template when goal became active
//    const static std::string ELEMENT_DISPLAY_OBJECT_SET = "display_object_set"; // v3.0.219.12+ points to an element name and its sub element tag name <display_object_set name="<main tag name>" template="<sub tag name>". Holds 3D Objects to display in target location
//    const static std::string ELEMENT_INST_OBJECT = "inst_object"; // goal element. Inform plugin to add new instance for speicifc obj3d template when goal became active
//
//    const static std::string ATTRIB_REPLACE_LAT = "replace_lat"; // v3.0.217.6
//    const static std::string ATTRIB_REPLACE_LONG = "replace_long"; // v3.0.217.6
//    const static std::string ATTRIB_REPLACE_ELEV_FT = "replace_elev_ft"; //  v3.0.217.6
//    const static std::string ATTRIB_REPLACE_ELEV_ABOVE_GROUND_FT = "replace_elev_above_ground_ft"; //  v3.0.217.6
//    const static std::string ATTRIB_INSTANCE_NAME = "instance_name"; // unique name for 3D object template
//    const static std::string ATTRIB_OBJ3D_TYPE = "obj_type";
//    const static std::string ATTRIB_ALTERNATE_OBJ_FILE  = "alternate_obj_file";
//    const static std::string ATTRIB_DISTANCE_TO_DISPLAY_NM = "distance_to_display_nm"; // v3.0.200
//    const static std::string ATTRIB_KEEP_UNTIL_GOAL = "keep_until_goal"; // v3.0.200 ***** converted to ATTRIB_KEEP_UNTIL_LEG *****
//    const static std::string ATTRIB_KEEP_UNTIL_LEG = "keep_until_leg"; // v3.0.221.15rc5 add LEG support
//    const static std::string ATTRIB_HIDE = "hide"; // v3.0.207.4 // boolean value that will flag a 3D Instance to hide
//    const static std::string PROP_DISPLAY_DEFAULT_OBJECT_FILE_OVER_ALTERNATE = "display_active_object"; // v3.0.200 // boolean. Which 3D Object to display the default or: "alternate_obj_file" attribute one.
//    const static std::string PROP_CAN_BE_DISPLAYED = "can_be_displayed"; // v3.0.200 // boolean. can an instance be drawn in X-Plane world ?
//    const static std::string ATTRIB_ELEV_ABOVE_GROUND_FT = "elev_above_ground_ft"; // v3.0.200 // boolean. can an instance be drawn in X-Plane world ?
//    const static std::string PREFIX_REPLACE_ = "replace_"; // v3.0.200 // all attributes we want to replace, should start with replace_xxx, example: replace_lat, replace_lon. The work after "replace_" must be the same as the original attribute, so we will be able to find it in the property.  
//    const static std::string ATTRIB_LINK_TASK = "link_task"; // v3.0.200 // used with "display_object" element when we want to add task state to the "show/hide" conditions with "cond_script, keep_until.., display_". Value format: "objective.task"
//    const static std::string PROP_LINK_OBJECTIVE_NAME = "link_objective_name"; // v3.0.200 // attrib_task is split to task and objects then this property holds the object name task is in.
//    const static std::string ATTRIB_SPEED_KMH = "speed_kmh"; // km per hour
//    const static std::string ATTRIB_WAIT_SEC = "wait_sec";
//
//
//    // Triggers
//    const static std::string ELEMENT_TRIGGERS    = "triggers";
//    const static std::string ELEMENT_TRIGGER     = "trigger";
//    const static std::string ELEMENT_SCRIPTS     = "scripts";
//    const static std::string ELEMENT_EVENT_SCRIPTS = "event_scripts";
//    const static std::string ELEMENT_SETTINGS      = "settings";
//    const static std::string ELEMENT_CONDITIONS    = "conditions";
//    const static std::string ATTRIB_PLANE_ON_GROUND = "plane_on_ground"; // bool. does trigger fires when plane is on ground ?
//    const static std::string ATTRIB_COND_SCRIPT     = "cond_script"; // script to evaluate trigger as part of the trigger firing conditions
//
//    const static std::string ATTRIB_POST_SCRIPT             = "post_script";
//    const static std::string ATTRIB_SCRIPT_NAME_WHEN_FIRED  = "script_name_when_fired"; // v3.0.213.4 script_name_when_enter was renamed to script_name_when_fired
//    const static std::string ATTRIB_SCRIPT_NAME_WHEN_LEFT   = "script_name_when_left"; // when plane leaves trigger area
//    const static std::string ELEMENT_LOC_AND_ELEV_DATA = "loc_and_elev_data"; //
//    const static std::string ELEMENT_POINT = "point"; //
//    const static std::string ELEMENT_REFERENCE_POINT = "reference_point"; // center of polygonal area or a reference point for triggers based script.
//    const static std::string ELEMENT_RADIUS = "radius"; //
//    const static std::string ELEMENT_ELEVATION_VOLUME = "elevation_volume"; //
//    const static std::string ATTRIB_ELEV_MIN_MAX_FT = "elev_min_max_ft"; // volume of trigger [ --N|++N] or [min|max]
//    const static std::string ATTRIB_ELEV_LOWER_UPPER_FT = "elev_lower_upper_ft"; // volume of trigger [ --N|++N] or [lower|upper] in feet // v3.0.205.3
//    const static std::string ATTRIB_LENGTH_MT = "length_mt"; // radius length in meter
//    const static std::string ATTRIB_ENABLED = "enabled"; // trigger
//
//
//    // Trigger types
//    const static std::string TRIG_TYPE_RAD = "rad"; // radius
//    const static std::string TRIG_TYPE_POLY = "poly"; // polygon zone
//    const static std::string TRIG_TYPE_SLOPE = "slope"; // slope
//    const static std::string TRIG_TYPE_SCRIPT = "script"; // 
//    //const static std::string TRIG_TYPE_FUEL = "fuel"; // 
//    //const static std::string TRIG_TYPE_TRADE = "trade"; // inventory related
//    const static std::string ATTRIB_ANGLE = "angle"; // slope related
//    const static std::string ELEMENT_CALC_SLOPE = "calc_slope"; // slope related // calculate 3 point slope from user data and one origin point
//    const static std::string ATTRIB_BEARING_2D = "bearing_2d"; // slope related // calculate 3 point slope from user data and one origin point
//    const static std::string ATTRIB_LENGTH_WITH_UNITS = "length_with_units"; // slope related // calculate 3 point slope from user data and one origin point
//    const static std::string ATTRIB_SLOPE_ANGLE_3D = "slope_angle_3d"; // slope related // calculate 3 point slope from user data and one origin point
//    const static std::string PROP_HAS_CALC_SLOPE = "has_calc_slope"; // slope related // special boolean property to flag that we read <calc_slope> and it is valid
//
//    const static std::string ATTRIB_LENGTH_NM = "length_nm"; // length_nm, can be used anywhere
//    const static std::string ATTRIB_TRIGGER_LENGTH_NM = ATTRIB_LENGTH_NM; // used in trigger
//
//
//    /// INVENTORIES // v3.0.213.1
//    const static std::string ELEMENT_INVENTORIES = "inventories";
//    const static std::string ELEMENT_INVENTORY   = "inventory";
//    const static std::string ELEMENT_PLANE   = "plane";
//    const static std::string ELEMENT_ITEM_BLUEPRINTS = "item_blueprints";
//    const static std::string ELEMENT_ITEM        = "item"; // inventory item
//    const static std::string ATTRIB_WEIGHT_KG = "weight_kg";
//    const static std::string ATTRIB_QUANTITY  = "quantity";
//    const static std::string ATTRIB_BARCODE = "barcode";
//    const static std::string ATTRIB_LINK_TO_BLUEPRINT = "link_to_blueprint"; // link to blueprint item
//
//
//
//    // MESSAGES
//    const static std::string ELEMENT_MESSAGE_TEMPLATES = "message_templates";
//    const static std::string ELEMENT_MESSAGES = "messages";
//    const static std::string ELEMENT_MESSAGE = "message";
//    const static std::string ATTRIB_MESSAGE_NAME_WHEN_FIRED = "message_name_when_fired"; // trigger message // deprecated message_name_when_enter for better readability
//    const static std::string ATTRIB_MESSAGE_NAME_WHEN_LEFT = "message_name_when_left"; // trigger message
//
//    // trigger other outcomes
//    const static std::string ELEMENT_OUTCOME = "outcome"; // v3.0.221.11+ trigger commands
//    const static std::string ATTRIB_COMMANDS_TO_EXEC_WHEN_FIRED = "commands_to_exec_when_fired"; // v3.0.221.11+ trigger commands
//    const static std::string ATTRIB_COMMANDS_TO_EXEC_WHEN_LEFT = "commands_to_exec_when_left"; // v3.0.221.11+ trigger commands
//    const static std::string ATTRIB_DATAREF_TO_EXEC_WHEN_FIRED = "dataref_to_modify_when_fired"; // v3.0.221.11+ trigger commands
//    const static std::string ATTRIB_DATAREF_TO_EXEC_WHEN_LEFT = "dataref_to_modify_when_left"; // v3.0.221.11+ trigger commands
//
//
//    const static std::string ELEMENT_MIX = "mix"; // hold message channels information
//    const static std::string ATTRIB_MESSAGE = ELEMENT_MESSAGE;
//    const static std::string ATTRIB_MESSAGE_TYPE = "message_type"; // message and queues // comm, back(ground), pad message.
//    const static std::string PROP_IS_MXPAD_MESSAGE = "is_mxpad"; // Internal flag. Define message as PAD message or not.
//    const static std::string PROP_MESSAGE_HAS_TEXT_TRACK = "has_text_track"; //created during class init() and needs to be removed once we read "text" channel
//    const static std::string ATTRIB_MESSAGE_MIX_TRACK_TYPE = "track_type"; // message and queues // comm, back(ground), pad message.
//    const static std::string ATTRIB_PARENT_MESSAGE = "parent_message"; // SoundFragment holds the message name it was defined in so we can stop specific communication channel or background sound.
//    const static std::string ATTRIB_MESSAGE_OVERRIDE_SECONDS_TO_PLAY = "override_seconds_to_play"; // Message/seconds to play/display message. Will override the queMessage display timer rules. Can be use for sound file too but need implementation.
//    const static std::string ATTRIB_MESSAGE_OVERRIDE_SECONDS_TO_DISPLAY_TEXT  = "override_seconds_to_display_text"; // Message/seconds to play/display message. Will override the queMessage display timer rules. Can be use for sound file too but need implementation.
//    const static std::string ATTRIB_MESSAGE_OVERRIDE_SECONDS_CALC_PER_LINE    = "override_seconds_calc_per_line"; // used when plugin calculate per line /seconds to play/display message. Will override the queMessage display timer rules. Can be use for sound file too but need implementation.
//    const static std::string ATTRIB_MESSAGE_MUTE_XPLANE_NARRATOR = "mute_xplane_narrator"; // if mute then we won't use "speak string"
//    const static std::string ATTRIB_MESSAGE_HIDE_TEXT = "hide_text"; // Message/Desc used under
//    const static std::string ATTRIB_SOUND_FILE = "sound_file"; // Message/Desc used under
//    const static std::string ATTRIB_SOUND_VOL = "sound_vol"; // Message/Desc used under
//
//    const static std::string PROP_IMAGE_FILE_NAME = "image_file_name"; // mxpad image file name
//    const static std::string PROP_TEXT_RGB_COLOR = "text_rgb_color"; // mxpad text color. there should be translation
//    const static std::string PROP_MXPAD_PLCAEMENT = "mxpad_label_placement"; // image placement left or right to text
//    const static std::string PROP_MXPAD_LABEL = "mxpad_label"; // label instead of image
//    const static std::string PROP_MXPAD_LABEL_COLOR = "mxpad_label_color"; // label text color
//    const static std::string ELEMENT_MXPAD_MESSAGES = "mxpad_active_messages"; // for savepoint, represent the current messages in MxPad window
//    const static std::string ELEMENT_MXPAD_ACTION_REQUEST = "mxpad_action_request"; // for savepoint, represent the current messages in MxPad window
//    const static std::string ELEMENT_MXPAD_DATA = "mxpad_data"; // for savepoint, represent the root MXPAD related elements
//    const static std::string PROP_CURRENT_MX_PAD_RUNNING = "current_mxpad_running"; // savepoint. stores current running mxpad script name. We need to init after load
//
//    const static std::string PROP_NEXT_MSG = "next_msg"; // v3.0.223.1 automatic find and send next message
//    const static std::string ATTRIB_ADD_MINUTES = "add_minutes"; // v3.0.223.1 add minutes as timelaps. We should configure
//    const static std::string ATTRIB_TIMELAPSE_TO_LOCAL_HOURS = "timelapse_to_local_hours"; // v3.0.223.1 timelapse to next local hour in format "24H:MI"
//    const static std::string ATTRIB_SET_DAY_HOURS = "set_day_hours"; // v3.0.223.1 Immediate set time and day in format "day:24H:MI"
//    const static int MAX_LAPS_I = 24; // v3.0.223.1
//    //const static std::string PROP_MSG_LABEL = "msg_label"; // v3.0.223.1 message label
//    //const static std::string PROP_MSG_LABEL_color = "msg_label_color"; // v3.0.223.1message label color instead of mxpad_label_color
//
//    const static int MXPAD_LINE_WIDTH = 50;
//
//
//    const static std::string CHANNEL_TYPE_TEXT = "text"; // comm / back / pad / text
//    const static std::string CHANNEL_TYPE_COMM = "comm"; // comm / back / pad / text
//    const static std::string CHANNEL_TYPE_BACKGROUND = "back"; // comm / back / pad / text
//    const static std::string CHANNEL_TYPE_PAD = "pad"; // comm / back / pad / text
//
//    // MESSAGES
//    const static std::string ELEMENT_END_MISSION = "end_mission";
//    const static std::string ELEMENT_END_MISSION_SUCCESS = "end_mission_success";
//    const static std::string ELEMENT_END_MISSION_FAIL = "end_mission_fail";
//    const static std::string ELEMENT_END_SUCCESS_IMAGE = "success_image";
//    const static std::string ELEMENT_END_SUCCESS_MSG = "success_msg";
//    const static std::string ELEMENT_END_SUCCESS_SOUND = "success_sound";
//    const static std::string ELEMENT_END_FAIL_IMAGE = "fail_image";
//    const static std::string ELEMENT_END_FAIL_MSG   = "fail_msg";
//    const static std::string ELEMENT_END_FAIL_SOUND = "fail_sound";
//
//    // MAP
//    const static std::string ATTRIB_FILE_NAME = "file_name";
//    const static std::string ATTRIB_FILE_PATH = "file_path";
//    const static std::string ATTRIB_FULL_FILE_PATH = "full_file_path"; // v3.0.217.2
//    const static std::string ATTRIB_REAL_W = "realW"; // real width
//    const static std::string ATTRIB_REAL_H = "realH"; // real height
//
//
//    // MXPAD
//    const static std::string ELEMENT_MXPAD = "mxpad"; // mxpad
//    const static std::string ATTRIB_MANAGE_SCRIPT = "manage_script"; // script that manages the mxpad progress
//    const static std::string ATTRIB_STARTING_FUNCTION     = "starting_function"; // When calling manage_script for first time, which function should be first to be evaluated.
//    const static std::string PROP_NEXT_RUNNING_FUNCTION     = "next_running_function"; // script that manages the mxpad progress
//    const static std::string PROP_CURRENT_RUNNING_FUNCTION  = "current_running_function"; // script that manages the mxpad progress
//    const static std::string PROP_PREV_RUNNING_FUNCTION     = "prev_running_function"; // script that manages the mxpad progress
//
//
//    // MXPAD ACTIONS
//    const static std::string MXPADREQ_TO_EXECUTE      = "mxpad_to_execute"; // mxpad name to execute by Mission(). If there is an active MXPAD this will be ignored.
//    const static std::string MXPADREQ_TO_EXECUTE_FUNC = "mxpad_to_execute_func"; // mxpad function name. must be given with MXPADREQ_TO_EXECUTE so Mission will call MXPAD with starting_function = requested function
//    const static std::string MXPADREQ_TO_ABORT        = "mxpad_to_abort"; // Asks MissoinX to abort any running mxpad
//    const static std::string MXPADREQ_TO_PAUSE = "mxpad_to_pause"; // Pause running MXPAD. Simply we won't execute the script each flc().
//    const static std::string MXPADREQ_TO_CONTINUE  = "mxpad_to_continue"; // v3.0.205.3 continue running MXPAD if was in pause
//
//
//    // PROPERTIES representing core attributes
//    const static std::string PROP_IS_LINKED = "is_linked"; // boolean property to flag a trigger/task as linked. Add it during Objective validation and use it to store "checkpoint" and load/set mission.
//    const static std::string PROP_LINKED_TO = "linked_to"; // boolean property to flag a trigger as linked to a task. checkpoint save/load
//    const static std::string PROP_All_COND_MET_B = "all_conditions_met_b"; // boolean property to flag a trigger as success
//    const static std::string PROP_SCRIPT_COND_MET_B = "script_conditions_met_b"; // boolean property to flag a condition script as success
//    const static std::string PROP_STATE_ENUM = "enum_state"; // enum property describes state of Trigger
//    const static std::string PROP_TRIG_ELEV_TYPE = "trig_elev_type"; // enum property to describe trigger elevation volume
//    const static std::string PROP_IS_VALID = "is_valid"; // boolean property to flag a goal/objective state
//    const static std::string PROP_HAS_MANDATORY = "has_mandatory"; // boolean property to flag a goal/objective with Mandatory Objective
//    const static std::string PROP_IS_COMPLETE = "is_complete"; // boolean property to flag a task is completed or not (achieved or not)
//    //const static std::string PROP_IS_COMPLETE_SUCCESS = "is_complete_success"; // boolean property to flag a task is completed successfully or not.
//    const static std::string PROP_COMPLETE_DESC = "complete_desc"; // boolean property to flag a task is completed successfully or not.
//    const static std::string PROP_ERROR_REASON = "error_reason"; // holds task error explanation
//    const static std::string PROP_COUNTER = "counter"; // message counter
//    const static std::string PROP_MESSAGE_BROADCAST_FOR = "broadcast_for"; // message, to whom was broadcast
//    const static std::string PROP_MESSAGE_TRACK_NAME = "track_name"; // message track name for Queue Message Manager
//    const static std::string PROP_POINTS = "points"; //
//    const static std::string PROP_HAS_ALWAYS_TASK = "has_always_task"; // objective attribute
//
//    // Datarefs
//    const static std::string ATTRIB_DREF_KEY = "key";
//
//    // Mission properties
//    const static std::string PROP_MISSION_CURRENT_LEG = "mission_current_leg"; // savepoint // v3.0.221.15rc5 renamed from PROP_MISSION_CURRENT_GOAL
//    const static std::string PROP_MISSION_STATE = "mission_state"; // MISSION // savepoint
//    const static std::string PROP_MISSION_ABORT_REASON = "mission_abort_reason"; // MISSION // savepoint
//    // Mission Savepoint plane location
//    const static std::string SAVEPOINT_PLANE_LATITUDE  = "savepoint_latitude"; // MISSION // savepoint
//    const static std::string SAVEPOINT_PLANE_LONGITUDE = "savepoint_longitude"; // MISSION // savepoint
//    const static std::string SAVEPOINT_PLANE_ELEVATION = "savepoint_elevation"; // MISSION // savepoint
//    const static std::string SAVEPOINT_PLANE_SPEED = "savepoint_speed"; // MISSION // savepoint // groundspeed
//    const static std::string SAVEPOINT_PLANE_HEADING  = "savepoint_heading"; // MISSION // savepoint // psi
//
//
//    // embedded script - seeded attributes
//    const static std::string MX_ = "mx_";
//    const static std::string EXT_MX_FUNC_CALL    = "mxFuncCall";
//    const static std::string EXT_MX_CURRENT_LEG = "mxCurrentLeg"; // v3.0.221.15rc5 replaces mxCurrentGoal
//    const static std::string EXT_MX_CURRENT_GOAL = "mxCurrentGoal";
//    const static std::string EXT_MX_CURRENT_OBJ  = "mxCurrentObjective";
//    const static std::string EXT_MX_CURRENT_TASK = "mxCurrentTask";
//    const static std::string EXT_MX_CURRENT_TRIGGER = "mxCurrentTrigger";
//    const static std::string EXT_MX_CURRENT_3DOBJECT = "mxCurrent3dObject"; // v3.0.200
//    const static std::string EXT_MX_CURRENT_3DINSTANCE = "mxCurrent3dInstance"; // v3.0.200
//    const static std::string EXT_MX_QM_MESSAGE = "mxQmMessage"; // v3.0.223.1 holds the message name
//    // Task get info
//    const static std::string EXT_mxState = "state"; // string "success/was_success/need_evaluation"
//    const static std::string EXT_mxType = ATTRIB_TYPE; // "mxType"; // string trigger/script/undefined 
//    const static std::string EXT_mxTaskActionName = "taskActionName"; // string // action_code_name
//    const static std::string EXT_mxTaskHasBeenEvaluated = "taskHasBeenEvaluated"; // bool
//
//    // Navigation seed attributes
//    const static std::string EXT_mxNavType = "mxNavType";
//    const static std::string EXT_mxNavLat = "mxNavLat";
//    const static std::string EXT_mxNavLon = "mxNavLon";
//    const static std::string EXT_mxNavHeight = "mxNavHeight";
//    const static std::string EXT_mxNavFreq = "mxNavFreq";
//    const static std::string EXT_mxNavHead = "mxNavHeading";
//    const static std::string EXT_mxNavID   = "mxNavID";
//    const static std::string EXT_mxNavName = "mxNavName";
//    const static std::string EXT_mxNavRegion = "mxNavRegion";
//
//
//    // Queue Message Manager - QMM
//    const static std::string QMM_DUMMY_MSG_NAME_PREFIX = "msg_"; // used to construct new messages from code
//
//    // calculate weight related
//    const static float DEFAULT_PILOT_WEIGHT = 85.0f; // v3.0.213.3  85kg
//    const static float DEFAULT_STORED_WEIGHT = 5.0f; // v3.0.213.3  5kg
//
//    // Base weights by designer
//    const static std::string ELEMENT_BASE_WEIGHTS_KG 	= "base_weights_kg"; // options // mute any sound, include narrator and fmod sounds
//    const static std::string ATTRIB_PILOT      				= "pilot"; // v3.0.213.4 pilot weight in kg
//    const static std::string ATTRIB_PASSENGERS 				= "passengers"; // v3.0.213.4 passengers weight in kg
//    const static std::string ATTRIB_STORAGE    				= "storage"; // v3.0.213.4 storage weight in kg
//    const static std::string ELEMENT_POSITION  				= "position"; // v3.0.223.1 Hidden parameter for now. Will skip plane repositioning at random start.
//    const static std::string ATTRIB_AUTO_POSITION_PLANE	= "auto_position_plane"; // v3.0.223.1 Hidden attribute. Boolean type. true=position plane (default), false=skip position plane.
//
//
//
//    // OPTIONS
//    const static std::string OPT_MUTE_MX_SOUNDS         = "mute_missionx_sound"; // options // mute any sound, include narrator and fmod sounds
//    const static std::string OPT_ABORT_MISSION_ON_CRASH = "abort_mission_on_crash"; // options // bool // abort mission on crash
//    const static std::string OPT_ENABLE_DESIGNER_MODE   = "enable_designer_mode"; // options // bool // 
//    const static std::string OPT_DISPLAY_VISUAL_CUES    = "enable_visual_cues"; // options // bool // 
//    const static std::string OPT_USE_COMPATIBILITY_UI   = "use_compatibility_ui"; //v3.0.211.2 options // For OSX compatibility drawing //
//    const static std::string OPT_AUTO_HIDE_SHOW_MXPAD   = "auto_hide_mxpad"; //v3.0.215.1 auto hide mxpad options 
//    const static std::string OPT_IGNORE_APT_DAT_CACHE   = "ignore_aptdat_cache"; //v3.0.219.12+ used in RandomEngine. When need to get random airport, will skip the option to read from cached file, will only use XPLM library results.
//    const static std::string OPT_AUTO_PAUSE_IN_VR   = "auto_pause_in_vr"; //v3.0.221.6 
//    const static std::string OPT_DISPLAY_MISSIONX_IN_VR = "display_missionx_in_vr"; //v3.0.221.7
//    const static std::string OPT_DISABLE_PLUGIN_COLD_AND_DARK_WORKAROUND = "disable_plugin_cold_and_dark_workaround"; //v3.0.221.10
//
//    // the following optional attributes are read from General_Properties of the mission, they are not stored in option file.
//    // In future build, we might add base weights options to the "Option" screen.
//    const static std::string OPT_PILOT_BASE_WEIGHT      = "pilot_base_weight"; //v3.0.213.3 assist in calculating weight of plane = empty plane + pilot + passengers + lauggage
//    const static std::string OPT_STORAGE_BASE_WEIGHT    = "storage_base_weight"; //v3.0.213.3 other payload
//    const static std::string OPT_PASSENGERS_BASE_WEIGHT = "passengers_base_weight"; //v3.0.213.3 passengers
//
//    // TTF FONTS
//    const static std::string FONT_TTF = "Adamina-Regular.ttf"; // "Ubuntu-R.ttf"; // true type fonts
//
//
//    // PLUGIN MESSAGES
//    const static std::string MESSAGE_NEED_TO_RESTART_XPLANE = "Please restart X-Plane so changes will take effect!";
//
//    //// COMMANDS related elements // v3.0.221.9
//    const static std::string ELEMENT_XP_COMMANDS = "xp_commands";
//    const static std::string ATTRIB_COMMANDS     = "commands"; // use as an attribute at goal level. example: <fire_commands_at_goal_begin commands="bell/412/push,bell/412/" /> we should execute each command at its own flb
//    const static std::string ELEMENT_FIRE_COMMANDS_AT_LEG_START  = "fire_commands_at_leg_start";
//    const static std::string ELEMENT_FIRE_COMMANDS_AT_GOAL_START  = "fire_commands_at_goal_start";
//    const static std::string ELEMENT_FIRE_COMMANDS_AT_LEG_END    = "fire_commands_at_goal_end";
//    const static std::string ELEMENT_FIRE_COMMANDS_AT_GOAL_END    = "fire_commands_at_goal_end";
//
//    /// TBD not yet implemented
//    const static std::string ATTRIB_FIRE_COMMANDS_ON_SUCCESS = "fire_commands_on_success"; // can be used with triggers
//    const static std::string ATTRIB_FIRE_COMMANDS_ON_FAILURE = "fire_commands_on_failure"; // can be used with triggers
//    const static std::string ATTRIB_FIRE_COMMANDS_AT_TASK_END    = "fire_commands_at_task_end"; // TODO: not sure if we want this
//    /// END TBD not yet implemented
//
//    const static std::string ATTRIB_START_COLD_AND_DARK    = "start_cold_and_dark"; // v3.0.221.10 briefers additional_location attribute
//    const static std::string ELEMENT_DATAREFS_START_COLD_AND_DARK    = "datarefs_start_cold_and_dark"; //  v3.0.221.10 will be stored at briefer level
//
//    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    const static std::string MX_TRUE  = "true"; // used when reading XML attributes
//    const static std::string MX_FALSE = "false"; // used when reading XML attributes
//    const static std::string MX_YES   = "yes"; // used when reading XML attributes
//    const static std::string MX_NO    = "no"; // used when reading XML attributes
//
//    //////////////////////////////  BITMAP BITMAP BITMAP  ////////////////////////////// 
//    const static std::string BITMAP_TRANSPARENT_BLACK = "bground_black_rgba_16x16.png";
//    const static std::string BITMAP_TRANSPARENT_BLUEISH = "bground_blueish_rgba_16x16.png";
//    //const static std::string BITMAP_TRANSPARENT_BLUEISH_ON_BUTTONS = "bground_comp_button_16x16.png";
//    const static std::string BITMAP_COMP_TRANSPARENT_MXPAD = "bground_comp_mxpad_16x16.png";
//    const static std::string BITMAP_NEXT = "next.png";
//    const static std::string BITMAP_PREV = "prev.png";
//    const static std::string BITMAP_START = "start.png";
//    const static std::string BITMAP_QUIT = "quit.png";
//    const static std::string BITMAP_HOME = "home.png";
//    const static std::string BITMAP_MAP = "map.png";
//    const static std::string BITMAP_GEAR = "gear_48x48.png";
//    const static std::string BITMAP_INVENTORY = "inventory.png";
//    const static std::string BITMAP_INVENTORY_MXPAD = "inventory_mxpad.png";
//    const static std::string BITMAP_CANCEL_RED = "cancel_red.png";
//    const static std::string BITMAP_CANCEL = "cancel.png";
//    const static std::string BITMAP_CANCEL_6SIDE = "cancel6side.png";
//    const static std::string BITMAP_CANCEL_GREY = "cancel_grey.png";
//    const static std::string BITMAP_CLOSE_BLUE = "close_blue.png";
//    const static std::string BITMAP_CLOSE_RED = "close_red.png";
//    const static std::string BITMAP_LOAD_SAVEPOINT = "load_savepoint.png";
//    const static std::string BITMAP_CREATE_SAVEPOINT = "create_savepoint.png";
//    const static std::string BITMAP_MXPAD = "headset_64x64.png";
//    const static std::string BITMAP_LOAD_MISSION = "load_mission.png";
//    const static std::string BITMAP_NO_MISSION = "no_mission.png";
//    const static std::string BITMAP_SKIN = "skins/skin.png"; // For NUKLEAR
//    const static std::string BITMAP_SKIN_NO_TRANS = "skins/skin_no_trans.png"; // For NUKLEAR but we use transparent
//    const static std::string BITMAP_SKIN_MXPAD_GENERIC = "skins/mxpad_skin_generic.png"; // generic is for the compatible UI and not the NUKLEAR based
//    const static std::string BITMAP_AUTO_HIDE_EYE_FOCUS = "eye_focus_16x16.png"; // used in MX-Pad to show if auto hide is active. 
//    const static std::string BITMAP_AUTO_HIDE_EYE_FOCUS_DISABLED = "eye_focus_disabled_16x16.png"; // used in MX-Pad to show if auto hide is active. 
//
//
//    const static std::string WHITE = "white";
//    const static std::string RED = "red";
//    const static std::string YELLOW = "yellow";
//    const static std::string GREEN = "green";
//    //const static std::string ORANGE = "orange";
//    //const static std::string PURPLE = "purple";
//    const static std::string BLUE = "blue";
//    const static std::string BLACK = "black";
//
//
//    const static float MANDATORY_R = 0.99f;
//    const static float MANDATORY_G = 0.001f;
//    const static float MANDATORY_B = 0.0f;
//
//    const static float DEFAULT_R = 0.9f;
//    const static float DEFAULT_G = 0.65f;
//    const static float DEFAULT_B = 0.0f;
//
//    const static float TASK_R = 0.9f;
//    const static float TASK_G = 0.9f;
//    const static float TASK_B = 0.0f;
//
//    const static int   FONT_HEIGHT = 8; // 14; // x-plane default font height
//    // ####### MXPAD SELECTION ####
//    const static int MAX_MXPAD_SELECTIONS = 6;
//    const static std::string MXPAD_SELECTION_DELIMITER = "#SEL#";
//
//    // MOVING OBJECT Constants
//    const static float DEFUALT_3D_OBJECT_SPEED_KMH = 10.0f;
//
//    // Generates Mission elements and attributes
//    const static std::string ATTRIB_USE_TRIGGER_NAME_FROM_TEMPLATE = "use_trigger_from_template"; // v3.0.221.10 will search for element <trigger name="" > in the template and will use it instead of the default map trigger template. This will allow for special triggers that are not just based on radius but maybe also need script This is an extension to the original trigger implementation.
//    const static std::string ATTRIB_ADD_TASKS_FROM_TEMPLATE = "add_tasks_from_template"; // v3.0.221.10 will search for element <tasks name="" > and will append all tasks in it to the current objective.
//    const static std::string ATTRIB_ADD_MESSAGES_FROM_TEMPLATE = "add_messages_from_template"; // v3.0.221.10 will search for element with tag <{add_messages_from_template}> and will append all messages in it to xMessages
//    const static std::string ATTRIB_ADD_TRIGGERS_FROM_TEMPLATE = "add_triggers_from_template"; // v3.0.221.10 will search for element with tag <{add_triggers_from_template}> and will append all triggers elements to xTriggers
//    const static std::string ATTRIB_ADD_SCRIPTS_FROM_TEMPLATE = "add_scripts_from_template"; // v3.0.221.10 will search for element with tag <{add_scripts_from_template}> and will append all tasks in it to xScripts.
//
//    const static std::string ATTRIB_LOCATION_TYPE = "location_type";
//    const static std::string ATTRIB_LOCATION_VALUE = "location_value";
//    const static std::string ATTRIB_EXPECTED_LAND_ON = "expected_land_on";
//    //const static std::string ATTRIB_ACTION_TYPE = "action_type"; // we will use TYPE for now.  v3.0.219.3 used with random <point>, assist in defining which action should be taken in that location. Example: "hover". This will override goal_type if it is land or hover.
//    
//    const static std::string ELEMENT_TEMPLATE = "TEMPLATE";
//    const static std::string ATTRIB_TEMPLATE = "template";
//    const static std::string ELEMENT_BRIEFER_AND_START_LOCATION = "briefer_and_start_location";
//    const static std::string ELEMENT_EXPECTED_LOCATION = "expected_location";
//    
//    const static std::string ATTRIB_PLANE_TYPE = "plane_type"; // v3.0219.6 used in template so we know which type of plane: [heli|ga|big] [helos|jets|turboprops|props|heavy]
//    const static std::string PLANE_TYPE_HELOS   = "helos"; // v3.0219.9 Helicopter
//    const static std::string PLANE_TYPE_JETS    = "jets"; // v3.0219.9 Jet planes
//    const static std::string PLANE_TYPE_PROPS   = "props"; // v3.0219.9 Propelor
//    const static std::string PLANE_TYPE_TURBOPROPS = "turboprops"; // v3.0219.9 Turbo Propeler, general heavier and larger types
//    const static std::string PLANE_TYPE_HEAVY     = "heavy"; // v3.0219.9 The big dadies :-)
//
//
//
//
//    const static std::string ELEMENT_CONTENT = "content"; // v3.0.219.14 Used in random templates only
//    const static std::string ATTRIB_LIST     = "list";    // v3.0.219.14 Used in random templates only
//
//
//    const static std::string LOCATION_TYPE_SAME_AS  = "same_as"; // v3.0.219.1 "generate template" used in "goal tempalte type" (implemented)
//    const static std::string GOAL_TEMPLATE_VAL_START  = "start"; // v3.0.219.3 "generate template" used in "goal tempalte type" (implemented)
//    const static std::string GOAL_TEMPLATE_VAL_LAND     = "land"; // v3.0.219.1 "generate template" used in "goal tempalte type" (implemented)
//    const static std::string GOAL_TEMPLATE_VAL_HOVER     = "hover"; // v3.0.219.3 "generate template" used in "goal tempalte type" ()
//    const static std::string GOAL_TEMPLATE_VAL_DELIVER     = "deliver"; // v3.0.219.6 "generate template" used in "goal tempalte type" ()
//    //const static std::string GOAL_TEMPLATE_VAL_SAME_AS_GOAL = "same_as_goal"; // v3.0.219.? " Not implemented yet.
//
//    const static std::string EXPECTED_LOCATION_TYPE_ICAO = "icao"; // v3.0.221.4 
//    const static std::string EXPECTED_LOCATION_TYPE_XY = "xy"; // xy, icao,goal,start  (xy can have string based random or number random that represents random lat/long
//    const static std::string EXPECTED_LOCATION_TYPE_NEAR = "near"; // "near" usually with land type, searches for nearest ICAO relative to last goal position
//
//
//    //const static std::string ELEMENT_GOAL_MEDEVAC = "goal_medevac";  // v3.0.219.13
//    //const static std::string ELEMENT_GOAL_DELIVERY= "goal_delivery"; // v3.0.219.13
//    //const static std::string ELEMENT_GOAL_LAND    = "goal_land";     // v3.0.219.13
//    const static std::string ELEMENT_ICAO    = "icao";     // v3.0.221.5 used in RandomEngine when we try convert any icao tag to point
//
//    const static std::string GENERATE_TYPE_MEDEVAC = "medevac";
//    const static std::string GENERATE_TYPE_DELIVERY = "delivery"; // v3.0.219.6
//    const static std::string DEFAULT_RANDOM_IMAGE_FILE = "random.png"; // v3.0.217.6
//
//    const static std::string TERRAIN_TYPE_MEDEVAC_SLOPE = "medevac_slope"; // v3.0.219.12+ will be used when using <display_object_set> if the template name is "medevac" and slope is larger than plugin define max terrain slope then replace with "medevac_slope" object sets.
//
//    const static std::string ATTRIB_RADIUS_MT = "radius_mt"; // v3.0.217.6 used in point template
//    const static std::string ATTRIB_LOC_DESC = "loc_desc"; // v3.0.217.6 used in point template
//    const static std::string ATTRIB_RANDOM_TAG = "random_tag"; // v3.0.219.1 the name of the element tag to search for random data. Can be random 3D Object or <point> etc.
//    const static std::string ATTRIB_SET_NAME = "set_name"; // v3.0.219.14 the name of a sub element that we need to random pick from.
//    const static std::string ATTRIB_RANDOM_WATER_TAG = "random_water_tag"; // v3.0.219.10 the name of the element tag to search for random water 3D Objects
//    const static std::string ATTRIB_RELATIVE_POS_BEARING_DEG_DISTANCE_MT = "relative_pos_bearing_deg_distance_mt"; // v3.0.219.1 position 3D Object providing: "bearing|distance"
//    //const static std::string ATTRIB_MAX_GOALS = "max_goals"; // v3.0.219.2 Max goals random engine can create. Plugin should limit the number to 8
//    const static std::string ATTRIB_EXCLUDE_OBJ = "exclude_obj"; // v3.0.219.5 used in <point> hint Engine which objects to remove from generated mission if exists.
//    const static std::string ATTRIB_INCLUDE_OBJ = "include_obj"; // v3.0.219.5 used in <point> hint Engine which objects to include in generated mission. Include can only pick one: "hiker,man,woman" will pick one and add it. overlaping location restriction.
//    const static std::string PROP_IS_WET = "is_wet"; // v3.0.219.10 holds point data if water or terrain (land)
//    const static std::string PROP_FOUND_ICAO = "found_icao"; // v3.0.219.12+ flags NavAidInfo as found in optimized aptData information
//    const static std::string ATTRIB_LIMIT_TO_TERRAIN_SLOPE = "limit_to_terrain_slope"; // v3.0.219.12+ used in <display_object> element where attribute "limit_to_terrain_slope" will assist if to display the object on certain terrain
//    const static std::string ATTRIB_IS_SET_OF_FLIGHT_LEGS = "is_set_of_flight_legs"; //v3.0.221.7 renamed to is_set_of_goals //v3.0.219.12+ used in <display_object> element where attribute "limit_to_terrain_slope" will assist if to display the object on certain terrain
//    //const static std::string ATTRIB_IS_SET_OF_GOALS = "is_set_of_goals"; //v3.0.221.7 renamed to is_set_of_goals //v3.0.219.12+ used in <display_object> element where attribute "limit_to_terrain_slope" will assist if to display the object on certain terrain
//    const static std::string ATTRIB_IS_RANDOM_COORDINATES = "is_random_coordinates"; // v3.0.219.12+ used in <display_object> element where attribute "limit_to_terrain_slope" will assist if to display the object on certain terrain
//    const static std::string ATTRIB_POI_TAG = "poi_tag"; // v3.0.219.14+ point of interest tag a <point> attribute. We will add to the GPS not as a target but as a point you can navigate too.
//    const static std::string ATTRIB_BASE_ON_EXTERNAL_PLUGIN = "base_on_external_plugin"; // v3.0.221.9 values: "true" or "false", flag that tells plugin to use external dataref to flag success of task. You still need to define a trigger since we are not sure that there is any plugin listenenig and working with these datarefs
//    const static std::string ATTRIB_CUSTOM_HOVER_TIME = "hover_time_sec_random"; // v3.0.221.11 random hover time in seconds. Can be in format "10" or "10-40"
//    const static std::string ATTRIB_CUSTOM_FLIGHT_LEG_DESC_FLAG = "custom_flight_leg_desc_flag"; // v3.0.221.15rc5 // v3.0.221.11 does goal has custom description
//    //const static std::string ATTRIB_CUSTOM_GOAL_DESC_FLAG = "custom_goal_desc_flag"; // v3.0.221.11 does goal has custom description
//
//    const static std::string ATTRIB_PICK_LOCATION_BASED_ON_SAME_TEMPLATE_B = "pick_location_based_on_same_template_b"; //v3.0.221.15rc5 renamed from pick_location_based_on_same_goal_template_b //  v3.0.221.15 rc3 an attribute that holds flag if random picked <point> should be same as the flight leg template type.
//    const static std::string ATTRIB_MIN_VALID_FLIGHT_LEGS = "min_valid_flight_legs"; // v3.0.221.15 rc3.2 minimum number of goals so mission would be valid.
//    const static std::string ATTRIB_SKIP_AUTO_TASK_CREATION_B = "skip_auto_task_creation_b"; // v3.0.221.15 rc4 skip auto task creation when building a goal. Designer probably will inject his/her tasks in goal_special_directives
//    const static std::string PROP_IS_LAST_FLIGHT_LEG = "is_last_flight_leg"; // v3.0.223.1 used during random mission generation
//
//
//    const static std::string APT_1_LAND_AIRPORT_HEADER_CODE_v11 = "1 "; // v3.0.219.12+ apt dat codes with space. add trim if you don't want space
//    const static std::string APT_16_SEAPLANE_BASE_HEADER_CODE_v11 = "16 "; // v3.0.219.12+ apt dat codes with space. add trim if you don't want space
//    const static std::string APT_17_HELIPORT_HEADER_CODE_v11 = "17 "; // v3.0.219.12+ apt dat codes with space. add trim if you don't want space
//    const static std::string APT_1300_RAMP_CODE_v11 = "1300 "; // v3.0.219.12+ apt dat codes with space. add trim if you don't want space
//    const static std::string APT_100_LAND_RW_CODE_v11 = "100 "; // v3.0.219.12+ apt dat codes with space. add trim if you don't want space
//    const static std::string APT_101_WATER_RW_CODE_v11 = "101 "; // v3.0.219.12+ apt dat codes with space. add trim if you don't want space
//    const static std::string APT_102_HELIPAD_RW_CODE_v11 = "102 "; // v3.0.219.12+ apt dat codes with space. add trim if you don't want space
//
//
//    const static std::string ATTRIB_DISTANCE_NM = "distance_nm"; // v3.0.219.11 hold distance from last goal to current
//    const static std::string ATTRIB_SHARED_TEMPLATE_TYPE = "shared_template_type"; // v3.0.221.7 used with shared type datarefs
//    const static std::string ATTRIB_SHARED_FLIGHT_LEG_TEMPLATE = "shared_flight_leg_template"; // v3.0.221.15rc5 support leg/ Replaces the name shared_goal_template  // v3.0.221.7 used with shared type datarefs
//    const static std::string ATTRIB_SHARED_GOAL_TEMPLATE = "shared_goal_template"; // v3.0.221.7 used with shared type datarefs
//
//
//    // helpers attribute for random creation
//    const static std::string ATTRIB_TARGET_POS = "target_pos"; // v3.0.221.9 target position - format: "{lat}|{lon}|{elev}. Internal use. holds goal main target location. Useful to share lat/lon with shared datarefs. Also we should use it when we determind not random mission, it should be the first task in a goal that is mandatory depends on trigger with location values.
//    const static std::string ATTRIB_TASK_TRIGGER_NAME = "task_trigger_name"; // v3.0.219.2 store the name of the trigger the goals task is dipendent apon.
//    const static std::string ATTRIB_HOVER_TIME = "hover_time"; // v3.0.219.3 store hover time, which is also task success evaluation time
//    const static std::string ATTRIB_DEPENDS_ON = "depends_on"; // v3.0.219.11 depends on other goal. Used in random mission generator template.
//    const static std::string ATTRIB_TERRAIN_SLOPE = "terrain_slope"; // v3.0.221.3 holds terrain slope from calculate_slope_for_build_goal_thread ( mission.flcPRE() )
//
//    const static int DEFAULT_HOVER_HEIGHT_FT = (int)(100 * missionx::meter2feet);
//    const static std::string DEFAULT_TEMPLATE_EXPECTED_LOCATION_LAND_ON = "any";
//    const static std::string DEFAULT_TEMPLATE_DELIVERY_EXPECTED_LOCATION_TYPE_VALUE = EXPECTED_LOCATION_TYPE_ICAO; //"icao"; // v3.0.219.6 for delivery type template
//    const static std::string EXPECTED_LOCATION_VALUE_DEFAULT_DISTANCE  = "30"; // 30nm for XY based option
//    const static std::string DEFAULT_INVENTORY_RADIUS_MT  = "100"; // v3.0.219.7 used in RandomEngine.injectInventory() function
//
//    const float DEFAULT_RANDOM_POINT_JUMP_NM  = 0.5f; // v3.0.219.5 used in RandomEngine::getRandomAirport(). Represent how much nautical miles to jump each airport search
//    const int MAX_DISTANCE_TO_SEARCH_AIRPORT = 1000; // v3.0.219.5 used in RandomEngine::getRandomAirport(). Represent the maximum distance we allow to search for next airport in nautical miles.
//    const float MIN_DISTANCE_TO_SEARCH_AIRPORT = 1.5f; // v3.0.219.12 used in RandomEngine::getRandomAirport(). Represent the minimum distance to filtter out airports.
//    const int DEFAULT_RANDOM_DEGREES_TO_EXCLUDE  = 20; // v3.0.219.5 used in RandomEngine::getRandomAirport(). Represent the degrees to include in the "exclude" configuration
//    const float MAX_SLOPE_TO_LAND_ON  = 6.0f; // v3.0.219.11
//
//    //const static std::string ATTRIB_RANDOM_PICK_TARGET_IN_AREA = "random_pick_target_in_area"; // in <expected_location> random template
//    
//    const static std::map <std::string, int> MAP_IGNORE_PROPS_DURING_IO = { {PROP_ERROR_REASON, 0} };
//
//
//} // end namespace mxconst

 
#endif // XX_MY_CONSTANTS_HPP


