#ifndef UTILS_H_
#define UTILS_H_
#pragma once

/**************


**************/
#include <random> // 
#include <unordered_set>
#include <deque>
#include <set>

#include "XPLMScenery.h"

#include "xx_rwmeter_constants.hpp"
#include "rwm_const.h"
#include "tokenizer/Tokenizer.h" // 
#include "../io/system_actions.h"
#include "MxUtils.h"


using namespace std;
using namespace rwmeter;

#ifdef __cplusplus
extern "C" {
#endif

//#define UNIX_EOL 13            /* Decimal code of Carriage Return char */
//#define LF 10            /* Decimal code of Line Feed char */
//#define EOF_MARKER 26    /* Decimal code of DOS end-of-file marker */
//#define MAX_REC_LEN 2048 /* Maximum size of input buffer */

#ifdef __cplusplus
}
#endif

namespace rwmeter
{

  typedef enum _unitsOfMeasure : uint8_t
  {
    unit_unsupports,
    ft, // feet/foot
    meter,
    km, // kilometers
    nm // nautical miles
  } mx_units_of_measure;

  static rwm_const drefConst;

class Utils : public mxUtils
{
public:
  Utils();
  virtual
  ~Utils();

  using mxUtils::formatNumber;
  using mxUtils::getPrecision;
  using mxUtils::isElementExists;
  using mxUtils::stringToNumber;


  static std::string logFilePath;

 // From Sandy Barboar plugin example: TestQuaternions.c
  typedef struct _QUATERNION
  {
    double w = 0.0;
    double x = 0.0;
    double y = 0.0;
    double z = 0.0;
  } QUATERNION;


  // From Sandy Barboar plugin example: TestQuaternions.c
  typedef struct _HPR
  {
    float Heading = 0.0f;
    float Pitch = 0.0f;
    float Roll = 0.0f;
  } HPR;



  ///*  Snagar */

  // For logs, try to use Log::logMsg() first, unless you have compilation errors when including Log() class.
  // Utils::logMsg will replace Log::logMsg.
  // Classes won't have compilation issues if they will include Utils() class.
  //static void logMsg(std::string message, bool writeToXPLog = true);



  ////////////////////// Convert Units of Measure ////////////////

  static rwmeter::mx_units_of_measure translateStringToUnits(std::string &inVal);
  static float convertNmTo(float & nm, mx_units_of_measure from);

  static float convertToNm(float & v, mx_units_of_measure from);


 // TEMPLATES

  /* *************************************************************** */
  /* *************************************************************** */
  /* *************************************************************** */
  template <class Container >
  static bool getElementAsDuplicateFromMap(Container& inMap, const typename Container::key_type& key, typename Container::mapped_type &outMappedType)
  {
    bool found = false;
    typename Container::mapped_type dummy;

    //typename Container::const_iterator iter = inMap.find(key);
    typename Container::const_iterator iter = inMap.find(key);
    if (iter != inMap.end())
    {
      found = true;
      outMappedType = inMap[key];
    }

    return found;
  }

  /* *************************************************************** */


  //template <class Container>
  //static void addElementPointerToMap(Container& inMap, const typename Container::key_type& key, typename Container::mapped_type *inElement)
  //{
  //  inMap.insert(make_pair(key, inElement));
  //}


  /* *************************************************************** */


  template <class Container>
  static void addElementToMap(Container& inMap, const typename Container::key_type& key, typename Container::mapped_type inElement)
  {
    inMap.insert(make_pair(key, inElement));
  }

  /* *************************************************************** */

  template <class Container>
  static bool addElementToMap(Container& inMap, const typename Container::key_type& key, typename Container::mapped_type inElement, std::string& errText)
  {
    bool success = true;
    errText.clear();

    // check if exists
    typename Container::const_iterator iter = inMap.find(key);
    if (iter == inMap.end())
      inMap.insert(make_pair(key, inElement));
    else
    {
      success = false;
      errText = "Key: " + key + ", is already present. Change key name.";
    }

    return success;
  } // end template


  /* *************************************************************** */

    template <class Container>
    static void cloneMap(Container& inSource, Container& inTarget)
    {
    
        // loop over source and insert into target map
        inTarget.clear();
        
        for (auto a : inSource )
        {
            Utils::addElementToMap(inTarget, a.first, a.second);
        }
 
        
    } // end template
  /* *************************************************************** */

  // Number to String Template - WITH precision formating
//  template <typename N>
//  static std::string  formatNumber ( N inVal, int precision )
//  {
//    std::ostringstream o;
//    if (!(  o << std::setiosflags(ios::fixed) << std::setprecision(precision) << inVal )  )
//    {
//#ifdef MX_EXE
//      std::cout << "The number: " << (N)inVal << ", could not be converted." << std::endl;
//#else
//      std::snprintf ( LOG_BUFF, 30, " The number %f, could not be converted.", (float)inVal );
//      Log::logMsg( LOG_BUFF, format_type::none );
//#endif
//    }
//
//
//    return o.str();
//  }


  /* *************************************************************** */

  // Convert String into Number
  //template <typename N>
  //static N stringToNumber(std::string s)
  //{
  //  std::istringstream is(s);
  //  N n;
  //  is >> n;

  //  return n;
  //}
  /* *************************************************************** */

  // Convert String into Number with precision
  template <typename N>
  static N stringToNumber(std::string s, int precision)
  {
    std::istringstream is(s);
    N n;
    std::string num; num.clear();
    for (int i = 0; i < precision; i++)
    {
      char c = '\0';
      is.get(c);
      num += c;
    }
    std::istringstream isNum(num);
    isNum >> n;

    //is >> std::setprecision(precision) >> std::setiosflags(ios::fixed) >> n;
    

    return n;
  }



/* *************************************************************** */
  //template <typename N>
  //static int getPrecision(N n)
  //{
  //  int count = 0;

  //  if (std::is_floating_point<N>::value)
  //  {
  //    n = abs(n);
  //    n = n - int(n); // remove left of decimal.
  //    while (n >= 0.0000001 && count < 7) // limit precision to 8
  //    {
  //      n = n * 10;
  //      count++;
  //      n = n - int(n); // remove left of decimal
  //    }
  //  } // if floating point


  //  return count;
  //}

/* *************************************************************** */

  static int getPrecisionFromString(std::string &inValue);

  /* *************************************************************** */

  //static bool is_alpha(const std::string &str);

  ///* *************************************************************** */

  //static bool is_digits(const std::string &str);

  ///* *************************************************************** */

  ////Is string has only these characters - "0123456789.", and no more than 1 "." !!!
  //static bool is_number(const std::string& s);

/* *************************************************************** */


//template <typename T>
//static bool  isStringNumber( T& t, const std::string& s, std::ios_base& (*f)(std::ios_base&) )
//{
//  std::istringstream iss(s);
//
//  return !(iss >> f >> t).fail();
//} // isStringNumber


  /* *************************************************************** */

  
  /* *************************************************************** */
// splitStringToLines() function will create lines of strings based on limiting width sent to it.
// The new line will be determined by the ";" character or if reached "width" characters.
// The function concatenate one character by one and evaluate each time the new word.
static std::vector<std::string> splitStringToLines(std::string source, size_t width);

/* *************************************************************** */


  /**
    replaceChar1WithChar2
    Replaces a character with another character or string of characters
    Uses: remove the LF and UNIX_EOL from strings
  */
static std::string replaceChar1WithChar2(std::string inString, char charToReplace, std::string newChar);

static std::string replaceChar1WithChar2_v2(std::string inString, char charToReplace, std::string newChar);

/* *************************************************************** */
/**
replaceCharsWithString
Replaces set of characters, one by one, with another string of characters
Uses: remove the LF and UNIX_EOL from strings
*/
static void replaceCharsWithString(std::string &outString, std::string charsToReplace, std::string newChar);

/* *************************************************************** */
static std::string replaceStringWithOtherString(std::string inStringToModify, std::string inStringToReplace, std::string inNewString, bool flag_forAllOccurances = false);

/* *************************************************************** */
static std::string getXPlanePrefFileAndPath ( string fileName )
{
	char path[1024];
  std::string result; result.clear();

	XPLMGetPrefsPath(path);
	XPLMExtractFileAndPath(path);
  result = std::string(path);
  result += XPLMGetDirectorySeparator() + fileName;

  return result;
}

/* *************************************************************** */
/* *************************************************************** */

static std::string getXPlaneInstallFolder( )
{
  std::string dir; dir.clear();
  char sys_path[1024];

  XPLMGetSystemPath(sys_path);
  dir.assign(sys_path);

  return dir;
}

/* *************************************************************** */
static std::string getXplanePluginsPath()
{
  std::string result; result.clear();
  char path[1024];
  XPLMGetSystemPath(path);
  result = std::string(path);
  result += std::string("Resources") + XPLMGetDirectorySeparator() + "plugins";
  return result;
}

/* *************************************************************** */

static std::string getPluginDirectoryWithSep( string pluginDirName )
{
  std::string result; result.clear();
  
  result = getXplanePluginsPath();  
  result += XPLMGetDirectorySeparator() + pluginDirName + XPLMGetDirectorySeparator();

  return result;
}

/* *************************************************************** */

/* snagar */
static std::string getCustomDirMissionX_WithSep(bool isRelative = false)
{
  std::string customMissionxPath;
  customMissionxPath.clear();

#ifdef MX_EXE
  customMissionxPath = std::string(mxconst::CUSTOM_MISSIONX_PATH) + XPLMGetDirectorySeparator();
#else
  char sysPath[1024];
  memset(sysPath, '\0', sizeof(sysPath));

  XPLMGetSystemPath(sysPath);

  if (isRelative)
    customMissionxPath.append("Custom Scenery").append(XPLMGetDirectorySeparator()).append("missionx").append(XPLMGetDirectorySeparator());
  else
    customMissionxPath.append(sysPath).append("Custom Scenery").append(XPLMGetDirectorySeparator()).append("missionx").append(XPLMGetDirectorySeparator());

#endif

return customMissionxPath;

}



/* *************************************************************** */
static std::string getCustomSceneryRelativePath()
{
  static std::string str;
  str = "Custom Scenery";
  str.append(XPLMGetDirectorySeparator());

  return str;
}


/* *************************************************************** */

/* *************************************************************** */

/* *************************************************************** */

/* *************************************************************** */

double static calcDistanceBetween2Points_nm(double gFromLat, double gFromLong, double gTargetLat, double gTargetLong); // calculate distance between 2 points in Nauticle Miles

/* *************************************************************** */
double static calcBearingBetween2Points(double gFromLat, double gFromLong, double gTargetLat, double gTargetLong);

/* *************************************************************** */
// provide two points and elevation between them. Function will retriev the slope angle
double static calcSlopeBetween2PointsWithGivenElevation(double gFromLat, double gFromLong, double gTargetLat, double gTargetLong, double relativeElevInFeet);

/* *************************************************************** */

float static calcElevBetween2Points_withGivenAngle_InFeet(float distanceBetweenPointsInFeet, float slopeAngle);

/* *************************************************************** */
/**
 * findCoordinateBasedOnDistanceAndAngle function return a Point based on location.
 * inLat, inLon: plane latitude/longitude
 * inHdg: the function angles are (clockwise): 0, 270, 180, 90
 *        x-plane angles (clockwise): 0, 90, 180, 270
 *        In order to compensate for the un-identical measures, we use 360 - Angle in order to be on par with X-Plane angles.
 *        The Headings must be in PSI and not in magnetic PSI.
 *        PSI: sim/flightmodel/position/psi
 */

void static calcPointBasedOnDistanceAndBearing_2DPlane(double& outLat, double& outLon, double inLat, double inLon, float inHdg, double inDistance);

/* *************************************************************** */
/* *************************************************************** */

std::vector<std::string> static buildSentenceFromWords(std::vector<std::string> inTokens, size_t allowedSentenceLength, size_t maxLinesAllowed/*, size_t *outRealLinesCount*/);

/* *************************************************************** */
/* *************************************************************** */

// split string by fetching last to characters from a string, and returning the split strings
static bool extractUnitsFromString(std::string inNumWithUnits, std::string &outNumber, std::string &outUnit);

/* *************************************************************** */
// return a vector of 2. 1: number and 2: string. If vector < 2 then it is not a number and then a string
// example: 20H = 20,H
std::vector<std::string> static splitStringToNumberAndChars(std::string &inString, std::string delimeter = EMPTY_STRING); 

/* *************************************************************** */
std::vector<std::string> static splitString(std::string inString, std::string delimateChars);
/* *************************************************************** */

std::map<int, std::string> static splitStringToMap (std::string inString, std::string delimateChars); // returns set of key,values. Key is seq starting from 0... while value is string


/* *************************************************************** */

std::list<std::string> static splitStringUsingStringDelimiter(std::string & inString, std::string delimateChars);

/* *************************************************************** */
// Extract base string from a string. Good to extract file name without the extension "[file].exe", for example.
static std::string extractBaseFromString(std::string inFullFileName, std::string delimiter = ".", std::string *outRestOfString = nullptr);


/* *************************************************************** */
// Extract base string from a string. Good to extract file name without the PATH, for example.
static std::string extractLastFromString(std::string inFullFileName, std::string delimiter = ".", std::string *outRestOfString = nullptr);



/* *************************************************************** */

// Simple template to split string into numeric vector
// Do not use strings with this function. For strings use the simple: "splitStringToNumbers()
template <class T>
std::vector<T> static splitStringToNumbers(std::string inString, std::string delimateChar)
{
  //flag_isNumber = true;
  std::vector<T> vecResult;
  std::vector<std::string> vecSplitResult;

  vecSplitResult.clear();
  vecSplitResult = Utils::splitString(inString, delimateChar);
  for (auto s : vecSplitResult)
  {    
    if (Utils::is_number(s) )
    {
      T val = (T)Utils::stringToNumber<T>(s, 6);
      vecResult.push_back(val);
    }
      
  }

  return vecResult;
}


/* *************************************************************** */
// Function receives min|max as string, a pointer to datatype and optionaly a default low value if no "low|high" values were sent. Default is Zero.
// The function won't check validity of type, it is up for the programmer to do this.
// example usage: getRand<double>( "min|max", &out ); or getRand<double>( "min|max", &out, 1000 ); // 1000 = default low value if not provided
template <class N>
static bool getRand(std::string inMinMaxToSplit, N* outRandVal, N inDefaultLow = (N)0 )
{
  N low;
  N high;
  N t;

  bool result = false;

  if (inMinMaxToSplit.empty())
    return result; // fail


  std::vector<N> vecValues = splitStringToNumbers<N>(inMinMaxToSplit, "|");

  int iSize = vecValues.size();
  if (iSize > 0)
  {
    low = vecValues.at(0);
    if (iSize > 1)
      high = vecValues.at(1);
    else
      high = inDefaultLow;

    // fix if high !> low
    if (high < low)
    { // switch values
      t = high;
      low = high;
      high = t;
    }

    // produce random numbers
    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_real_distribution<> dis(low, high);
    (*outRandVal) = dis(gen); // generate rand number

    result = true;
  }


  return result;
}

/* *************************************************************** */
// This function split words into sentences, while trying to keep the line width in boundaries.

// v3.0.223.1 main function to construct sentences with length in mind.
void static sentenceTokenizerWithBoundaries(std::deque<std::string> & outDeque, std::string &inString, std::string delimiterChar, size_t width = 0, std::string in_special_LF_char = ";"); 

// convert the std::deque sentence to vector, where vector is used. TODO: maybe change all calls to this function to use the deque only function.
std::vector<std::string> static sentenceTokenizerWithBoundaries(std::string inString, std::string delimiterChar, size_t width = 0, std::string in_special_LF_char = ";"); 

// v3.0.223.1 add the new string to the sentence string. Take into consideration limiting length and too long strings.
std::string static add_word_to_line(std::deque<std::string>& outDeque, std::string inCurrentLine_s, std::string inNewWord_s, int inMaxLineLength_i, bool flag_force_new_line = false); 

/* *************************************************************** */

std::vector<std::string> static tokenizer(std::string inString, char delimateChar, size_t width);

/* *************************************************************** */
/* *************************************************************** */
/* *************************************************************** */


double static lRound(double x);

/* *************************************************************** */

double static pathRound(double x, int inBase);


/* *************************************************************** */

/// Sandy Barbour
void QuaternionToHPR( QUATERNION quaternion, HPR *phpr)
{
	double local_w = quaternion.w;
	double local_x = quaternion.x;
	double local_y = quaternion.y;
	double local_z = quaternion.z;
    	
	double sq_w = local_w * local_w;
  double sq_x = local_x * local_x;
  double sq_y = local_y * local_y;
  double sq_z = local_z * local_z;

  phpr->Heading = (float)atan2(2.0 * (local_x * local_y + local_z * local_w),(sq_x - sq_y - sq_z + sq_w)) * RadToDeg;
  phpr->Pitch = (float)asin(-2.0 * (local_x * local_z - local_y * local_w)) * RadToDeg;
  phpr->Roll = (float)atan2(2.0 * (local_y * local_z + local_x * local_w),(-sq_x - sq_y + sq_z + sq_w)) * RadToDeg;
}

/// Sandy Barbour
/// Convert location info to quanterian to locate plane in Graphical Sphare
//---------------------------------------------------------------------------
/***

psi' = pi / 360 * psi
theta' = pi / 360 * theta
phi' = pi / 360 * phi
q[0] =  cos(psi') * cos(theta') * cos(phi') + sin(psi') * sin(theta') * sin(phi')
q[1] =  cos(psi') * cos(theta') * sin(phi') - sin(psi') * sin(theta') * cos(phi')
q[2] =  cos(psi') * sin(theta') * cos(phi') + sin(psi') * cos(theta') * sin(phi')
q[3] = -cos(psi') * sin(theta') * sin(phi') + sin(psi') * cos(theta') * cos(phi')

*/
void static HPRToQuaternion( HPR hpr, QUATERNION *pquaternion)
{
	double local_Heading = hpr.Heading * DegToRad; // DegToRad is a constant
	double local_Pitch = hpr.Pitch * DegToRad;
	double local_Roll = hpr.Roll * DegToRad;

	double Cosine1 = cos(local_Roll / 2);
	double Cosine2 = cos(local_Pitch / 2);
	double Cosine3 = cos(local_Heading / 2);
	double Sine1 = sin(local_Roll / 2);
	double Sine2 = sin(local_Pitch / 2);
	double Sine3 = sin(local_Heading / 2);

	pquaternion->w = Cosine1 * Cosine2 * Cosine3 + Sine1 * Sine2 * Sine3;
	pquaternion->x = Sine1 * Cosine2 * Cosine3 - Cosine1 * Sine2 * Sine3;
	pquaternion->y = Cosine1 * Sine2 * Cosine3 + Sine1 * Cosine2 * Sine3;
	pquaternion->z = Cosine1 * Cosine2 * Sine3 - Sine1 * Sine2 * Cosine3;
}



//template <class Container>
//static bool isElementExists(Container& inMap, const typename Container::key_type &key)
//{
//     
//  //assert(inMap != nullptr);
//
//  //if (inMap != nullptr && inMap.find(key) != inMap.end())
//  if (inMap.find(key) != inMap.end())
//    return true;
//
//    return false;
//}


static float calc_heading_base_on_plane_move(double &inCurrentLat, double &inCurrentLong, double &inTargetLat, double &inTargetLong, int &inTargetHeading);




  // generate random numbers
  //// random related
static int getRandomIntNumber(int inMin, int inMax); // TODO: convert to a template ?

/* *************************************************************** */

static double getRandomRealNumber(double inMin, double inMax); // TODO: convert to a template ?

/* *************************************************************** */


  /* *************************************************************** */

static std::string convert_string_to_24_min_numbers(std::string inTimeIn24Hfortmat, int & outHour, int & outMinutes, int & outCycles); // returns error string. Format: HH24:MIN:Cycles

  /* *************************************************************** */
  /* *************************************************************** */


};


} // namespace
#endif /*UTILS_H_*/
