#ifndef POINT_H_
#define POINT_H_

#pragma once

#include <math.h>
#include "../xx_rwmeter_constants.hpp"
#include "../Utils.h"
// #include "UtilsGraph.hpp"
//#include "../../io/IXMLParser.h"
//#include "../../data/mxProperties.hpp"

using namespace rwmeter;
//using namespace mxconst;

namespace rwmeter
{
  typedef enum _object_state : uint8_t
  {
    point_undefined = 0,
    defined = 1
  } mx_point_state;

  class Point 
  {  
  private:
    double elevation_ft;
    double elevation_mt;
    double store_elev_ft;
    double store_elev_mt;   

    double elev_above_ground_ft/*, elev_above_ground_mt*/;
    double store_elev_above_ground_ft/*, store_elev_above_ground_mt*/;

    double lat, storeLat;
    double lon, storeLon;


    float   heading, storeHeading;
    float   roll, storeRoll;
    float   pitch, storePitch;

    std::string err;

    float  speed_fts; // feet in seconds
    float  speed_kmh; // km in hour

  public:
    mx_point_state pointState;
    XPLMProbeResult probe_result;

    double local_x;
    double local_y;
    double local_z;

    bool   elevWasProbed; // does elevation represent terrain info

    float   adjust_heading, storeAdjustHeading;  // v3.0.207.5

    float timeToWaitOnPoint_sec; // for moving object. wait on next point.


    Point() {
      init();
      storeData();
    }
    
    Point(double inLat, double inLong)
    {
      init();
      lat = inLat;
      lon = inLong;
      pointState = mx_point_state::defined;
      storeData();

      storeDataAsProperties(); // v3.0.213.7
    }

    Point(double inLat, double inLong, double inElev)
    {
      init();
      lat = inLat;
      lon = inLong;
      elevation_ft = inElev;
      elevation_mt = elevation_ft * rwmeter::feet2meter;
      pointState = mx_point_state::defined;
      storeData();

      storeDataAsProperties(); // v3.0.213.7
    }

    void clone(Point &inPoint)
    {
      this->lat = inPoint.lat;
      this->lon = inPoint.lon;
      this->setElevationFt(inPoint.getElevationInFeet());
      this->heading = inPoint.heading;
      this->adjust_heading = inPoint.adjust_heading;  // v3.0.207.5
      this->pitch = inPoint.pitch;
      this->roll = inPoint.roll;
      this->speed_fts = inPoint.speed_fts; // renamed in v3.0.202
      this->speed_kmh = inPoint.speed_kmh; // v3.0.202
      this->timeToWaitOnPoint_sec = inPoint.timeToWaitOnPoint_sec;
      this->setElevationAboveGroundFt(inPoint.getElevationAboveGroundInFeet()); // v3.0.202
      this->local_x = inPoint.local_x; // v3.0.203
      this->local_y = inPoint.local_y; // v3.0.203
      this->local_z = inPoint.local_z; // v3.0.203


      pointState = mx_point_state::defined;

      storeData();

      storeDataAsProperties(); // v3.0.213.7

    }
    //// Operators Overload
    void operator =(Point inPoint)
    {
      clone(inPoint);
    }


    bool operator <(const Point &p) const {
      return lat < p.lat || (lat == p.lat && lon < p.lon);
    }

    //// END Operators Overload


    void init()
    {
      lat = 0.0;
      lon = 0.0;
      setElevationFt(0.0);
      elevWasProbed = false;
      
      heading = 0.0f; // v3.0.207.2
      adjust_heading = 0.0f; // v3.0.207.5
      pitch = 0.0f; // v3.0.207.2
      roll = 0.0f; // v3.0.207.2
      setSpeedInKmh(0.0f); // v3.0.207.2 

      setElevationAboveGroundFt(0.0); // v3.0.202

      timeToWaitOnPoint_sec = 0.0f; // v3.0.207.2

      this->local_x = this->local_y = this->local_z = 0.0;
      this->storeLat = this->storeLon = 0.0;
      this->storeHeading = this->storeAdjustHeading = this->storePitch = this->storeRoll = 0.0f;

      pointState = mx_point_state::point_undefined;
    }

    /* ********************************** */

    void storeData()
    {
      storeLat = lat;
      storeLon = lon;
      store_elev_ft = elevation_ft;
      store_elev_mt = elevation_mt;

      storeHeading = heading;
      storeAdjustHeading = adjust_heading;  // v3.0.207.5
      storePitch = pitch;
      storeRoll = roll;

      store_elev_above_ground_ft = elev_above_ground_ft;  // v3.0.202
      //store_elev_above_ground_mt = elev_above_ground_mt;  // v3.0.202

    }

    /* ********************************** */
    void setLat(double inValue)
    {
      //this->setNumberProperty(mxconst::ATTRIB_LAT, inValue);
      lat = inValue;
      pointState = mx_point_state::defined;
    }

    double getLat()
    {
      //return getPropertyValue<double>(mxconst::ATTRIB_LAT, err);
    }
    /* ********************************** */
    void setLon(double inValue)
    {
      //this->setNumberProperty(mxconst::ATTRIB_LONG, inValue);
      lon = inValue;
      pointState = mx_point_state::defined;
    }

    double getLon()
    {
      //return getPropertyValue<double>(mxconst::ATTRIB_LONG, err);
    }

    /* ********************************** */
    
    void setHeading(double inValue)
    {
      //this->setNumberProperty(mxconst::ATTRIB_HEADING_PSI, inValue);
      heading = (float)inValue;
      pointState = mx_point_state::defined;
    }

    double getHeading()
    {
      //return getPropertyValue<double>(mxconst::ATTRIB_HEADING_PSI, err);
      return heading;
    }


    /* ********************************** */
    /* ********************************** */
    
    void setRoll(double inValue)
    {
      //this->setNumberProperty(mxconst::ATTRIB_ROLL, inValue);
      roll = (float)inValue;
      pointState = mx_point_state::defined;
    }

    double getRoll()
    {
      //return getPropertyValue<double>(mxconst::ATTRIB_ROLL, err);
      return roll;
    }


    /* ********************************** */
    void setPitch(double inValue)
    {
      //this->setNumberProperty(mxconst::ATTRIB_PITCH, inValue);
      pitch = (float)inValue;
      pointState = mx_point_state::defined;
    }

    double getPitch()
    {
      //return getPropertyValue<double>(mxconst::ATTRIB_PITCH, err);
      return this->pitch;
    }


    /* ********************************** */
    void setAdjustHeading(double inValue)
    {
      //this->setNumberProperty(mxconst::ATTRIB_ADJUST_HEADING, inValue);
      adjust_heading = (float)inValue;
      pointState = mx_point_state::defined;
    }

    double getAdjustHeading()
    {
      //return getPropertyValue<double>(mxconst::ATTRIB_ADJUST_HEADING, err);
      return this->adjust_heading;
    }


    /* ********************************** */
    /* ********************************** */
    /* ********************************** */

    void storeDataAsProperties() // v3.0.213.7
    {
      //this->setNumberProperty(mxconst::ATTRIB_LAT, lat);
      //this->setNumberProperty(mxconst::ATTRIB_LONG, lon);
      //this->setNumberProperty(mxconst::ATTRIB_ELEV_FT, elevation_ft);
      //this->setNumberProperty(mxconst::ATTRIB_HEADING_PSI, heading);
      //this->setNumberProperty(mxconst::ATTRIB_ADJUST_HEADING, adjust_heading);
      //this->setNumberProperty(mxconst::ATTRIB_PITCH, pitch);
      //this->setNumberProperty(mxconst::ATTRIB_ROLL, roll);
      //this->setNumberProperty(mxconst::ATTRIB_ELEV_ABOVE_GROUND_FT, elev_above_ground_ft);

      //this->setNumberProperty(mxconst::ATTRIB_SPEED_KMH, speed_kmh);
      //this->setNumberProperty(mxconst::ATTRIB_WAIT_SEC, timeToWaitOnPoint_sec);

    }

    /* ********************************** */

    //void saveCheckpoint(IXMLNode & inParent)
    //{
    //  //IXMLNode point = inParent.addChild(ELEMENT_POINT.c_str());

    //  //point.addAttribute(ATTRIB_LAT.c_str(), mxUtils::formatNumber<double>(lat, 8).c_str());
    //  //point.addAttribute(ATTRIB_LONG.c_str(), mxUtils::formatNumber<double>(lon, 8).c_str());
    //  //point.addAttribute(ATTRIB_ELEV_FT.c_str(), mxUtils::formatNumber<double>(this->getElevationInFeet(), 3).c_str());

    //  //point.addAttribute(ATTRIB_HEADING_PSI.c_str(), mxUtils::formatNumber<float>(heading, 2).c_str());
    //  //point.addAttribute(ATTRIB_ADJUST_HEADING.c_str(), mxUtils::formatNumber<float>(adjust_heading, 2).c_str());
    //  //point.addAttribute(ATTRIB_PITCH.c_str(), mxUtils::formatNumber<float>(pitch, 2).c_str());
    //  //point.addAttribute(ATTRIB_ROLL.c_str(), mxUtils::formatNumber<float>(roll, 2).c_str());
    //  //point.addAttribute(ATTRIB_ELEV_ABOVE_GROUND_FT.c_str(), mxUtils::formatNumber<double>(elev_above_ground_ft, 2).c_str());
    //  //point.addAttribute(ATTRIB_SPEED_KMH.c_str(), mxUtils::formatNumber<float>(speed_kmh, 2).c_str());
    //  //point.addAttribute(ATTRIB_WAIT_SEC.c_str(), mxUtils::formatNumber<float>(timeToWaitOnPoint_sec, 2).c_str());


    //  //point.addAttribute(PROP_POINT_DATA.c_str(), this->format_point_to_savepoint().c_str()); // v3.0.192 for obj3d

    //}
    /* ********************************** */
    void resetData()
    {
      lat = storeLat;
      lon = storeLon;
      setElevationFt(store_elev_ft); // will set feet and meter
      setElevationAboveGroundFt(store_elev_above_ground_ft);  // v3.0.202

      heading = storeHeading;
      adjust_heading = storeAdjustHeading; // v3.0.207.5
      pitch = storePitch;
      roll = storeRoll;

      this->calcSimLocalData();

      storeDataAsProperties();

    }

    /* ********************************** */

    void setElevationFt(double inValueInFeet)
    {
      this->elevation_ft = inValueInFeet;
      //this->setNumberProperty(mxconst::ATTRIB_ELEV_FT, inValueInFeet);

      elevation_mt = elevation_ft * rwmeter::feet2meter;
      pointState = mx_point_state::defined;
    }

    /* ********************************** */

    void setElevationMt(double inValueInMeter)
    {
      this->elevation_mt = inValueInMeter;
      elevation_ft = elevation_mt * rwmeter::meter2feet;
      //this->setNumberProperty(mxconst::ATTRIB_ELEV_FT, elevation_ft);
      pointState = mx_point_state::defined;
    }

    /* ********************************** */

    void setElevationAboveGroundFt(double inValueInFeet)
    {
      this->elev_above_ground_ft = inValueInFeet;
      //this->elev_above_ground_mt = elev_above_ground_ft * rwmeter::feet2meter;

      //this->setNumberProperty(mxconst::ATTRIB_ELEV_ABOVE_GROUND_FT, inValueInFeet); // v3.0.213.7

      pointState = mx_point_state::defined;
    }

    /* ********************************** */

    void setElevationAboveGroundMt(double inValueInMeter)
    {
      //this->elev_above_ground_mt = inValueInMeter;
      elev_above_ground_ft = inValueInMeter * rwmeter::meter2feet;

      //this->setNumberProperty(mxconst::ATTRIB_ELEV_ABOVE_GROUND_FT, elev_above_ground_ft); // v3.0.213.7

      pointState = mx_point_state::defined;
    }

    /* ********************************** */

    void setSpeedInFts(double inSpeed) // v3.0.202
    {
      this->speed_fts = (float)inSpeed;
      this->speed_kmh = (float)(inSpeed * rwmeter::fts2kmh);
    }

    /* ********************************** */

    void setSpeedInKmh(double inSpeed) // v3.0.202
    {
      this->speed_kmh = (float)inSpeed;
      this->speed_fts = (float)(inSpeed * rwmeter::kmh2fts);
    }

    /* ********************************** */

    float getSpeedKmh() { return (this->speed_kmh >= 0.0f)? this->speed_kmh : 0.0f; }

    /* ********************************** */

    float getSpeedFts() { return (this->speed_fts >= 0.0f)? this->speed_fts : 0.0f; }

    /* ********************************** */
    /* ********************************** */

    double getElevationInMeter()
    {
      return elevation_mt;
      //return (getPropertyValue<double>(mxconst::ATTRIB_ELEV_FT, err) *  rwmeter::feet2meter);
    }

    /* ********************************** */

    double getElevationInFeet()
    {
      return elevation_ft;
      //return  getPropertyValue<double>(mxconst::ATTRIB_ELEV_FT, err);
    }

    /* ********************************** */

    double getElevationAboveGroundInMeter()
    {
      //elev_above_ground_ft = getPropertyValue<double>(mxconst::ATTRIB_ELEV_ABOVE_GROUND_FT, err);
      return elev_above_ground_ft * rwmeter::feet2meter; // return elevation in meters
      //return this->elev_above_ground_mt;
    }

    /* ********************************** */

    double getElevationAboveGroundInFeet()
    {
      // v3.0.213.7
      //return getPropertyValue<double>(mxconst::ATTRIB_ELEV_ABOVE_GROUND_FT, err);

      return this->elev_above_ground_ft;
    }

    /* ********************************** */

    /* ********************************** */

    double distanceTo(Point q)
    {
      return std::hypot(this->lat - q.lat, this->lon - q.lon);
        //return Math.hypot(this.x - q.x, this.y - q.y);
    }

    std::string to_string()
    {
      std::string format; format.clear();

      format = "lat: " + mxUtils::formatNumber<double>(lat, 8) + ", lon: " + mxUtils::formatNumber<double>(lon, 8) + ", elev_ft: " + mxUtils::formatNumber<double>(getElevationInFeet()) + " [Defined: " + ((pointState == mx_point_state::defined) ? rwmeter::MX_YES : rwmeter::MX_NO) + "]";

      return format;
    }
    /* ********************************** */

    static int slope(int x1, int y1, int x2, int y2)
    {
      if (x2 - x1 == 0)
      {
        //cout << "Slope is 90 degrees" << endl;
        return 90;
      }

      double tanx, s;
      tanx = (y2 - y1) / (x2 - x1);
      s = atan(tanx);
      s = (180 / rwmeter::PI)*s;
      return (int)s;
    }

    /* ********************************** */

    int slope(Point & inP)
    {
      if (inP.lat - this->lat == 0)
      {
        //cout << "Slope is 90 degrees" << endl;
        return 90;
      }

      double tanx, s;
      tanx = (inP.lon - this->lon) / (inP.lat - this->lat);
      s = atan(tanx);
      s = (180 / rwmeter::PI)*s;
      return (int)s;
    }
    /* ********************************** */

    static float slope(Point & p1, Point & p2)
    {
      if (p2.lat - p1.lat == 0)
      {
        //cout << "Slope is 90 degrees" << endl;
        return 90;
      }


      double tanx, s;
      tanx = (p2.lon -p1.lon) / (p2.lat - p1.lat);
      s = atan(tanx);
      s = (180 / rwmeter::PI)*s;
      return (float)s;
    }

    /* ********************************** */

    double calcDistanceBetween2Points(Point & pTarget, std::string *err = nullptr)
    {
      if (pTarget.pointState == rwmeter::mx_point_state::point_undefined)
      {
        if (err != nullptr)
          (*err) = "Target point state is undefined. Please fix";

        return -1; // not valid
      }
        

      return Utils::calcDistanceBetween2Points_nm(this->lat, this->lon, pTarget.lat, pTarget.lon);
    }

    /* ********************************** */

    static double calcDistanceBetween2Points(Point & pOrigin, Point & pTarget, std::string *err = nullptr)
    {
      if (pTarget.pointState == rwmeter::mx_point_state::point_undefined || pOrigin.pointState == rwmeter::mx_point_state::point_undefined)
      {
        if (err != nullptr)
          (*err) = "Target or Origin point state is undefined. Please fix";

        return -1; // not valid
      }

      return pOrigin.calcDistanceBetween2Points(pTarget);      
    }

    /* ********************************** */

    // calculate target point relative to Origin point, given bearing(heading) and distance in nautical Miles
    void static calcPointBasedOnDistanceAndBearing_2DPlane(Point &pOrigin, Point &pTarget, float inHdg, double inDistance, rwmeter::mx_units_of_measure inUnits = rwmeter::mx_units_of_measure::nm)
    {
      float distanceInNm = (float)inDistance;

      if (inUnits != nm)
      {
        distanceInNm = Utils::convertToNm(distanceInNm, inUnits);
      }


      Utils::calcPointBasedOnDistanceAndBearing_2DPlane(pTarget.lat, pTarget.lon, pOrigin.lat, pOrigin.lon, inHdg, distanceInNm);

      // v3.0.219.11 added set properties since lat/long variables are not being used in Point, only property values
      pTarget.setLat(pTarget.lat);
      pTarget.setLon(pTarget.lon);
    }


    /* ********************************** */   

    /* ********************************** */   

    /* ********************************** */
    // read only 1 child point element of inParentPointNode parent element
    /* ********************************** */

    void calcSimLocalData()
    {
      // calculate locals from lat long elev. Will need to re-calculate every scenery change
      //XPLMWorldToLocal(lat, lon, elevation_mt, &local_x, &local_y, &local_z);
      XPLMWorldToLocal(getLat(), getLon(), getElevationInMeter(), &local_x, &local_y, &local_z);
    }

    /* *************************************************************** */

    static double getTerrainElevInMeter_FromPoint(rwmeter::Point& p1, XPLMProbeResult& outResult)
    {
      double local_x = 0.0;
      double local_y = 0.0;
      double local_z = 0.0;
      double lat, lon, elev_mt;
      lat = lon = elev_mt = 0.0;

      XPLMProbeInfo_t info;

      outResult = -1;

      lat = p1.getLat();
      lon = p1.getLon();
      elev_mt = p1.getElevationInMeter();

      XPLMWorldToLocal(lat, lon, 0, &local_x, &local_y, &local_z);

      info = Point::terrain_Y_Probe(local_x, local_y, local_z, outResult);
      if (outResult == 0)
      {
        XPLMLocalToWorld(info.locationX, info.locationY, info.locationZ, &lat, &lon, &elev_mt);
        //p1.setBoolProperty(mxconst::PROP_IS_WET, info.is_wet);
        p1.setElevationMt(elev_mt);
      }
      else
        elev_mt = 0.0;

      return elev_mt;
    }

    /* *************************************************************** */

    static int probeIsWet(rwmeter::Point& p1, XPLMProbeResult& outResult)
    {
//      double lat, lon, elev_mt;
//      lat = lon = elev_mt = 0.0;

      XPLMProbeInfo_t info;

      outResult = -1;
      p1.calcSimLocalData();

      info = Point::terrain_Y_Probe(p1.local_x, p1.local_y, p1.local_z, outResult);
      if (outResult == 0)
      {        
        //p1.setBoolProperty(mxconst::PROP_IS_WET, info.is_wet);
        return info.is_wet;
      }


      return 0;
    }

    /* *************************************************************** */
    void set_isWet(bool inVal)
    {
      //this->setBoolProperty(mxconst::PROP_IS_WET, inVal);
    }

    /* *************************************************************** */

    static XPLMProbeInfo_t terrain_Y_Probe(double & inXgl, double & inYgl, double & inZgl, XPLMProbeResult & outResult)
    {

      //XPLMProbeResult probeResult;
      XPLMProbeRef    probe = NULL;
      // Create a Y probe
      probe = XPLMCreateProbe(xplm_ProbeY);

      static XPLMProbeInfo_t info;
      // initialized info struct.
      info.is_wet = 0;
      info.locationX = 0.0f;
      info.locationY = 0.0f;
      info.locationZ = 0.0f;
      info.normalX = 0.0f;
      info.normalY = 0.0f;
      info.normalZ = 0.0f;
      info.velocityX = 0.0f;
      info.velocityY = 0.0f;
      info.velocityZ = 0.0f;
      info.structSize = sizeof(info);

      outResult = XPLMProbeTerrainXYZ(probe, (float)inXgl, (float)inYgl, (float)inZgl, &info);


      XPLMDestroyProbe(probe);

      return info;
    }

    /* ********************************** */
    /* ********************************** */
    /* ********************************** */

    void clearPoint()
    {
      this->lat = 0.0;
      this->lon = 0.0;
      this->local_x = 0.0;
      this->local_y = 0.0;
      this->local_z = 0.0;

      this->elevWasProbed = false;

      this->pitch = 0.0f;
      this->heading = 0.0f;
      this->roll = 0.0f;

      this->speed_fts = 0.0f;
      this->speed_kmh = 0.0f;
      this->timeToWaitOnPoint_sec = 0.0f;

      this->setElevationFt(0.0);
      this->setElevationAboveGroundFt(0.0);  // v3.0.202

      pointState = mx_point_state::point_undefined;

    }

    /* ********************************** */
    /* ********************************** */
    /* ********************************** */

    std::string to_string_xy()
    {
      std::string format; format.clear();

      format = "lat: " + mxUtils::formatNumber<double>(lat, 8) + ", lon: " + mxUtils::formatNumber<double>(lon, 8);

      return format;
    }

    
    std::string format_point_to_savepoint()
    {
      return mxUtils::formatNumber<double>(lat, 8) + "/" + mxUtils::formatNumber<double>(lon, 8) + "/" + mxUtils::formatNumber<double>(elevation_ft, 3) + "|"  
           + mxUtils::formatNumber<double>(heading, 2) + "/" + mxUtils::formatNumber<double>(pitch, 2) + "/" + mxUtils::formatNumber<double>(roll, 2) + "|" 
           + mxUtils::formatNumber<double>(speed_fts, 2) + "/" + mxUtils::formatNumber<double>(timeToWaitOnPoint_sec, 2);
    }


  };





}

#endif


