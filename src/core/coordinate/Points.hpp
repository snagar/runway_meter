#ifndef POINTS_H_
#define POINTS_H_

#pragma once

#include <deque>

#include "Point.hpp"
#include <unordered_set>
#include <math.h>
#include <algorithm>
#include "../../data/mxProperties.hpp"

using namespace std;
using namespace missionx;
using namespace mxconst;

namespace missionx
{

  class Points : public mxProperties
  {
  public:
    Points() noexcept {
      init();
    }

    // core attributes
    Point pCenter;
    mx_point_state pointsState;
    //std::vector<missionx::Point> deqPoints;
    std::deque<missionx::Point> deqPoints;

    /* ***************************************** */
    // members


    //void clone(std::vector <missionx::Point> &inVecPoints)
    //{
    //  init();
    //  this->deqPoints = inVecPoints;
    //  //Points::sortPointsGrahmScan(*this);

    //  pointsState = mx_point_state::defined;
    //}

    void clone(std::deque <missionx::Point> &inVecPoints)
    {
      init();
      this->deqPoints = inVecPoints;
      //Points::sortPointsGrahmScan(*this);

      pointsState = mx_point_state::defined;
    }

    /* ***************************************** */

    void addPoint(missionx::Point p)
    {
      this->deqPoints.push_back(p);
      pointsState = mx_point_state::defined;
    }

    /* ***************************************** */

    void clear()
    {
      init();
    }


    /* ***************************************** */
    void init()
    {
      pointsState = mx_point_state::point_undefined;
      deqPoints.clear();
    }

    /* ***************************************** */


    ////////////////
    // Convex Haul//
    ///////////////
    // https://en.wikibooks.org/wiki/Algorithm_Implementation/Geometry/Convex_hull/Monotone_chain#C.2B.2B

    // 2D cross product of OA and OB vectors, i.e. z-component of their 3D cross product.
    // Returns a positive value, if OAB makes a counter-clockwise turn,
    // negative for clockwise turn, and zero if the points are collinear.
    // cross was renamed to ccw
    static double ccw(missionx::Point &O, missionx::Point &A, missionx::Point &B)
    {
      //return (A.lat - O.lat) * (B.lon - O.lon) - (A.lon - O.lon) * (B.lat - O.lat);
      return (A.getLat() - O.getLat()) * (B.getLon() - O.getLon()) - (A.getLon() - O.getLon()) * (B.getLat() - O.getLat());
    }

    /* ***************************************** */

    // Returns a list of points on the convex hull in counter-clockwise order.
    // Note: the last point in the returned list is the same as the first one.
    //static std::vector<missionx::Point> convex_hull(std::vector<missionx::Point> P)
    static void convex_hull(std::vector<missionx::Point> &P)
    {
      int n = (int)P.size(), k = 0;
      if (n == 1) return ;

      std::vector<missionx::Point> H(2 * n);

      // Sort points lexicographically
      std::sort(P.begin(), P.end());

      // Build lower hull
      for (int i = 0; i < n; ++i) {
        while (k >= 2 && Points::ccw(H[k - 2], H[k - 1], P[i]) <= 0.0) k--;
        H[k++] = P[i];
      }

      // Build upper hull
      for (int i = n - 2, t = k + 1; i >= 0; i--) {
       // while (k >= t && Points::cross(H[k - 2], H[k - 1], P[i]) <= 0) k--;
        while (k >= t && Points::ccw(H[k - 2], H[k - 1], P[i]) <= 0.0) k--;
        H[k++] = P[i];
      }

      H.resize(k - (int)1);

      P = H;
      //return H;
    }


    /* ********************************** */

    bool isPointInPolyArea(Point &point)
    {

      /* The coordinates of the plane coordinations */
      float px, py;
      Point p1;
      Point p2;

      px = (float)point.getLat();
      py = (float)point.getLon();

      // Calculate How many times the ray crosses the area segments
      int crossings = 0;

      float x1, x2;

      size_t counter = 0;
      for (auto itPoint : deqPoints)
      {
        counter++;
        p1 = (itPoint);
        if (counter < deqPoints.size())
          p2 = deqPoints[counter];
        else
          p2 = deqPoints[0];

        // This is done to ensure that we get the same result when
        // the line goes from left to right and right to left
        if (p1.getLat() < p2.getLat())
        {
          x1 = (float)p1.getLat();
          x2 = (float)p2.getLat();
        }
        else
        {
          x1 = (float)p2.getLat();
          x2 = (float)p1.getLat();
        }

        // First check if the ray is possible to cross the line
        if (px > x1 && px <= x2 && (py < (float)p1.getLon() || py <= (float)p2.getLon()))
        {
          static const float eps = 0.000001f;


          // Calculate the equation of the line
          float dx = (float)p2.getLat() - (float)p1.getLat();
          float dy = (float)p2.getLon() - (float)p1.getLon();
          float k;

          if (fabs(dx) < eps)
          {
#ifdef IBM
            k = numeric_limits<float>::infinity(); // max();  //INFINITY; // math.h
#else
            k = numeric_limits<float>::max();
#endif
          }
          else
          {
            k = dy / dx;
          }

          float m = (float)p1.getLon() - k * (float)p1.getLat();

          // Find if the ray crosses the line
          float y2 = k * px + m;
          if (py <= y2)
          {
            crossings++;
          }
        }

      } // end for loop

        // Evaluate if plane in Area
      return (crossings % 2 == 1);
    }


    /* ***************************************** */

    // Does line segment intersect ray?
    bool contains(double x0, double y0)
    {
      Point p(x0, y0);

      return contains(p);

    }

    /* ***************************************** */

    // Does line segment intersect ray?
    bool contains(Point p)
    {
      int crossings = 0;

      size_t vSize = deqPoints.size();

      for (size_t i = 0; i + 1 < vSize; i++)
      {
        double x1, x2;
        x1 = x2 = 0.0;

        auto p1 = deqPoints[i];
        auto p2 = deqPoints[i + 1];

        // this switch between latitudes, fixes the convex hull based vector.
        // This is done to ensure that we get the same result when
        // the line goes from left to right and right to left (clockwise or counter clockwise)
        if (p1.getLat() < p2.getLat())
        {
          x1 = (float)p1.getLat();
          x2 = (float)p2.getLat();
        }
        else
        {
          x1 = (float)p2.getLat();
          x2 = (float)p1.getLat();
        }

        double slope = (p2.getLon() - p1.getLon()) / (x2 - x1);
        bool cond1 = (x1 <= p.getLat()) && (p.getLat() < x2);
        bool cond2 = (x2 <= p.getLat()) && (p.getLat() < x1);
        bool above = (p.getLon() < slope * (p.getLat() - x1) + p1.getLon());

        if ((cond1 || cond2) && above)
          crossings++;

      }

      return (crossings % 2 != 0);

    }

    /* ***************************************** */

    // checkpoint
    void saveCheckpoint(IXMLNode & inParent)
    {
      IXMLNode points = inParent.addChild(PROP_POINTS.c_str());

      for (auto p : deqPoints)
        p.saveCheckpoint(points);

    }

    /* ***************************************** */
    // load <points> sub elements and construct the radius/polygonal point/s
    bool loadCheckpoint(ITCXMLNode &inParent, std::string &outError)
    {
      bool loadSuccess = true;
      ITCXMLNode xPoints = inParent.getChildNode(PROP_POINTS.c_str());
      if (!xPoints.isEmpty())
      { // TODO: need to read the whole aspect of point including tilt and such. See if we can use the readPoint() function from "load_mission_file"
        int nChilds = xPoints.nChildNode(ELEMENT_POINT.c_str());
        for (int i = 0; i < nChilds && loadSuccess; i++)
        {
          ITCXMLNode xChild = xPoints.getChildNode(i);
          if (xChild.isEmpty())
          {
            loadSuccess = false;
            outError += "Found wrong <point> information. 'lat' or 'long' attributes might not have been set correctly. Please fix!!!";
          }
          else
          {
            //std::string lat = xChild.getAttribute(ATTRIB_LAT.c_str());
            //std::string lon = xChild.getAttribute(ATTRIB_LONG.c_str());
            //std::string elev = xChild.getAttribute(ATTRIB_ELEV_FT.c_str());
            //if (lat.empty() || lon.empty() || !Utils::is_number(lat) || !Utils::is_number(lon))
            //{
            //  loadSuccess = false;
            //  outError += "Found wrong <point> information. 'lat' or 'long' attributes might not have been set correctly. Please fix!!!";
            //}
            //if (elev.empty() || !Utils::is_number(elev))
            //  elev = MINUS_ONE;
            //Point p((double)Utils::stringToNumber<double>(lat), (double)Utils::stringToNumber<double>(lon), (double)Utils::stringToNumber<double>(elev));
            //this->deqPoints.push_back(p);

            Point p;
            p.loadPointElement(xChild, outError);
            if (p.getLat() != 0.0 && p.getLon() != 0.0)
              this->deqPoints.push_back(p);
          }


        } // end loop

        if (!loadSuccess)
          this->deqPoints.clear();

      }

      return loadSuccess;
    }
    /* ***************************************** */
    // readChildPointElements allow you to read multiple point child elements under parent element.
    static void readChildPointElements(ITCXMLNode & inParentPointNode, std::deque<missionx::Point> & outDeqPoints, int inLimit, std::string & outErr)
    {
      int nChilds = inParentPointNode.nChildNode(mxconst::ELEMENT_POINT.c_str());
      inLimit = (inLimit <= 0) ? nChilds : inLimit; // if limit is 0 then we fetch all points

      for (int i1 = 0; i1 < nChilds && i1 < inLimit; ++i1)
      {
        Point p;
        ITCXMLNode xPoint = inParentPointNode.getChildNode(mxconst::ELEMENT_POINT.c_str(), i1);
        if (!xPoint.isEmpty())
        {
          p.loadPointElement(xPoint, outErr);
          if (outErr.empty())
            outDeqPoints.push_back(p);
        }
          
      }
    }
    /* ***************************************** */

    bool isPointInRadiusArea(Point & trigPoint, float inRadius_nm, Point &objectPoint)
    {
      double distance = 0.0f;

      distance = Utils::calcDistanceBetween2Points_nm(objectPoint.getLat(), objectPoint.getLon(), trigPoint.getLat(), trigPoint.getLon());

      return ((inRadius_nm >= (float)distance) ? true : false);
    }


    /* ***************************************** */
    /* ***************************************** */
    //static Point calculateCenterOfShape(std::vector<missionx::Point> & vecPoint)
    static Point calculateCenterOfShape(std::deque<missionx::Point> & vecPoint)
    {
      Point p;

      if (!vecPoint.empty())
      {

        double sumLat = 0.0;
        double sumLon = 0.0;
        unsigned int counter = 0;
        //      for ( itPoint = vecPoint.begin(); itPoint < vecPoint.end(); itPoint++ )
        for (auto itPoint : vecPoint)
        {
          sumLat += itPoint.getLat();
          sumLon += itPoint.getLon();
          counter++;
        } // end loop

        p.setLat( (sumLat / counter) );
        p.setLon( (sumLon / counter) );  
        p.pointState = missionx::mx_point_state::defined;


#ifdef DEBUG_AREA
        Log::logMsg(std::string("[Area calc center] lat:") + mxUtils::formatNumber<double>(p.getLat()) + ", lon: " + mxUtils::formatNumber<double>(p.getLon())); // debug        
#endif


      } // end if Point is empty

      return p;
    } 
    // end calculateCenterOfShape


    /* ***************************************** */
    static bool collinear(Point a, Point b, Point c) // on same plane
    {
      return ccw(a, b, c) == 0;
    }

    /* ***************************************** */
    std::string to_string()
    {
      int counter = 1;
      std::string format; format.clear();
      format = "\nPoints defined: " + ((pointsState == mx_point_state::defined) ? mxconst::MX_YES : mxconst::MX_NO) + ", List of stored points:" + UNIX_EOL;
      for (auto p : deqPoints)
      {
        format += "point " + Utils::formatNumber<int>(counter) + ": " + p.to_string_xy() + UNIX_EOL;
        counter++;
      }

      format += "\n[Center] " + pCenter.to_string() + UNIX_EOL;

      return format;
    }

  };

}

#endif


