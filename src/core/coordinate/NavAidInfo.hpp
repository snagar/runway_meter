/*
 * RandomEngine.h
 *
 *  Created on: Jan 29, 2019
 *      Author: snagar
 *  Extracted from RandomEngine class
 */
#ifndef  NAVAIDINFO_H_
#define  NAVAIDINFO_H_


#include "../xx_rwmeter_constants.hpp"
#include "../MxUtils.h"
#include "Point.hpp"

namespace rwmeter
{
  class NavAidInfo : public missionx::mxProperties
  {
  private:
    //static IXMLNode xMainNode;
  public:
    float degRelativeToSearchPoint; // degrees // bearingRelativeToSearchPoint;

    XPLMNavRef navRef; // XPSDK type
    XPLMNavType navType; // XPSDK type
    float lat, lon, height_mt, heading;
    int freq;
    char ID[64];
    char name[256];
    char inRegion[1];

    missionx::Point p; // will hold lat/long/elev/heading_psi
    IXMLNode dummyTop, xmlPointNode;
    std::string loc_desc, template_type, err;
    std::string flightLegName;
    //std::string foundAptIcao; // used in getRandomAirport instead of mxProperties. We will use mxProperties once we understand gcc build failure
    std::string radius_mt_suggested_s; // used in RandomEngine when we pick data from apt.dat or not. If from apt.dat then we should use ~40mt if not then empty and it wil default to 500mt
    bool flag_picked_random_lat_long; // v3.0.219.14 used when we want to flag special case of random coordinate. Can help with deciding if template is medevac or not and if need heli or not.
    IXMLNode xLegNode_from_template; // v3.0.221.2 holds template goal node.
    bool flag_force_picked_same_point_template_as_flight_leg_tempalte_type; // v3.0.221.15rc4 

    //double slope;

    ~NavAidInfo(){}

    NavAidInfo()
    {
      init();
    }

    void init()
    {
      degRelativeToSearchPoint = -1.0f; // not set

      navRef = XPLM_NAV_NOT_FOUND;
      lat = lon = height_mt = heading = 0.0f;
      freq = 0; // not set
      ID[0] = '\0';
      name[0] = '\0';
      inRegion[0] = '\0';

      dummyTop = IXMLNode::createXMLTopNode("dummy");
      xmlPointNode = dummyTop.addChild(mxconst::ELEMENT_POINT.c_str()).deepCopy();
      //dummyTop.deleteNodeContent(); // TODO implement later
      

      this->loc_desc.clear(); // = "lat/lon";
      this->template_type.clear();
      flightLegName.clear();
      
      radius_mt_suggested_s.clear();
      flag_picked_random_lat_long = false; // v3.0.219.14
      xLegNode_from_template = IXMLNode().emptyIXMLNode; // v3.0.221.2
      flag_force_picked_same_point_template_as_flight_leg_tempalte_type = false; // v3.0.221.15rc4 
      
    }

    std::string getLat()
    {
      return Utils::formatNumber<float>(this->lat, 8);
    }

    std::string getLon()
    {
      return Utils::formatNumber<float>(this->lon, 8);
    }

    std::string get_latLon()
    {
      return getLat() + "," + getLon();
    }


    std::string getID()
    {
      return std::string(this->ID);
    }

    std::string getNavAidName()
    {
      return std::string(this->name);
    }


    std::string get_locDesc()
    {
      if (std::string(ID).empty() && std::string(name).empty())
        this->loc_desc = "coordinates: [" + Utils::formatNumber<float>(this->lat,6) + "," + Utils::formatNumber<float>(this->lon,6) + "]";
      else if (std::string(ID).empty())
        this->loc_desc = std::string(name);
      else
        this->loc_desc = std::string(name) + "(" + std::string(ID) + ")";

      if (this->height_mt != 0.0f)
      {
        float height_ft = height_mt * missionx::meter2feet;
        loc_desc += " (elevation: ~" + Utils::formatNumber<float>(height_ft, 0) + "ft)";
      }

      return loc_desc;
    }


    void setID(std::string inVal)
    {

#ifdef IBM
      strncpy_s(this->ID, 63, inVal.c_str(), 63);
#else
      std::strncpy(this->ID, inVal.c_str(), 63);
#endif
    }

    void setName(std::string inVal)
    {
#ifdef IBM
      strncpy_s(this->name, 250, inVal.c_str(), 250);
#else
      std::strncpy(this->name, inVal.c_str(), 250);
#endif
    }

    std::string getNavAsAptRampCode_1300()
    {
      return mxconst::APT_1300_RAMP_CODE_v11 + mxUtils::formatNumber<double>(this->lat, 8) + mxconst::SPACE + mxUtils::formatNumber<double>(this->lon, 8);
    }

    void synchToPoint()
    {
      if (this->loc_desc.empty()) // v3.0.221.10
        get_locDesc(); // this init the loc_desc attribute

      p = Point(lat, lon);
      p.setElevationMt(height_mt);
      p.setHeading(heading);
      p.setName(flightLegName);
      p.setStringProperty(ATTRIB_ID, ID);
      p.setStringProperty(mxconst::ATTRIB_RADIUS_MT, radius_mt_suggested_s);  // v3.0.219.12+
      p.setBoolProperty(mxconst::ATTRIB_IS_RANDOM_COORDINATES, flag_picked_random_lat_long); // v3.0.219.14
      p.setStringProperty(mxconst::ELEMENT_ICAO, std::string(this->ID));
      p.setStringProperty(mxconst::ATTRIB_LOC_DESC, this->loc_desc); // v3.0.221.10

      if (!xmlPointNode.isEmpty())
      {
        xmlPointNode.updateAttribute(mxUtils::formatNumber<double>(lat, 8).c_str(), mxconst::ATTRIB_LAT.c_str(), mxconst::ATTRIB_LAT.c_str());
        xmlPointNode.updateAttribute(mxUtils::formatNumber<double>(lon, 8).c_str(), mxconst::ATTRIB_LONG.c_str(), mxconst::ATTRIB_LONG.c_str());
        xmlPointNode.updateAttribute(mxUtils::formatNumber<double>(p.getElevationInFeet()).c_str(), mxconst::ATTRIB_ELEV_FT.c_str(), mxconst::ATTRIB_ELEV_FT.c_str());

        xmlPointNode.updateAttribute(this->ID, mxconst::ELEMENT_ICAO.c_str(), mxconst::ELEMENT_ICAO.c_str()); // v3.0.221.7 add the icao to the point. We will add this to the GPS too before the point lat/lon


        xmlPointNode.updateAttribute(loc_desc.c_str(), mxconst::ATTRIB_LOC_DESC.c_str(), mxconst::ATTRIB_LOC_DESC.c_str());
        xmlPointNode.updateAttribute(this->template_type.c_str(), mxconst::ATTRIB_TEMPLATE.c_str(), mxconst::ATTRIB_TEMPLATE.c_str());
        xmlPointNode.updateAttribute(this->flightLegName.c_str(), mxconst::ATTRIB_NAME.c_str(), mxconst::ATTRIB_NAME.c_str()); // v3.0.219.11 goal name

        xmlPointNode.updateAttribute(this->radius_mt_suggested_s.c_str(), mxconst::ATTRIB_RADIUS_MT.c_str(), mxconst::ATTRIB_RADIUS_MT.c_str()); // v3.0.219.12+ add suggested radius_mt 
        xmlPointNode.updateAttribute(mxUtils::formatNumber<bool>(this->flag_picked_random_lat_long).c_str(), mxconst::ATTRIB_IS_RANDOM_COORDINATES.c_str(), mxconst::ATTRIB_IS_RANDOM_COORDINATES.c_str()); // v3.0.219.14
      }
    }

  void syncPointToNav()
  {
    NavAidInfo();
    lat = (float)p.getLat();
    lon = (float)p.getLon();
    heading = (float)p.getHeading();
    height_mt = (float)p.getElevationInMeter();
    template_type = p.getPropertyValue_asString(mxconst::ATTRIB_TEMPLATE, err);
    flightLegName = p.getName();
    radius_mt_suggested_s = p.getPropertyValue_asString(mxconst::ATTRIB_RADIUS_MT, err);
    flag_picked_random_lat_long = p.getPropertyValue<bool>(mxconst::ATTRIB_IS_RANDOM_COORDINATES, err); // v3.0.219.14
    this->setID(Utils::readAttrib(xmlPointNode, mxconst::ELEMENT_ICAO, EMPTY_STRING)); // // v3.0.221.7 store icao
    if (p.hasProperty(mxconst::ATTRIB_LOC_DESC))
    {
      std::string p_loc_desc = p.getPropertyValue_asString(mxconst::ATTRIB_LOC_DESC, err);
      if (err.empty() && !p_loc_desc.empty())
        this->loc_desc = p_loc_desc;
      
    }
    
    if (!xmlPointNode.isEmpty())
    {
      xmlPointNode.updateAttribute(mxUtils::formatNumber<double>(lat, 8).c_str(), mxconst::ATTRIB_LAT.c_str(), mxconst::ATTRIB_LAT.c_str());
      xmlPointNode.updateAttribute(mxUtils::formatNumber<double>(lon, 8).c_str(), mxconst::ATTRIB_LONG.c_str(), mxconst::ATTRIB_LONG.c_str());
      xmlPointNode.updateAttribute(mxUtils::formatNumber<double>(p.getElevationInFeet()).c_str(), mxconst::ATTRIB_ELEV_FT.c_str(), mxconst::ATTRIB_ELEV_FT.c_str());
             
      xmlPointNode.updateAttribute(loc_desc.c_str(), mxconst::ATTRIB_LOC_DESC.c_str(), mxconst::ATTRIB_LOC_DESC.c_str());
      xmlPointNode.updateAttribute(this->template_type.c_str(), mxconst::ATTRIB_TEMPLATE.c_str(), mxconst::ATTRIB_TEMPLATE.c_str());
      xmlPointNode.updateAttribute(this->flightLegName.c_str(), mxconst::ATTRIB_NAME.c_str(), mxconst::ATTRIB_NAME.c_str()); // v3.0.219.11 goal name

      xmlPointNode.updateAttribute(this->radius_mt_suggested_s.c_str(), mxconst::ATTRIB_RADIUS_MT.c_str(), mxconst::ATTRIB_RADIUS_MT.c_str()); // v3.0.219.12+ add suggested radius_mt 
      xmlPointNode.updateAttribute(mxUtils::formatNumber<bool>(this->flag_picked_random_lat_long).c_str(), mxconst::ATTRIB_IS_RANDOM_COORDINATES.c_str(), mxconst::ATTRIB_IS_RANDOM_COORDINATES.c_str()); // v3.0.219.14

      xmlPointNode.updateAttribute(this->ID, mxconst::ELEMENT_ICAO.c_str(), mxconst::ELEMENT_ICAO.c_str()); // v3.0.221.7 add the icao to the point. We will add this to the GPS too before the point lat/lon

    }
  }

    void syncXmlPointToNav()
    {
      if (xmlPointNode.isEmpty())
        return;

      NavAidInfo();

      //bool flag_found = false;
      lat = (float)Utils::readNumericAttrib(xmlPointNode, mxconst::ATTRIB_LAT, 0.0);
      lon = (float)Utils::readNumericAttrib(xmlPointNode, mxconst::ATTRIB_LONG, 0.0);
      double elev_ft = (float)Utils::readNumericAttrib(xmlPointNode, mxconst::ATTRIB_ELEV_FT, 0.0);
      height_mt = (float)(elev_ft * missionx::feet2meter);

      heading = (float)Utils::readNumericAttrib(xmlPointNode, mxconst::ATTRIB_HEADING_PSI, 0.0);
      this->template_type = Utils::readAttrib(xmlPointNode, mxconst::ATTRIB_TEMPLATE, EMPTY_STRING);

      flightLegName = Utils::readAttrib(xmlPointNode, mxconst::ATTRIB_NAME, EMPTY_STRING);
      radius_mt_suggested_s = Utils::readAttrib(xmlPointNode, mxconst::ATTRIB_RADIUS_MT, EMPTY_STRING);

      flag_picked_random_lat_long = (bool)Utils::readNumericAttrib(xmlPointNode, mxconst::ATTRIB_IS_RANDOM_COORDINATES, 0.0); // v3.0.219.14
      this->setID( Utils::readAttrib(xmlPointNode, mxconst::ELEMENT_ICAO, EMPTY_STRING) ); // // v3.0.221.7 store icao
      this->loc_desc = Utils::readAttrib(xmlPointNode, mxconst::ATTRIB_LOC_DESC, this->loc_desc); // v3.0.221.10



      p = Point(lat, lon);
      p.setElevationMt(height_mt);
      p.setHeading(heading);
      p.setName(flightLegName);
      p.setStringProperty(ATTRIB_ID, ID);
      p.setStringProperty(mxconst::ATTRIB_TEMPLATE, this->template_type);
      p.setStringProperty(mxconst::ATTRIB_RADIUS_MT, radius_mt_suggested_s); // v3.0.219.12+
      p.setBoolProperty(mxconst::ATTRIB_IS_RANDOM_COORDINATES, flag_picked_random_lat_long); // v3.0.219.14
      p.setStringProperty(mxconst::ELEMENT_ICAO, std::string(this->ID));

      p.setStringProperty(mxconst::ATTRIB_LOC_DESC, this->loc_desc); // v3.0.221.10
    }

  };

}
#endif 
