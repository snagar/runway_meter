#ifndef UTILSGRAPH_H_
#define UTILSGRAPH_H_
#pragma once

#include "XPLMGraphics.h"
#include "Point.hpp"


class UtilsGraph
{
public:

  /* *************************************************************** */

  static double getTerrainElevInMeter_FromPoint ( rwmeter::Point& p1, XPLMProbeResult& outResult )
  {
    double lat,lon,elev_mt;
    lat = lon = elev_mt = 0.0;

    XPLMProbeInfo_t info;

    outResult = -1;
    p1.calcSimLocalData();

    info = UtilsGraph::terrain_Y_Probe( p1.local_x, p1.local_y, p1.local_z, outResult  );

    if ( outResult == 0)
    {
      XPLMLocalToWorld ( info.locationX, info.locationY, info.locationZ, &lat, &lon, &elev_mt );
    }
    else
      elev_mt = 0.0;

    return elev_mt;
  }

/* *************************************************************** */

  static XPLMProbeInfo_t terrain_Y_Probe ( double & inXgl, double & inYgl, double & inZgl, XPLMProbeResult & outResult  )
  {

    //XPLMProbeResult probeResult;
    XPLMProbeRef    probe=NULL;
    // Create a Y probe
    probe = XPLMCreateProbe(xplm_ProbeY);

    static XPLMProbeInfo_t info;
    // initialized info struct.
    info.is_wet = 0;
    info.locationX = 0.0f;
    info.locationY = 0.0f;
    info.locationZ = 0.0f;
    info.normalX = 0.0f;
    info.normalY = 0.0f;
    info.normalZ = 0.0f;
    info.velocityX = 0.0f;
    info.velocityY = 0.0f;
    info.velocityZ = 0.0f;
    info.structSize = sizeof(info);

    outResult = XPLMProbeTerrainXYZ(probe, (float)inXgl, (float)inYgl, (float)inZgl, &info) ;


    XPLMDestroyProbe ( probe );

    return info ;
  }

  /* *************************************************************** */


}; // CLASS


 #endif // UTILSGRAPH_H_
