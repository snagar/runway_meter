#include "rw_data_manager.h"

#include <array>
//#include "coordinate/NavAidInfo.hpp"

#include <cstdio>
#include <iostream>
#include <fstream>



#ifdef IBM
#include "../io/dirent.vs.h" // directory header
#else
#include "dirent.h" // directory header
#endif

#include "../core/coordinate/UtilsGraph.hpp"

namespace rwmeter
{
  // VR Related
  XPLMDataRef rwmeter::rw_data_manager::g_vr_dref;
  int rwmeter::rw_data_manager::prev_vr_is_enabled_state;

  // TimeLaps
  rwmeter::TimeLaps rwmeter::rw_data_manager::timelaps;

  ////// TimeLaps class
  rwm_const rwmeter::TimeLaps::dc;

  bool rwmeter::rw_data_manager::flag_apt_dat_optimization_is_running;
  bool rwmeter::rw_data_manager::flag_generate_engine_is_running;
  std::map<std::string, int> rwmeter::rw_data_manager::mapIndexNavAids;
  std::map< std::string, std::list<std::string> > rwmeter::rw_data_manager::cachedNavInfo_map;

}




std::string rwmeter::TimeLaps::set_date_and_hours(std::string inDateAndTime)
{
  std::string err;
  err.clear();

  std::map<int, std::string> mapTime; // 0 = day in year, 1 = hours, 2 = min
  mapTime = Utils::splitStringToMap(inDateAndTime, ":");

  int dayOfYear = -1;
  int hours = -1;
  int minutes = -1;
  std::string val = "";

  this->start_local_date_days_i = XPLMGetDatai(rwmeter::TimeLaps::dc.dref_local_date_days_i);
  this->local_time_sec_f = XPLMGetDataf(rwmeter::TimeLaps::dc.dref_local_time_sec_f);
  this->zulu_time_sec_f = XPLMGetDataf(rwmeter::TimeLaps::dc.dref_zulu_time_sec_f);

  // store day in year
  if (Utils::isElementExists(mapTime, 0) && !mapTime[0].empty() && Utils::is_number(mapTime[0]))
  {
    dayOfYear = Utils::stringToNumber<int>(mapTime[0]);
    if (dayOfYear >= 0 && dayOfYear <= 364)
      futureDayOfYearAfterTimeLapse = dayOfYear;
    else
      futureDayOfYearAfterTimeLapse = -1;
  }
   
  // set timelap with new time and cycle 1
  if (Utils::isElementExists(mapTime, 1) && !mapTime[1].empty() && Utils::is_number(mapTime[1]) )
  {
    hours = Utils::stringToNumber<int>( mapTime[1] );
    if (Utils::isElementExists(mapTime, 2) && !mapTime[2].empty() && Utils::is_number(mapTime[2]))
      minutes = Utils::stringToNumber<int>(mapTime[2]);
    
    if (minutes < 0 || minutes > 59)
      minutes = 0;

    if (hours >= 0 && hours < 24)
    {
     err = this->timelapse_to_local_hour(hours, minutes, 1);
     if (!err.empty())
       Log::logDebugBO(err);
    }
  }
 

  return err;
}

/* ********************************************** */
std::string rwmeter::TimeLaps::timelap_add_minutes(int inMinutesToAdd, int inHowManyCycles)
{
  // get local day
  // get local time
  // get UTC time
  // Check if inMinutesToAdd > 1 and inSecondsToCycle between 1 and 24
  std::string err;
  err.clear();

  // do sanity tests
  if (inMinutesToAdd < 1)
    return ("Value is too small. Add more minutes.");

  if (inMinutesToAdd > 1440)
    return ("Value is to big. You should not add more than 24hours in minutes");

  if (inHowManyCycles < 1)
    inHowManyCycles = 1;

  if (inHowManyCycles > 24)
    inHowManyCycles = 24;


  // read datarefs
  this->start_local_date_days_i = XPLMGetDatai(rwmeter::TimeLaps::dc.dref_local_date_days_i);
  this->local_time_sec_f  = XPLMGetDataf(rwmeter::TimeLaps::dc.dref_local_time_sec_f);
  this->zulu_time_sec_f   = XPLMGetDataf(rwmeter::TimeLaps::dc.dref_zulu_time_sec_f);
  
  // calculate the future time. Make sure we do not pass next day and maybe even next year

  int seconds_to_add = inMinutesToAdd * 60; // convert minutes to seconds
  this->total_time_to_add_in_seconds = (float)seconds_to_add;
  this->expected_time_after_addition_in_seconds = local_time_sec_f + total_time_to_add_in_seconds; // this can be greater than 86400, so we need to be careful
  //this->overall_laps_time_in_sec = inHowManyCycles;
  this->cycles = inHowManyCycles;

  this->seconds_per_lap_f = (float)(this->total_time_to_add_in_seconds / this->cycles);


  rwmeter::Timer::start(this->timer, 1.0, "timelap_add_minutes, cycle: " + Utils::formatNumber<int>(this->cycleCounter));
  this->total_passed = 0.0f;
  this->flag_isActive = true;

  return err;
}


/* ********************************************** */

std::string rwmeter::TimeLaps::timelapse_to_local_hour(int inFutureLocalHour, int inFutureMinutes, int inHowManyCycles)
{
  // get local day
   // get local time
   // get UTC time
   // Check if inMinutesToAdd > 1 and inSecondsToCycle between 1 and 24
  std::string err;
  err.clear();

  // make sure timelapse is not active
  if (rwmeter::rw_data_manager::timelaps.flag_isActive)
    return "TimeLapsed is active. Can't interfier with its action. Aborting your request. Suggestion: Time it differently.";

  // do sanity tests
  if (inFutureLocalHour < 0 || inFutureLocalHour > 23)
    return ("Hours value needs to be between 0-23. Value received: " + Utils::formatNumber<int>(inFutureLocalHour) );

  if (inFutureMinutes < 0 || inFutureMinutes > 59)
    return ("Minutes value needs to be between 0-59. Value received: " + Utils::formatNumber<int>(inFutureMinutes));

  
  // read datarefs
  this->start_local_date_days_i = XPLMGetDatai(rwmeter::TimeLaps::dc.dref_local_date_days_i);
  this->local_time_sec_f = XPLMGetDataf(rwmeter::TimeLaps::dc.dref_local_time_sec_f);
  this->zulu_time_sec_f = XPLMGetDataf(rwmeter::TimeLaps::dc.dref_zulu_time_sec_f);


  if (inHowManyCycles < 0) // try to figure cycles for designer. We will use 24h/(24h - inFutureLocalHour)
  {
    // figure delta hours
    int local_time_in_hours = (int)(local_time_sec_f / rwmeter::SECONDS_IN_1HOUR_3600);
    int delta_hours = 0;
    if (inFutureLocalHour >= local_time_in_hours )
      delta_hours = inFutureLocalHour - local_time_in_hours;
    else
      delta_hours = (24 + inFutureLocalHour) - local_time_in_hours;

    // guess cycles
    inHowManyCycles = delta_hours;

  }

  if (inHowManyCycles < 1)
    inHowManyCycles = 1;

  if (inHowManyCycles > 24)
    inHowManyCycles = 24;

  // calculate the future time. Make sure we do not pass next day and maybe even next year

  float future_time_in_seconds =  (float)((inFutureLocalHour * 3600) + (inFutureMinutes * 60)); // convert hours and minutes to seconds from midnight. This represents the real hour in x-plane but in seconds and for local time. We need to find the delta for UTC.
  float seconds_to_add = 0.0f;

  // if future_time_in_seconds > local_time_sec_f then we are on same day
  if (future_time_in_seconds >= local_time_sec_f)
    seconds_to_add = future_time_in_seconds - local_time_sec_f;
  else if (future_time_in_seconds < local_time_sec_f) // we need to add 24 hours
    seconds_to_add = (future_time_in_seconds + rwmeter::SECONDS_IN_1DAY - local_time_sec_f);

 
  this->total_time_to_add_in_seconds = seconds_to_add;
  this->expected_time_after_addition_in_seconds = local_time_sec_f + total_time_to_add_in_seconds; // this can be greater than 86400, so we need to be careful
  //this->overall_laps_time_in_sec = inHowManyCycles;
  this->cycles = inHowManyCycles;

  this->seconds_per_lap_f = (float)(this->total_time_to_add_in_seconds / this->cycles);


  rwmeter::Timer::start(this->timer, 1.0, "timelapse to local hour, cycles: " + Utils::formatNumber<int>(this->cycleCounter));
  this->total_passed = 0.0f;
  this->flag_isActive = true;

  return err;
}

/* ********************************************** */

/* ********************************************** */

void rwmeter::TimeLaps::flc_timelap()
{
  if (this->flag_isActive)
  {
    if (rwmeter::Timer::wasEnded(this->timer, true)) // v3.0.223.1 changed to OS time
    {
      ++this->cycleCounter;
      float seconds_to_add = this->seconds_per_lap_f; // 
      this->total_passed += this->seconds_per_lap_f;
      // check if total_time is greater than time should pass. if so then just add the delta seconds.
      if (this->total_time_to_add_in_seconds < this->total_passed)
        seconds_to_add = this->total_passed - this->total_time_to_add_in_seconds;

      // we ignore the added seconds that passed during the timelap
      this->zulu_time_sec_f = XPLMGetDataf(rwmeter::TimeLaps::dc.dref_zulu_time_sec_f);
      XPLMSetDataf(rwmeter::TimeLaps::dc.dref_zulu_time_sec_f, ((float)(this->zulu_time_sec_f + seconds_to_add))); // add time

      // decide if to reset timer and end timelap      
      if (this->total_time_to_add_in_seconds <= this->total_passed)
      {
        if (this->expected_time_after_addition_in_seconds >= rwmeter::SECONDS_IN_1DAY) // check if we passed local midnight
        {
          // make sure that during the timelaps date was not change already. 
          // Change only if on same day
          int   current_local_date_days_i = XPLMGetDatai(rwmeter::TimeLaps::dc.dref_local_date_days_i);
          if (current_local_date_days_i == this->start_local_date_days_i)
          {
            int new_days_in_year = this->start_local_date_days_i + 1;
            if (new_days_in_year >= 365 || new_days_in_year < 0) // 364 is last day of the year. 0 is the first day of the year in X-Plane
              new_days_in_year = 0;

            XPLMSetDatai(rwmeter::TimeLaps::dc.dref_local_date_days_i, (new_days_in_year));
          }
        }

        // set day of year if was set. Example in setDateAndHour. this will override any day set by this function
        if (this->futureDayOfYearAfterTimeLapse >= 0 && this->futureDayOfYearAfterTimeLapse < 364)
          XPLMSetDatai(rwmeter::TimeLaps::dc.dref_local_date_days_i, this->futureDayOfYearAfterTimeLapse);

        this->reset(); // free timelap and ready to other
      }
      else
      {
        this->timer.reset();
        rwmeter::Timer::start(this->timer, 1.0, "timelap_add_minutes, cycle: " + Utils::formatNumber<int>(this->cycleCounter));
        Log::logMsg("Timelap Cycle:" + Utils::formatNumber<int>(this->cycleCounter));
      }

    } // end if timer passed (wasEnded)

    if (this->cycleCounter > 30)
    {
      Log::logMsg("[TimeLaps] Time laps has exceed its maximum cycles. Reseting timelap. Notify developer.");
      this->reset();
    }

  }// end if isActive

} // End TimeLaps::flc_timelap()





/* ********************************************** */
/* ********************************************** */

rwmeter::rw_data_manager::rw_data_manager()
{


}

/* ********************************************** */

rwmeter::rw_data_manager::~rw_data_manager()
{
}


/* ********************************************** */

void rwmeter::rw_data_manager::flc_vr_state()
{
  // v3.0.219.7
  rwmeter::mxvr::vr_is_enabled = XPLMGetDatai(g_vr_dref);

  //auto val = rwmeter::system_actions::pluginOptions.getPropertyValue<int>(mxconst::OPT_DISPLAY_MISSIONX_IN_VR, rw_data_manager::errStr); // override VR state. enable/disable rwmeter in VR
  //if (!val)
  //  rwmeter::mxvr::vr_is_enabled = false;

  if (rwmeter::mxvr::vr_is_enabled)
    rwmeter::mxvr::vr_ratio = rwmeter::mxvr::VR_RATIO; // 0.75f; // 0.52f;
  else
    rwmeter::mxvr::vr_ratio = rwmeter::mxvr::DEFAULT_RATIO; //  1.0f;
  
}

/* ********************************************** */
std::string rwmeter::rw_data_manager::set_local_time(int inHours, int inMinutes, int inDayOfYear_0_364)
{
  static rwmeter::rwm_const dc;
  std::string err;
  err.clear();


  // do sanity tests
  if (rwmeter::rw_data_manager::timelaps.flag_isActive)
    return ("TimeLapsed is active. Can't interfier with its action. Aborting your request. Suggestion: Time it differently.");

  if (inHours < 0 || inHours > 23)
    return ("Hours value needs to be between 0-23. Value received: " + Utils::formatNumber<int>(inHours));

  if (inMinutes < 0 || inMinutes > 59)
    return ("Minutes value needs to be between 0-59. Value received: " + Utils::formatNumber<int>(inMinutes));

  if (inDayOfYear_0_364 < -1 || inDayOfYear_0_364 > 364)
    return ("Day in year must be between 0 and 364. Value received: " + Utils::formatNumber<int>(inDayOfYear_0_364));

  float zulu_time_sec_f = XPLMGetDataf(dc.dref_zulu_time_sec_f);
  float local_time_sec_f = XPLMGetDataf(dc.dref_local_time_sec_f);
  int start_local_date_days_i = XPLMGetDatai(dc.dref_local_date_days_i);

  if (inDayOfYear_0_364 != -1) // -1 = undefined, same day.
    XPLMSetDatai(dc.dref_local_date_days_i, (inDayOfYear_0_364));

  float new_local_time_in_seconds = (float)(inHours * rwmeter::SECONDS_IN_1HOUR_3600 + inMinutes * rwmeter::SECONDS_IN_1MINUTE);
  float delta_between_currentLocal_and_newLocal = new_local_time_in_seconds - local_time_sec_f; //we want to minus to add later to zulu

  // set the new time
  XPLMSetDataf(dc.dref_zulu_time_sec_f, zulu_time_sec_f + delta_between_currentLocal_and_newLocal); // set zulu time to reflect the local time


  return err;
}

/* ********************************************** */


/* ********************************************** */
/* ********************************************** */

/* ********************************************** */

/* ********************************************** */

/* ********************************************** */
/* ********************************************** */
/* ********************************************** */

