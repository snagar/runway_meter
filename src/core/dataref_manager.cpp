/*
 * dataref_manager.cpp
 *
 *  Created on: Aug 25, 2017
 *      Author: snagar
 */

/**************


**************/
#include "dataref_manager.h"

namespace missionx
{
  Point           missionx::dataref_manager::planePoint;
  XPLMDataTypeID  missionx::dataref_manager::dataRefType;

}




missionx::dataref_manager::dataref_manager()
{
  // TODO Auto-generated constructor stub
  init();
  //Log::logMsg(std::string("In dataref_manager. pause_dref value: ").append(  (pause_dref == NULL)? "NULL": "Something"  ) );

}

void missionx::dataref_manager::init()
{
}

missionx::dataref_manager::~dataref_manager()
{
  // TODO Auto-generated destructor stub
}

void missionx::dataref_manager::flc()
{

  dataref_manager::storePlanePoint();

}


void    missionx::dataref_manager::writeToDataRef ( std::string full_name, double inValue )
{
  static XPLMDataRef dataRefId; // = XPLMFindDataRef(full_name.c_str());
  dataRefId = XPLMFindDataRef(full_name.c_str());

  if ( dataRefId ) // if exists
  {
    dataRefType = XPLMGetDataRefTypes ( dataRefId );
    switch ( dataRefType )
    {
    case xplmType_Int:
    {
      XPLMSetDatai ( dataRefId , (int)inValue );
    }
    break;
    case xplmType_Float:
    {
      XPLMSetDataf ( dataRefId , (float)inValue );
    }
    break;
    case ( xplmType_Float | xplmType_Double ):
    {
      XPLMSetDatad ( dataRefId , inValue );
    }
    break;
    case (xplmType_IntArray): //  can only return the value of specific array and not the whole array. arrayElementPicked must be defined
    {
      Log::logMsg("Setting value into INT array is not supported yet.");
    }
    break;
    case (xplmType_FloatArray): // can only return the value of specific array and not the whole array. arrayElementPicked must be defined
    {
      Log::logMsg("Setting value into FLOAT array is not supported yet.");
    }
    break;
    default:
    {
      Log::logMsg("Can't handle this Dataref Datatype!!! ");
    }
    break;
    }// end switch
  } // end if
} // end writeToDataRef

XPLMDataRef missionx::dataref_manager::getDataRef (std::string inDrefName) // v2.1.0
{
  return XPLMFindDataRef ( inDrefName.c_str() );
}



/////////////////////////////////////////////////////////////////////


bool missionx::dataref_manager::isSimPause()
{
  if ( missionx::drefConst.dref_pause )
    return (bool)XPLMGetDatai (missionx::drefConst.dref_pause); // this should always work


  return (bool)getDataRefValue<int>(std::string("sim/time/paused")); // just in case
}

bool missionx::dataref_manager::isPlaneOnGround()
{
  return ( XPLMGetDataf (missionx::drefConst.dref_faxil_gear_f) == 0.0f ? false : true ) ; // in air value == 0
}

double missionx::dataref_manager::getLat ()
{
  return XPLMGetDatad(missionx::drefConst.dref_lat_d );
}

double missionx::dataref_manager::getLong ()
{
  return XPLMGetDatad(missionx::drefConst.dref_lon_d );
}

double missionx::dataref_manager::getElevation ()
{
  return XPLMGetDatad(missionx::drefConst.dref_elev_d );
}

float missionx::dataref_manager::getHeadingPsi ()
{
  return XPLMGetDataf(missionx::drefConst.dref_heading_true_psi_f );
}

float missionx::dataref_manager::getTotalRunningTimeSec()
{
  return XPLMGetDataf(missionx::drefConst.dref_total_running_time_sec_f);
}

int missionx::dataref_manager::getLocalDateDays()
{
  return XPLMGetDatai(missionx::drefConst.dref_local_date_days_i);
}

float missionx::dataref_manager::getLocalTimeSec()
{
  return XPLMGetDataf(missionx::drefConst.dref_local_time_sec_f);
}



/* *********************************************** */

void missionx::dataref_manager::setQuaternion(float w, float x, float y, float z)
{
  float floatVals[4];
  floatVals[0] = w;
  floatVals[1] = x;
  floatVals[2] = y;
  floatVals[3] = z;

  if (missionx::drefConst.dref_q != NULL) 
    XPLMSetDatavf(missionx::drefConst.dref_q, floatVals, 0, 4);
  else
    Log::logMsgErr("Failed to find Quaternion values !!!");

}

missionx::Point missionx::dataref_manager::getPlanePointLocation()
{
  return missionx::dataref_manager::planePoint;
}

missionx::Point missionx::dataref_manager::getCurrentPlanePointLocation(bool inStoreLocation)
{
  missionx::Point p(dataref_manager::getLat(), dataref_manager::getLong());
  p.setElevationMt(dataref_manager::getElevation());
  p.setHeading((double)dataref_manager::getHeadingPsi()); // v3.0.217.3

  if (inStoreLocation)
    dataref_manager::planePoint = p;


  return p;
}

/* *********************************************** */
void missionx::dataref_manager::storePlanePoint()
{    
  dataref_manager::planePoint = getCurrentPlanePointLocation(true);

}

/* *********************************************** */



/* *********************************************** */
/* *********************************************** */
