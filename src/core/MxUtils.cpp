#include "MxUtils.h"

rwmeter::mxUtils::mxUtils()
{
}

rwmeter::mxUtils::~mxUtils()
{
}


/*
THE FOLLOWING FUNCTIONS WERE COPIED FROM "uTILS" CLASS SO PROPERTIES WILL BE INDEPENDENT FOR ANY CLASS
*/


/* ********************************************** */


string rwmeter::mxUtils::stringToUpper(string strToConvert)
{//change each element of the string to upper case
  for (size_t i = 0; i<strToConvert.length(); i++)
  {
    strToConvert[i] = toupper(strToConvert[i]);
  }
  return strToConvert;//return the converted string
}

/* *************************************************************** */

string rwmeter::mxUtils::stringToLower(string strToConvert)
{//change each element of the string to lower case
  for (size_t i = 0; i<strToConvert.length(); i++)
  {
    strToConvert[i] = tolower(strToConvert[i]);
  }
  return strToConvert;//return the converted string
}


/* *************************************************************** */

bool rwmeter::mxUtils::is_alpha(const std::string &str)
{
  return std::all_of(str.begin(), str.end(), ::isalpha); // C++11
}

/* *************************************************************** */

bool rwmeter::mxUtils::is_digits(const std::string &str)
{
  //return str.find_first_not_of("0123456789.") == std::string::npos;
  return std::all_of(str.begin(), str.end(), ::isdigit); // C++11
}

/* *************************************************************** */

//Is string has only these characters - "0123456789.", and no more than 1 "." !!!
bool rwmeter::mxUtils::is_number(const std::string& s)
{
  bool validNumber = true;

  bool minusPlusNotAllowed = false; // as long we did not found digits, we can use "+/-" signs

  bool foundDot = false;

  std::string::const_iterator it = s.begin();
  while (it != s.end() && validNumber)
  {
    // number is legal if starts with "+/-" signs
    if (!minusPlusNotAllowed && (((*it) == '+') || ((*it) == '-')))
    {
      minusPlusNotAllowed = true; // only one sign is allowed at the begining
      ++it;
    }
    else if ((*it) == '.' && !foundDot)
    {
      foundDot = true;
      minusPlusNotAllowed = true;
      ++it;
    }
    else if (::isdigit(*it))
    {
      minusPlusNotAllowed = true;
      ++it;
    }
    else
      validNumber = false;
  } // end while

  return !s.empty() && it == s.end();
}

/* *************************************************************** */

std::string rwmeter::mxUtils::ltrim(string str, const std::string& chars )
{
  // trim leading spaces
  size_t startpos = str.find_first_not_of(chars);//(" \t");
  if (string::npos != startpos)
  {
    str = str.substr(startpos);
  }

  return str;
} // ltrim

  /* *************************************************************** */
std::string rwmeter::mxUtils::rtrim(std::string& str, const std::string& chars )
{
  str.erase(str.find_last_not_of(chars) + 1);
  return str;
}

/* *************************************************************** */
std::string rwmeter::mxUtils::trim(std::string& str, const std::string& chars )
{
  return ltrim(rtrim(str, chars), chars);
}

/* *************************************************************** */

bool rwmeter::mxUtils::isStringBool(std::string inTestValue, bool &outResult)
{
  std::string inValue = stringToLower(inTestValue);


  if (inValue.length() == 1)
  {
    char c = inValue[0];
    outResult = (bool)c;
  }
  else if ((rwmeter::MX_TRUE.compare(inValue) == 0) || (rwmeter::MX_YES.compare(inValue) == 0))
  {
    outResult = true; // true for boolean or numbers
  }
  else if ((rwmeter::MX_FALSE.compare(inValue) == 0) || (rwmeter::MX_NO.compare(inValue) == 0))
  {
    outResult = false; // false for boolean or numbers
    return true; // special case where the value is boolean but its value is false, we need to return if the value was found from function
  }
  else if (is_number(inValue))
  {
    outResult = stringToNumber<bool>(inValue);
    return true; // special case // number can 0 or minus
  }
  else
  {
    // fail to parse attrib string to number. Setting property to false;
    outResult = false;

  }

  return outResult;
}

std::vector<std::string> rwmeter::mxUtils::split(const std::string & s, char delimiter)
{
  std::vector<std::string> tokens;
  std::string token;
  std::istringstream tokenStream(s);
  while (std::getline(tokenStream, token, delimiter))
  {
    if (token.empty())
      continue;

    tokens.push_back(token);
  }
  return tokens;
}

std::string rwmeter::mxUtils::translateMxPadLabelPositionToValid(std::string inLabelPosition)
{
  std::string mLabelPosition;
  // v3.0.197 - translate position string to valid one
  mLabelPosition = mxUtils::stringToUpper(inLabelPosition);
  if (mLabelPosition.compare("L") == 0 || mLabelPosition.compare("LEFT") == 0)
    mLabelPosition = "L";
  else if (mLabelPosition.compare("R") == 0 || mLabelPosition.compare("RIGHT") == 0)
    mLabelPosition = "R";
  else if (!mLabelPosition.empty())
    mLabelPosition = "L";


  return mLabelPosition;
}

/* *************************************************************** */

std::string rwmeter::mxUtils::emptyReplace(std::string inValue, std::string inReplaceWith)
{
  if (inValue.empty())
    return inReplaceWith;

  return inValue;
}

rwmeter::mx_btn_colors rwmeter::mxUtils::translateStringToButtonColor(std::string inColor)
{
  //mx_btn_colors color_val;

  //if (YELLOW.compare(inColor) == 0)
  //  return rwmeter::mx_btn_colors::yellow;
  //else if (RED.compare(inColor) == 0)
  //  return rwmeter::mx_btn_colors::red;
  //else if (GREEN.compare(inColor) == 0)
  //  return rwmeter::mx_btn_colors::green;
  //else if (BLUE.compare(inColor) == 0)
  //  return rwmeter::mx_btn_colors::blue;
  //else if (BLACK.compare(inColor) == 0)
  //  return rwmeter::mx_btn_colors::black;

  return rwmeter::mx_btn_colors::white;
}


/* *************************************************************** */
/* *************************************************************** */
/* *************************************************************** */


