#ifndef BASE_XP_INCLUDE_H_
#define BASE_XP_INCLUDE_H_

/**************


**************/

#include "base_c_includes.h"

// X-Plane includes

#include "XPLMPlugin.h"
#include "XPLMDisplay.h"
#include "XPLMGraphics.h"
#include "XPLMDataAccess.h"
#include "XPLMUtilities.h"
#include "XPLMProcessing.h"
#include "XPLMMenus.h"
#include "XPLMNavigation.h"
#include "XPWidgetUtils.h"
#include "XPWidgets.h"
#include "XPStandardWidgets.h"
#include "XPLMScenery.h"
#include "XPLMPlanes.h"


#endif // BASE_XP_INCLUDE_H_
