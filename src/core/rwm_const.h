#ifndef rwmeter_const_h_
#define rwmeter_const_h_


#include <string>
#include "XPLMDataAccess.h"
#include "XPLMMenus.h"

using namespace std;

namespace rwmeter
{

  
class rwm_const
{
public:
  ~rwm_const(void) {};

//  std::map <std::string, XPLMDataRef> map_dref;

  rwm_const(void)
  {


  }

  // Menu Commands/sub menus
  typedef enum _menuIdRefs : uint8_t
  {
    RW_MENU_ENTRY, // Menu seed under plug-in menu
    GENERATE_RW_OPTIMIZED_FILE,
    TOGGLE_RW_METER
  } rwmMenu_enum;
  static XPLMMenuID rwmMenuEntry;

  const XPLMDataRef dref_acf_m_empty_weight = XPLMFindDataRef("sim/aircraft/weight/acf_m_empty"); // plane empty weight // v3.0.213.3
  const XPLMDataRef dref_acf_m_max_weight   = XPLMFindDataRef("sim/aircraft/weight/acf_m_max"); // plane max weight (payload + fuel) // v3.0.213.3

  const XPLMDataRef dref_lat_d = XPLMFindDataRef("sim/flightmodel/position/latitude"); // latitude
  const XPLMDataRef dref_lon_d = XPLMFindDataRef("sim/flightmodel/position/longitude"); // longitude
  const XPLMDataRef dref_elev_d = XPLMFindDataRef("sim/flightmodel/position/elevation"); // elevation
  const XPLMDataRef dref_faxil_gear_f = XPLMFindDataRef("sim/flightmodel/forces/faxil_gear"); // faxil_gear
  const XPLMDataRef dref_brake_Left_add_f = XPLMFindDataRef("sim/flightmodel/controls/l_brake_add"); // l_brake_add - add Left brake
  const XPLMDataRef dref_brake_Right_add_f = XPLMFindDataRef("sim/flightmodel/controls/r_brake_add"); // r_brake_add - add Right brake

  const XPLMDataRef dref_frame_rate_period = XPLMFindDataRef("sim/operation/misc/frame_rate_period"); // v3.0.207.1 // frame_rate_period - fps 

  const XPLMDataRef dref_m_fuel_f_arr = XPLMFindDataRef("sim/flightmodel/weight/m_fuel");  // weight for each 9 tanks
  const XPLMDataRef dref_m_fuel1_f = XPLMFindDataRef("sim/flightmodel/weight/m_fuel1");
  const XPLMDataRef dref_m_fuel2_f = XPLMFindDataRef("sim/flightmodel/weight/m_fuel2");
  const XPLMDataRef dref_m_fuel3_f = XPLMFindDataRef("sim/flightmodel/weight/m_fuel3");
  const XPLMDataRef dref_m_fuel_total_f = XPLMFindDataRef("sim/flightmodel/weight/m_fuel_total"); // weight in Kg
  const XPLMDataRef dref_groundspeed_f = XPLMFindDataRef("sim/flightmodel/position/groundspeed"); // Groundspeed meters/sec
  const XPLMDataRef indicated_airspeed_f = XPLMFindDataRef("sim/flightmodel/position/indicated_airspeed"); // indicated_airspeed Kias
  const XPLMDataRef dref_local_vz_f = XPLMFindDataRef("sim/flightmodel/position/local_vz"); // Velocity of aircraft in its own coordinate system meters/sec
  const XPLMDataRef dref_local_vx_f = XPLMFindDataRef("sim/flightmodel/position/local_vx"); // Velocity of aircraft in its own coordinate system meters/sec
  const XPLMDataRef dref_q = XPLMFindDataRef("sim/flightmodel/position/q"); // quaternion 
  const XPLMDataRef dref_use_real_weather = XPLMFindDataRef("sim/weather/use_real_weather_bool"); // real weather usage flag
  const XPLMDataRef dref_acf_h_eqlbm = XPLMFindDataRef("sim/aircraft/gear/acf_h_eqlbm"); // equilibrium - height of plane body from ground // calc plane location when moving it
  // dataref for step statistics
  const XPLMDataRef dref_local_date_days_i = XPLMFindDataRef("sim/time/local_date_days"); // local_date_days - Day in year
  const XPLMDataRef dref_local_time_sec_f = XPLMFindDataRef("sim/time/local_time_sec"); // local_time_sec - how many millisiconds from midnight. When value between   greater 66600 or less 18000 it will mark as night
  const XPLMDataRef dref_zulu_time_sec_f = XPLMFindDataRef("sim/time/zulu_time_sec"); // zulu_time_sec - how many millisiconds from ZULU midnight. When value between   greater 66600 or less 18000 it will mark as night

  const XPLMDataRef dref_local_x_d       = XPLMFindDataRef("sim/flightmodel/position/local_x"); // local_x
  const XPLMDataRef dref_local_y_d       = XPLMFindDataRef("sim/flightmodel/position/local_y"); // local_y
  const XPLMDataRef dref_local_z_d       = XPLMFindDataRef("sim/flightmodel/position/local_z"); // local_z

  const XPLMDataRef dref_pitch_f = XPLMFindDataRef("sim/flightmodel/position/true_theta"); // The pitch of the aircraft relative to the earth precisely below the aircraft
  const XPLMDataRef dref_roll_f  = XPLMFindDataRef("sim/flightmodel/position/true_phi"); // The roll of the aircraft relative to the earth precisely below the aircraft
  const XPLMDataRef dref_heading_true_psi_f= XPLMFindDataRef("sim/flightmodel/position/true_psi"); // The heading of the aircraft relative to the earth precisely below the aircraft - true degrees north, always
  const XPLMDataRef dref_heading_mag_psi_f = XPLMFindDataRef("sim/flightmodel/position/mag_psi"); // The real magnetic heading of the aircraft - the old magpsi dataref was FUBAR
  const XPLMDataRef dref_heading_psi_f = XPLMFindDataRef("sim/flightmodel/position/psi"); // The true heading of the aircraft in degrees from the Z axis - OpenGL coordinates

  const XPLMDataRef dref_total_running_time_sec_f = XPLMFindDataRef("sim/time/total_running_time_sec"); // The real magnetic heading of the aircraft - the old magpsi dataref was FUBAR
  const XPLMDataRef dref_vh_ind_f = XPLMFindDataRef("sim/flightmodel/position/vh_ind"); // VVI - vertical velocity in meters per second
  const XPLMDataRef dref_gforce_normal_f = XPLMFindDataRef("sim/flightmodel2/misc/gforce_normal"); // 
  const XPLMDataRef dref_gforce_axil_f = XPLMFindDataRef("sim/flightmodel2/misc/gforce_axil"); // 
  const XPLMDataRef AoA_f = XPLMFindDataRef("sim/flightmodel/position/alpha"); // Angle of Attack degrees
  const XPLMDataRef dref_pause = XPLMFindDataRef("sim/time/paused"); // is Sim in pause

  // v3.0.138
  const XPLMDataRef dref_render_window_width = XPLMFindDataRef("sim/graphics/view/window_width");  // v3.0.138
  const XPLMDataRef dref_render_window_height = XPLMFindDataRef("sim/graphics/view/window_height");  // v3.0.138

  // v3.0.219.3
  const XPLMDataRef dref_camera_view_x_f = XPLMFindDataRef("sim/graphics/view/view_x");  // v3.0.219.3
  const XPLMDataRef dref_camera_view_y_f = XPLMFindDataRef("sim/graphics/view/view_y");  // v3.0.219.3
  const XPLMDataRef dref_camera_view_z_f = XPLMFindDataRef("sim/graphics/view/view_z");  // v3.0.219.3
  
  // v3.0.221.11 CUSTOM datarefs should not be XPLMDataRef const since they won't be available until plugin will finish initialization. Instead we will hold their "path"
  const std::string dref_xpshared_target_status_i_str="xpshared/target/status"; // = XPLMFindDataRef("xpshared/target/status");  // v3.0.221.9  -1: fail, 0: waiting 1: success
  const std::string dref_xpshared_target_listen_plugin_available_i_str= "xpshared/target/listen_plugin_available";// = XPLMFindDataRef("xpshared/target/listen_plugin_available");  // v3.0.221.9  1=listen 0 =no one is listening

                                                                                                
  // const XPLMDataRef dref_
}; 



} // rwmeter
#endif // rwmeter_const_h_
