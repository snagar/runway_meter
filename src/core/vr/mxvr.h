#ifndef MXVR_H_
#define MXVR_H_
#pragma once

namespace rwmeter
{

  class mxvr
  {
  public:
    ~mxvr();
    mxvr();


    static int vr_is_enabled;
    static float vr_ratio;  

    static const float VR_RATIO;// = 0.75f;
    static const float DEFAULT_RATIO; //= 1.0f;
  };

}
#endif