#include "mxvr.h"

namespace rwmeter
{

  int rwmeter::mxvr::vr_is_enabled;  // v3.0.221.6
  float rwmeter::mxvr::vr_ratio;  // v3.0.221.6

  const float mxvr::VR_RATIO = 0.75f;
  const float mxvr::DEFAULT_RATIO = 1.0f;

  mxvr::mxvr()
  {
    vr_ratio = 1.0f;
    vr_is_enabled = 0;
  }


  mxvr::~mxvr()
  {
  }

}

