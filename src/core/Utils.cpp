#include "Utils.h"
using namespace rwmeter;


namespace rwmeter
{  
  std::string Utils::logFilePath;
}

/**************/
// initialize map
//const std::map<std::string, int> Utils::mapCloudTypes = { { rwmeter::CLOUD_TYPE1,0 },{ rwmeter::CLOUD_TYPE2, 1 }, { rwmeter::CLOUD_TYPE3, 2}, { rwmeter::CLOUD_TYPE4, 3}, { rwmeter::CLOUD_TYPE5, 4}, { rwmeter::CLOUD_TYPE6, 5},{ rwmeter::CLOUD_TYPE7, 6 } };

rwmeter::Utils::Utils()
{
  

}

rwmeter::Utils::~Utils()
{
}

/* *************************************************************** */

//void rwmeter::Utils::logMsg(std::string message, bool writeToXPLog)
//{
//  std::string err;
//  err.clear();
//
//  if (message.empty())
//    return;
//  if (writeToXPLog)
//    XPLMDebugString(message.c_str());
//
//  system_actions::write_missionx_log_file(Utils::logFilePath, message, err);
//
//}


////////////////////// Convert Units of Measure ////////////////

rwmeter::mx_units_of_measure rwmeter::Utils::translateStringToUnits(std::string &inVal)
{
  inVal = Utils::stringToLower(inVal); // to lower case

  if (inVal.compare("ft") == 0)
    return rwmeter::mx_units_of_measure::ft;
  else if (inVal.compare("mt") == 0)
    return rwmeter::mx_units_of_measure::meter;
  else if (inVal.compare("km") == 0)
    return rwmeter::mx_units_of_measure::km;
  else if (inVal.compare("nm") == 0)
    return rwmeter::mx_units_of_measure::nm;

  return rwmeter::mx_units_of_measure::unit_unsupports;
}

/* *************************************************************** */
float rwmeter::Utils::convertNmTo(float & nm, mx_units_of_measure from)
{
  switch (from)
  {
  case mx_units_of_measure::ft:
  {
    return ((nm*nm2meter)*meter2feet);
  }
  break;
  case mx_units_of_measure::meter:
  {
    return nm*rwmeter::nm2meter;
  }
  break;
  case mx_units_of_measure::km:
  {
    return nm*rwmeter::nm2km;
  }
  break;
  default:
    break;
  }

  return nm;
}

/* *************************************************************** */
float rwmeter::Utils::convertToNm(float & v, mx_units_of_measure from)
{
  switch (from)
  {
  case mx_units_of_measure::ft:
  {
    return ((v*feet2meter)*meter2nm);
  }
  case mx_units_of_measure::meter:
  {
    return v*rwmeter::meter2nm;
  }
  break;
  case mx_units_of_measure::km:
  {
    return v*rwmeter::km2nm;
  }
  break;
  default:
    break;
  }

  return v;
}

/* *************************************************************** */

//FMOD_RESULT rwmeter::Utils::checkResult(FMOD_RESULT result)
//{
//  if (result != FMOD_OK)
//  {
//    std::string err(FMOD_ErrorString(result));
//    err = "FMOD error! (code: " + Utils::formatNumber<int>(result) + ") " + err + rwmeter::UNIX_EOL;
//    Log::logMsgErr(err);
//  }
//  //return FmodErrorCheck(result);
//
//  return result;
//}

/* *************************************************************** */
int rwmeter::Utils::getPrecisionFromString(std::string &inValue)
{
  int precision = 0;
  size_t dotLocation;
  std::stoi(inValue, &dotLocation);
  if (dotLocation)
    precision = (int)std::string(inValue).substr(dotLocation).size();

  return precision;
}

/* *************************************************************** */
/* *************************************************************** */

std::vector<std::string> rwmeter::Utils::splitStringToLines(std::string source, size_t width)
{
  int sourceLen = (int)source.length();
  int strLastPos = 0;
  int strLeftLength = sourceLen;
  int lastNewLinePos = 0; // source.npos;
  int widthToCut = 0;
  int spacePos = 0;
  int debugCounter = 0;

  std::string cutString = EMPTY_STRING; // v3.0.148
  std::vector<std::string> vecSentences; // v3.0.148
  std::vector<std::string> vecReturnSentences; // v3.0.150
  vecSentences.clear();
  vecReturnSentences.clear();

#ifdef DEBUG
  Utils::logMsg(source);
#endif

  try
  {
    // Loop as long the remain string is longer than "width" restriction
    while (strLeftLength > 1 && debugCounter < 50)
    {
      debugCounter++;
      // find first "\n" (UNIX_EOL) in this widget for new line
      lastNewLinePos = (int)source.find(rwmeter::UNIX_EOL, ((lastNewLinePos == (int)source.npos) ? 0 : strLastPos + 1));


      if (lastNewLinePos != (int)std::string::npos && ((lastNewLinePos - strLastPos) <= (int)width)) // if found ';' before "width"
      {
        widthToCut = lastNewLinePos - strLastPos;
        cutString = ltrim(source.substr(strLastPos, widthToCut));
        //target += cutString;
        vecSentences.push_back(cutString); // v3.0.148 add line

        strLeftLength -= widthToCut + 1; // decrease left length
        strLastPos += widthToCut + 1;
       
      }
 
      else if ((lastNewLinePos != (int)std::string::npos && lastNewLinePos > (int)width)
        || lastNewLinePos == (int)source.npos)
      {
        // make sure lastNewLinePos > -1 ( Avoid Out of range error )
        lastNewLinePos = (lastNewLinePos == (int)std::string::npos) ? 0 : lastNewLinePos;

        widthToCut = (int)width;
        if (source.at(lastNewLinePos) != ' ')
        {
          // calculate new lastNewLinePos to point to SPACE if possible
          spacePos = (int)source.find_last_of(' ', (strLastPos + width));

          if (spacePos != (int)source.npos && spacePos > strLastPos)
            widthToCut = spacePos - strLastPos; // change the width to cut
        }

        cutString = ltrim(source.substr(strLastPos, widthToCut));
        vecSentences.push_back(cutString); // v3.0.148 add line
        //target += cutString;

        strLastPos += widthToCut;
        strLeftLength -= widthToCut;

      } // if lastNLPos > width


    } // end while


  }
  catch (out_of_range &oor) {
    std::string what = oor.what();    
    //Utils::logMsg(what);
    
  }

  //     add last characters to Target string
  if (strLeftLength > 0)
  {
    cutString = source.substr(strLastPos, strLeftLength);
    vecSentences.push_back(cutString); // v3.0.148 add line
    //target += cutString;
  }
    

  // remove special characters from sentences. Replace with space
  for (auto s : vecSentences)
  {
    std::string outResult = s;
    replaceCharsWithString(outResult, "\r\t\n", rwmeter::SPACE);
    vecReturnSentences.push_back(outResult);
  }


  return vecReturnSentences;
}

/* *************************************************************** */


/**
replaceChar1WithChar2
Replaces a character with another character or string of characters
Uses: remove the LF and UNIX_EOL from strings
*/
std::string rwmeter::Utils::replaceChar1WithChar2(std::string inString, char charToReplace, std::string newChar)
{
  std::string outputStr;
  size_t lenString = 0;
  outputStr = inString;
  lenString = outputStr.length();
  bool no_more_to_search = false;

  size_t loc = 0;

  for (size_t i = 0; i < lenString && !no_more_to_search; i++)
  {
    loc = outputStr.find(charToReplace, i);
    if (loc != outputStr.npos && loc <= lenString)
      outputStr.replace(loc, 1, newChar);
    else
      no_more_to_search = true; // skip loop if nothing was found

  }

  return outputStr;
}

/* *************************************************************** */

std::string rwmeter::Utils::replaceChar1WithChar2_v2(std::string inString, char charToReplace, std::string newChar)
{
  std::string result; result.clear();

  int length = (int)inString.length();

  for (auto &c : inString)
  {
    if (c == charToReplace)
    {
      if (newChar.empty())
        continue;

      result += newChar;
    }
    else
      result.push_back(c);
  }


  return result;
}

// replaceChar1WithChar2_v2

  /* *************************************************************** */
  /**
  replaceCharsWithString
  Replaces set of characters, one by one, with another string of characters
  Uses: remove the LF and UNIX_EOL from strings
  */
void rwmeter::Utils::replaceCharsWithString(std::string &outString, std::string charsToReplace, std::string newChar)
{
  std::string result = outString;

  //outputStr = inString;
//  size_t lenString = 0;
//  lenString = outString.length();
  bool no_more_to_search = false;

  size_t loc = 0;
  for (auto c = charsToReplace.begin(); c != charsToReplace.end(); c++)
  {
    no_more_to_search = false;
    for (size_t i = 0; i < result.length() && !no_more_to_search; i++)
    {
      loc = result.find((*c), i);
      if (loc != result.npos)
        result.replace(loc, 1, newChar);
      else
        no_more_to_search = true; // skip loop if nothing was found

    } // end internal loop
  } // end external loop

  outString = result;

}
  std::string rwmeter::Utils::replaceStringWithOtherString(std::string inStringToModify, std::string inStringToReplace, std::string inNewString, bool flag_forAllOccurances)
  {
    bool flag_continueSearch = true;

    int pos = (int)(inStringToModify.find(inStringToReplace));
    while ( pos != (int)(std::string::npos) )
    {
      inStringToModify.replace(pos, inStringToReplace.length(), inNewString);
      pos = (int)(std::string::npos);

      if (flag_forAllOccurances)
        pos = (int)(inStringToModify.find(inStringToReplace));
      else
        break;
    }

    return inStringToModify;
  }

// replaceCharsWithString

  /* *************************************************************** */

double rwmeter::Utils::calcDistanceBetween2Points_nm(double gFromLat, double gFromLong, double gTargetLat, double gTargetLong)
{
  double angle, pLatRad, pLongRad, pTargetLatRad, pTargetLongRad;

  // convert lat/long to Radiance
  pLatRad = rwmeter::PI / 180 * gFromLat;
  pLongRad = rwmeter::PI / 180 * gFromLong;

  pTargetLatRad = rwmeter::PI / 180 * gTargetLat;
  pTargetLongRad = rwmeter::PI / 180 * gTargetLong;

  //circumference in miles at equator, if you want km, use km value here
  double circ = EQUATER_LEN_NM;
  double lon = (pLongRad - pTargetLongRad);

  // simple absolute function
  if (lon < 0.0)
    lon = -1 * (lon);

  if (lon > rwmeter::PI)
  {
    lon = rwmeter::PI2 - lon;
  }

  angle = acos(sin(pTargetLatRad) * sin(pLatRad) + cos(pTargetLatRad) * cos(pLatRad) * cos(lon));
  return circ * angle / (rwmeter::PI2);

}

/* *************************************************************** */
double rwmeter::Utils::calcBearingBetween2Points(double gFromLat, double gFromLong, double gTargetLat, double gTargetLong)
{

  double /*slope_angle,*/ pLatRad, pLongRad, pTargetLatRad, pTargetLongRad;

  // convert lat/long to Radiance
  pLatRad = rwmeter::PI / 180 * gFromLat;
  pLongRad = rwmeter::PI / 180 * gFromLong;

  pTargetLatRad = rwmeter::PI / 180 * gTargetLat;
  pTargetLongRad = rwmeter::PI / 180 * gTargetLong;

  double tc1 = fmod(atan2(sin(pTargetLongRad - pLongRad)*cos(pTargetLatRad),
    cos(pLatRad)*sin(pTargetLatRad) - sin(pLatRad)*cos(pTargetLatRad)*cos(pTargetLongRad - pLongRad)),
    rwmeter::PI2);
  double val = RadToDeg * tc1;

  if (val < 0)
    val = 359 + val;

  return val;
}
/* *************************************************************** */
// provide two points and elevation between them. Function will retriev the slope slope_angle
double rwmeter::Utils::calcSlopeBetween2PointsWithGivenElevation(double gFromLat, double gFromLong, double gTargetLat, double gTargetLong, double relativeElevInFeet)
{
  // http://www.cplusplus.com/forum/beginner/90710/
  // https://www.geeksforgeeks.org/find-two-sides-right-angle-triangle/ 
  // http://www.analyzemath.com/Geometry_calculators/right_triangle_calculator.html (calculator)
  /*  
  	cout << "  |* ";
	cout << "\n  |   * ";
	cout << "\n  | b    *       C";
	cout << "\n  |         * ";
	cout << "\nA |            * ";
	cout << "\n  |               * ";
	cout << "\n  | 90           a   * ";
	cout << "\n  |_____________________* ";
	cout << "\n             B \n\n";  

  A^2 + B^2 = C^2 =
  A*A + B*B = C*C
  
  */

  double slope_angle_deg = 0.0f;
  double distanceIn_nm = calcDistanceBetween2Points_nm(gFromLat, gFromLong, gTargetLat, gTargetLong);
  double C = distanceIn_nm * nm2meter * meter2feet; // FEET_TO_NM; // convert to FEET // Represent B leg
  double A = relativeElevInFeet; // represents Elevation in feet
  double B = sqrt(C*C - A*A); // we need to find B


  double b = A / C;
  double a_rad = asin(b);
  slope_angle_deg = a_rad * 180.0 / rwmeter::PI;

  double a = B / C;
  double b_rad = asin(a);
  double slope_angle_b = b_rad * 180.0 / rwmeter::PI;

  

//  double tangA = 0.0f;
  //double radTangA = 0.0f; // in radians




  double tangA = A / B;
  double radTangA = atan(tangA);
  double slope_angle_tandA = radTangA * 180.0 / rwmeter::PI;


  


  return slope_angle_deg;
}

/* *************************************************************** */

float rwmeter::Utils::calcElevBetween2Points_withGivenAngle_InFeet(float distanceBetweenPointsInFeet, float slopeAngle)
{

  double radTangA = 0.0f; // in radians
  double sideA = 0.0f;
  double sideB = distanceBetweenPointsInFeet;

  radTangA = slopeAngle * rwmeter::PI / 180; // radians
  sideA = tan(radTangA) * sideB;


  return (float)sideA; // Elevation
}


/* *************************************************************** */
/**
* findCoordinateBasedOnDistanceAndAngle function return a Point based on location.
* inLat, inLon: plane latitude/longitude
* inHdg: the function angles are (counter clockwise): 0, 270, 180, 90
*        x-plane angles (clockwise): 0, 90, 180, 270
*        In order to compensate for the un-identical measures, we use 360 - Angle in order to be on par with X-Plane angles.
*        The Headings must be in PSI and not in magnetic PSI.
*        PSI: sim/flightmodel/position/psi
*/

void rwmeter::Utils::calcPointBasedOnDistanceAndBearing_2DPlane(double& outLat, double& outLon, double inLat, double inLon, float inHdg_deg, double inDistance_nm)
{
  double epsilon = 0.000001; // # threshold for floating-point equality
                             // Convert info to radians
  double rlat1 = DegToRad * inLat; //
  double rlon1 = DegToRad * inLon;
  float  rbearing = DegToRad * (360 - inHdg_deg); // Fix heading with X-Plane
  double distance = inDistance_nm / EARTH_AVG_RADIUS_NM; // # normalize linear distance to radian slope_angle

  double rlat = asin(sin(rlat1) * cos(distance) + cos(rlat1) * sin(distance) * cos(rbearing));
  double rlon = 0.0;

  if (cos(rlat) == 0 || fabs(cos(rlat)) < epsilon) //# Endpoint a pole
    rlon = rlon1;
  else
  {
    double dlon = atan2(sin(rbearing)*sin(distance)*cos(rlat1), cos(distance) - sin(rlat1)*sin(rlat));
    // TODO: in Linux should Link with -lm
    rlon = fmod(rlon1 - dlon + rwmeter::PI, rwmeter::PI2) - rwmeter::PI;
    //    rlon = fmod ( (rlon1 - asin( sin(rbearing)* sin(distance) / cos(rlat) ) + PI ) , 2*PI ) - PI ;
  }

  outLat = rlat * RadToDeg;
  outLon = rlon * RadToDeg;

}


/* *************************************************************** */
/* *************************************************************** */

std::vector<std::string> rwmeter::Utils::buildSentenceFromWords(std::vector<std::string> inTokens, size_t allowedSentenceLength, size_t maxLinesAllowed/*, size_t *outRealLinesCount*/)
{

  std::vector<std::string> vecSentence;
  std::vector<std::string>::iterator itToken;
  std::string sentence = "";
  size_t lineLen = 0;
  size_t lineCounter = 0;
  size_t realLineCount = 0;

  for (itToken = inTokens.begin(); itToken < inTokens.end() && lineCounter <= maxLinesAllowed; itToken++)
  {
    // check if new Token.length() + lineLen < sentenceLength ? sentance+=Token : start new line
    if (itToken->length() + lineLen <= allowedSentenceLength)
    {
      sentence = (sentence.length() == 0) ? *itToken : sentence.append(" ").append(*itToken);
      lineLen = sentence.length();
    }
    else {

      // add sentence to vector
      vecSentence.push_back(sentence);
      sentence.clear();
      lineCounter++;
      realLineCount++;
      lineLen = 0;

      sentence = *itToken;
      lineLen = sentence.length();

    } // end sentence length check
  } // end loop

    // handle remainig string
    // std::cout<< sentence.length() << std::endl;
  if (sentence.length() > 0 && sentence.length() <= allowedSentenceLength && lineCounter < maxLinesAllowed)
  {
    vecSentence.push_back(sentence);
    realLineCount++;
  }

  // v2.1.28.8
  //if (outRealLinesCount != nullptr)
  //  (*outRealLinesCount) = realLineCount;

  return vecSentence;

}
/* *************************************************************** */
/* *************************************************************** */

// split string by fetching last to characters from a string, and returning the split strings
bool rwmeter::Utils::extractUnitsFromString(std::string inNumWithUnits, std::string &outNumber, std::string &outUnit)
{
  bool success = true;

  outUnit.clear();
  outNumber.clear();

  std::string num = "";

  if (inNumWithUnits.length() < 3) // probably no units where defined
  {
    num = inNumWithUnits;
  }
  else // check units
  {
    std::string units = inNumWithUnits.substr(inNumWithUnits.length() - 2, 2); // get last 2 chars
    if (Utils::is_alpha(units))
    {
      outUnit = units;
      num = inNumWithUnits.substr(0, inNumWithUnits.length() - 2);
    }
    else // if untis are not set then assume only numbers
      num = inNumWithUnits;

  } // end check length


  if (Utils::is_number(num))
    outNumber = num;
  else
    success = false;

  return success;
}

/* *************************************************************** */
/* *************************************************************** */

std::vector<std::string> rwmeter::Utils::splitStringToNumberAndChars(std::string &inString, std::string delimeter)
{
  std::vector<std::string> vecResult;
  std::vector<std::string> vecStringSplitResult;
  vecResult.clear();
  vecStringSplitResult.clear();
  std::string numString; numString.clear();
  std::string stringAfterNum; stringAfterNum.clear();

  bool flag_startsAsNumber = false;
  int counter = 0;
  for (auto c: inString)
  {
    std::string s; s.clear();
    s += c;
    if (counter == 0)
    {
      // chek digit
      if (Utils::is_digits(s))
      {
        flag_startsAsNumber = true;
        numString.clear();
        numString.append(s);
      }
      else
      {
        vecResult.clear();
        break;
      }
    }
    else
    {
      // chek digit
      if (Utils::is_digits(s))
        numString.append(s);
      else
      {
        stringAfterNum = inString.substr((size_t)counter);
        vecStringSplitResult = Utils::splitString(stringAfterNum, delimeter);

        break;
      }
    }

    ++counter;
  }

  if (flag_startsAsNumber && !stringAfterNum.empty())
  {
    vecResult.push_back(numString);
    for (auto st : vecStringSplitResult)
      vecResult.push_back(st);
    //vecResult.push_back(stringAfterNum);
  }

  return vecResult;
}

std::vector<std::string> rwmeter::Utils::splitString(std::string inString, std::string delimateChars)
{
  std::vector<std::string> vecSplitValues;
  vecSplitValues.clear();

  Tokenizer tkn(inString);
  std::string delimeter = std::string(delimateChars);
  tkn.setDelimiter(delimeter);

  return tkn.split();;

}

/* *************************************************************** */

std::map<int, std::string> rwmeter::Utils::splitStringToMap(std::string inString, std::string delimateChars)
{
  int seq = 0;
  std::map<int, std::string> mapStrings;
  mapStrings.clear();

  std::vector<std::string> vecSplitValues = Utils::splitString(inString, delimateChars);
  for (auto val : vecSplitValues)
  {
    Utils::addElementToMap(mapStrings, seq, val);
    ++seq;
  }


  return mapStrings;
}

/* *************************************************************** */
std::list<std::string> rwmeter::Utils::splitStringUsingStringDelimiter(std::string & inString, std::string delimateChars)
{

  Tokenizer tkn(inString);
  std::string delimeter = std::string(delimateChars);
  tkn.setDelimiter(delimeter);

  return tkn.splitByStringDelimeter();
}


/* *************************************************************** */
// Extract base string from a string. Good to extract file name without the extension "[file].exe", for example.
std::string rwmeter::Utils::extractBaseFromString(std::string inFullFileName, std::string delimiter, std::string *outRestOfString)
{
  std::string result = "";
  size_t lengthString = inFullFileName.length();

  if (delimiter.empty() || lengthString == 1)
    return inFullFileName;

  char d = delimiter.front();
  size_t loc = inFullFileName.find_first_of(d);

  if (loc == string::npos) // did not find
  {
    if (outRestOfString != nullptr)
      outRestOfString->empty();

    return inFullFileName;
  }
  
  result = inFullFileName.substr(0, loc);
  if (outRestOfString != nullptr)
    (*outRestOfString) = inFullFileName.substr((loc + 1));



  return result;
}

/* *************************************************************** */
// Extract Last string from a string. Good to extract file name without the PATH, for example.
std::string rwmeter::Utils::extractLastFromString(std::string inFullFileName, std::string delimiter, std::string *outRestOfString)
{
  std::string result; result.clear();
  size_t lengthString = inFullFileName.length();

  if (delimiter.empty() || lengthString == 1)
    return inFullFileName;

  char d = delimiter.front();
  size_t loc = inFullFileName.find_last_of(d);

  if (loc == string::npos) // did not find
  {
    if (outRestOfString != nullptr)
      outRestOfString->empty();

    return inFullFileName;
  }

  
  result = inFullFileName.substr(loc+1);
  if (outRestOfString != nullptr)
    (*outRestOfString) = inFullFileName.substr(0, loc);



  return result;
}



std::string rwmeter::Utils::add_word_to_line(std::deque<std::string>& outList, std::string inCurrentLine_s, std::string inNewWord_s, int inMaxLineLength_i, bool flag_force_new_line)
{
  std::string newSentence = inCurrentLine_s;
  std::string newWord = inNewWord_s;
  auto lineLength = newSentence.length();
  size_t max_line_length = (size_t)inMaxLineLength_i;

  if (!newWord.empty() || flag_force_new_line)
  {
    // check length is valid
    size_t tmp_len = lineLength + newWord.length();

    if (tmp_len <= max_line_length)
    {
      newSentence += (newSentence.empty()) ? newWord : std::string(" ") + newWord;

      if (flag_force_new_line)
      {
        outList.push_back(newSentence); // add current good sentence length
        newSentence.clear();
      }

    } // end if sentence is in good length
    else if (tmp_len > max_line_length)
    {
      if (!newSentence.empty())
      {
        outList.push_back(newSentence); // add current good sentence length
        newSentence.clear();
      }


      tmp_len =newWord.length(); // check if new word is also too long
      auto devide_i = (size_t)(tmp_len / max_line_length);
      auto mod_i = (size_t)(tmp_len % max_line_length);

      for (size_t i1 = 0; i1 < devide_i; ++i1) // add parts of the work as a sentence
      {
        size_t from_st = i1 * max_line_length;
        newSentence = newWord.substr(from_st, max_line_length);
        outList.push_back(newSentence);
        newSentence.clear();
      }

      // std::cout << "mod: " << devide_i * max_line_length << std::endl;
      if (mod_i > 0)
      {
        newSentence = newWord.substr(devide_i * max_line_length); // start new sentence.
      }

    } // end if temp_len is too long or at correct length

    newWord.clear();
  } // end add newWord 

  return newSentence;
}



std::vector<std::string> rwmeter::Utils::sentenceTokenizerWithBoundaries(std::string inString, std::string delimiterChar, size_t width, std::string in_special_LF_char)
{
  std::deque<std::string> dequeSentences;

  // if width = 0 then immediate return vecWord. For backword compatibility with old code, if any.
  if (width == 0)
  {
    std::vector<std::string> vecWords;
    Tokenizer str(inString);
    str.setDelimiter(delimiterChar);
    vecWords = str.split();
    return vecWords; // no need to create sentence
  }

  Utils::sentenceTokenizerWithBoundaries(dequeSentences, inString, delimiterChar, width, in_special_LF_char); // main function to build the sentences, using deque instead vector, mainly for memory management benefits
    
  std::vector<std::string> vecSentence({ dequeSentences.begin(), dequeSentences.end() }); // converts deque to vector
 
  return vecSentence;
}


void rwmeter::Utils::sentenceTokenizerWithBoundaries(std::deque<std::string> &outDeque, std::string &inString, std::string delimiterChar, size_t width, std::string in_special_LF_char)
{

  char delimiter = (delimiterChar.empty()) ? ' ' : delimiterChar.front();

  std::string xWord = inString;


  std::string inSpecialChar = in_special_LF_char;
  auto lineLength = (size_t)0;
  auto max_line_length = width;

  std::string eol_found;

  std::string newSentence;
  std::string newWord;
  size_t charPos_i = -1;
  size_t skip_chars_i = 0;

  bool flag_skip = false;

  charPos_i = -1;
  auto pos_winEol = xWord.find(rwmeter::WIN_EOL.c_str());
  auto pos_unixEol = xWord.find(rwmeter::UNIX_EOL.c_str());
  auto pos_specialEol = xWord.find(inSpecialChar);
  auto lenWord_i = xWord.length();
  eol_found.clear();


  for (auto chr : xWord)
  {
    ++charPos_i; // current char position

    // check if we need to skip chars, in case of: "\n" or "\r\n"
    if (!flag_skip)
    {
      // handle Win EOL "\r\n"
      if (charPos_i == pos_winEol)
      {
        flag_skip = true;
        eol_found = rwmeter::WIN_EOL;
        skip_chars_i = eol_found.length(); // We just need one more character to skip. calculate how many chars to skip

        pos_winEol = xWord.find(rwmeter::WIN_EOL.c_str(), charPos_i + skip_chars_i);
        pos_unixEol = xWord.find(rwmeter::UNIX_EOL.c_str(), charPos_i + skip_chars_i);

      }
      else
        // handle Unix EOL "\r"
        if (charPos_i == pos_unixEol)
        {
          eol_found = rwmeter::UNIX_EOL;
          skip_chars_i = eol_found.length(); // - 1; // calculate how many chars to skip
          flag_skip = true;

          pos_winEol = xWord.find(rwmeter::WIN_EOL.c_str(), charPos_i + skip_chars_i);
          pos_unixEol = xWord.find(rwmeter::UNIX_EOL.c_str(), charPos_i + skip_chars_i);

        }
        else
          // handle Special EOL ";"
          if (charPos_i == pos_specialEol)
          {
            eol_found = inSpecialChar;
            skip_chars_i = eol_found.length(); // calculate how many chars to skip
            flag_skip = true;

            pos_specialEol = xWord.find(inSpecialChar, charPos_i + skip_chars_i);
          }

    }


    // check if we need to skip chars, in case of: "\n" or "\r\n"
    if (flag_skip)
    {
      --skip_chars_i;

      if (skip_chars_i <= 0)
      {
        flag_skip = false; // reset skip flag

        // add newWord to sentence and add sentence to vector. reset newWord and sentence
        newSentence = Utils::add_word_to_line(outDeque, newSentence, newWord, (int)max_line_length, true);

        newWord.clear();
        //newSentence.clear(); // bug: we should not delete the "sentence" that was returned from Utils::add_word_to_line(). Reason, long lines fail to display the "rest" of the line that was left out after substringing the line.
        lineLength = newSentence.length();

      }

    }
    else //if (!flag_skip /*&& char !='\r' && char != '\n'*/)
    {
      if (chr == delimiter)
      {
        newSentence = add_word_to_line(outDeque, newSentence, newWord, (int)max_line_length);
        newWord.clear();
        lineLength = newSentence.length();
      }
      else
        newWord += chr; // add characters to string

    }


  } // end word chars loop

  if (!newWord.empty())
    newSentence = Utils::add_word_to_line(outDeque, newSentence, newWord, (int)max_line_length);


  newWord.clear();
  lineLength = (int)newSentence.length();

  // Add last word if has value in it
  if (!newSentence.empty())
    outDeque.push_back(newSentence);


}

  /* *************************************************************** */

std::vector<std::string> rwmeter::Utils::tokenizer(std::string inString, char delimateChar, size_t width)
{
  size_t sourceLen = 0;
  size_t strLastPos = 0;
  size_t lastNewLinePos = inString.npos;
  size_t strLeftLength = sourceLen;
  size_t widthToCut = 0;
  size_t spacePos = 0;
  int debugCounter = 0;
  std::string SPACE = " ";

  sourceLen = inString.length();
  //char chr = '\0';
  //int loc = 0;
  std::vector<std::string> vecWord;

  vecWord.clear();

  // *************
  while (strLastPos < sourceLen && debugCounter < 500)
  {
    debugCounter++;
    // find first ";" (UNIX_EOL) in this widget for new line
    lastNewLinePos = inString.find(delimateChar, ((lastNewLinePos == inString.npos) ? 0 : strLastPos + 1));

    if (lastNewLinePos != inString.npos && ((lastNewLinePos - strLastPos) <= width)) // if found ' ' before "width"
    {
      widthToCut = lastNewLinePos - strLastPos;
      vecWord.push_back(ltrim(inString.substr(strLastPos, widthToCut)));

      strLeftLength -= widthToCut; // decrease left length
      strLastPos += widthToCut;

      // if next "delimateChar" is greater then "width", add ' '
    }
    else if ((lastNewLinePos != inString.npos && lastNewLinePos > width)
      || lastNewLinePos == inString.npos)
    {
      // make sure lastNewLinePos > -1 ( Avoid Out of range error )
      lastNewLinePos = (lastNewLinePos == inString.npos) ? 0 : lastNewLinePos;

      widthToCut = width;
      if (inString.at(lastNewLinePos) != ' ')
      {
        // calculate new lastNewLinePos to point to SPACE if possible
        spacePos = inString.find_last_of(' ', (strLastPos + width));

        if (spacePos != inString.npos && spacePos > strLastPos)
          widthToCut = spacePos - strLastPos; // change the width to cut
      }

      vecWord.push_back(ltrim(inString.substr(strLastPos, widthToCut)));

      strLastPos += widthToCut;
      strLeftLength -= widthToCut;

    } // if lastNLPos > width


  } // end while


  return vecWord;

} // end tokenizer



  /* *************************************************************** */
  /* *************************************************************** */
  /* *************************************************************** */


double rwmeter::Utils::lRound(double x) {

  double intPart;

  if (modf(x, &intPart) >= .5)
    return (x >= 0) ? ceil(x) : floor(x);

  return (x<0) ? ceil(x) : floor(x);
}

/* *************************************************************** */

double rwmeter::Utils::pathRound(double x, int inBase)
{
  double result = 0.0f;

  switch (inBase)
  {
  case 0:
  {
    //result = x;
    result = floor(x * 1000 + 0.5) / 1000;
  }
  break;
  case 1:
  {
    //floor(floatValue*100+0.5)/100;
    result = floor(x + 0.5);
  }
  break;
  case 5:
  {
    int iX = (int)x;
    int div10 = iX % 10;

    if (div10 > (inBase + 2))
      result = (float)(iX - div10 + 10);
    else if (div10 < (inBase - 2))
      result = (float)(iX - div10);
    else
      result = (float)(iX - div10 + inBase);

    //      if ( x >= 10)
    //        result =  floor( x/10 + 0.5 ) *10 + inBase;
    //      else
    //        result = inBase;
  }
  break;
  case 10:
  default:
  {
    inBase = 10;
    result = (x >= 10.0) ? lRound(x / 10) * 10 : lRound(x);
  }
  break;
  } // end switch

  return result;
}

/* *************************************************************** */

float rwmeter::Utils::calc_heading_base_on_plane_move(double &inCurrentLat, double &inCurrentLong, double &inTargetLat, double &inTargetLong, int &inTargetHeading)
{
  // decide if need to fix heading
  // if Long was [+] and new Long is [-] than modify heading by "+90" relative to the horizon
  static const int degreeseInCircle = 360;

  int heading = inTargetHeading;

  if (inCurrentLong >= 0 && inTargetLong < 0) // +90
  {
    heading += 90;
    if (heading > degreeseInCircle)
      heading -= degreeseInCircle;
  }
  else if (inCurrentLong < 0 && inTargetLong >= 0)
  {
    heading -= 90;
    if (heading <= 0)
      heading += degreeseInCircle;
  }


  return (float)heading;
} // calc_heading_base_on_plane_move

/* *************************************************************** */
//
//double rwmeter::Utils::readNumericAttrib(ITCXMLNode &node, string attribName, double defaultValue)
//{
//  double returnValue = defaultValue; // init with default value
//
//  std::string attribValue;
//  attribValue.clear();
//
//  attribName = mxUtils::stringToLower(attribName); // v3.0.194 always convert attribute "name" to lower case
//
//  char exists = node.isAttributeSet(attribName.c_str());
//
//  if (exists)
//  {
//    attribValue = node.getAttribute(attribName.c_str());
//    attribValue = Utils::trim(attribValue); // always trim numeric values from spaces
//
//    if (!Utils::trim(attribValue).empty())
//    {
//      // check if value is a numeric
//      if (Utils::is_number(attribValue))
//      {
//        returnValue = Utils::stringToNumber<double>(attribValue);
//      }
//    }
//  }
//
//  return returnValue; // calling function will have to cast to the correct numeric type (int/float...)
//}
//
//double rwmeter::Utils::readNumericAttrib(ITCXMLNode& node, string attribOptionName1, std::string attribOptionName2, double defaultValue)
//{
//  double returnValue = defaultValue; // init with default value
//
//  std::string attribNameToUse;  // v3.0.221.15rc5
//  std::string attribValue;
//
//  attribNameToUse.clear(); // v3.0.221.15rc5
//  attribValue.clear();
//
//  attribOptionName1 = mxUtils::stringToLower(attribOptionName1); // v3.0.194 always convert attribute "name" to lower case
//  attribOptionName2 = mxUtils::stringToLower(attribOptionName2); // v3.0.221.15rc5 always convert attribute "name" to lower case
//
//  char exists = node.isAttributeSet(attribOptionName1.c_str());
//  char exists2 = node.isAttributeSet(attribOptionName2.c_str());
//
//  if (exists)
//    attribNameToUse = attribOptionName1;
//  else if (exists2)
//    attribNameToUse = attribOptionName2;
//
//
//  if (attribNameToUse.empty()) // if one of the attributes exists
//  {
//    returnValue = defaultValue;
//  }
//  else
//  {
//    attribValue = (node.getAttribute(attribNameToUse.c_str()));
//    if (Utils::trim(attribValue).empty() || !(Utils::is_number(attribValue)))
//    {
//      returnValue = defaultValue;
//    }
//    else
//    {
//      returnValue = Utils::stringToNumber<double>(attribValue, (int)(attribValue.length()));
//    }
//  }
//
//  return returnValue; // calling function will have to cast to the correct numeric type (int/float...)
//}
//
///* *************************************************************** */
//bool rwmeter::Utils::readBoolAttrib(ITCXMLNode &node, string attribName, bool defaultValue)
//{
//  bool returnValue = defaultValue; // init with default value
//
//  std::string attribValue;
//  attribValue.clear();
//
//  attribName = mxUtils::stringToLower(attribName); // v3.0.194 always convert attribute "name" to lower case
//
//  char exists = node.isAttributeSet(attribName.c_str());
//
//  if (exists)
//  {
//    attribValue = node.getAttribute(attribName.c_str());
//    attribValue = Utils::trim(attribValue); // always trim bool values from spaces
//
//    if (!Utils::trim(attribValue).empty())
//    {
//      // check if value is a boolean      
//      rwmeter::mxUtils::isStringBool(attribValue, returnValue); // will check and assign the bool value
//      
//    }
//  }
//
//  return returnValue; 
//}
//
///* *************************************************************** */
//
//string rwmeter::Utils::readAttrib(ITCXMLNode &node, string attribName, string defaultValue, bool needStringTrim)
//{
//  std::string attribValue;
//  attribValue.clear();
//
//  attribName = mxUtils::stringToLower(attribName); // v3.0.194 always convert attribute "name" to lower case
//
//  char exists = node.isAttributeSet(attribName.c_str());
//
//  if (exists)
//  {
//    attribValue = (node.getAttribute(attribName.c_str()));
//    if (Utils::trim(attribValue).empty())
//    {
//      attribValue = defaultValue;
//    }
//  }
//  else
//  {
//    attribValue = defaultValue;
//  }
//
//  return (needStringTrim) ? Utils::trim(attribValue) : attribValue;
//}

/* *************************************************************** */
//
//string rwmeter::Utils::readAttrib(ITCXMLNode& node, string attribOptionName1, string attribOptionName2, string defaultValue, bool needStringTrim)
//{
//  std::string attribNameToUse;  // v3.0.221.15rc5
//  std::string attribValue;
//
//  attribNameToUse.clear(); // v3.0.221.15rc5
//  attribValue.clear();
//
//  attribOptionName1 = mxUtils::stringToLower(attribOptionName1); // v3.0.194 always convert attribute "name" to lower case
//  attribOptionName2 = mxUtils::stringToLower(attribOptionName2); // v3.0.221.15rc5 always convert attribute "name" to lower case
//
//  char exists = node.isAttributeSet(attribOptionName1.c_str());
//  char exists2 = node.isAttributeSet(attribOptionName2.c_str());
//
//  if (exists)
//    attribNameToUse = attribOptionName1;
//  else if (exists2)
//    attribNameToUse = attribOptionName2;
//
//
//  if (!attribNameToUse.empty()) // if one of the attributes exists
//  {
//    attribValue = (node.getAttribute(attribNameToUse.c_str()));
//    if (Utils::trim(attribValue).empty())
//    {
//      attribValue = defaultValue;
//    }
//  }
//  else
//  {
//    attribValue = defaultValue;
//  }
//
//  return (needStringTrim) ? Utils::trim(attribValue) : attribValue;
//}
//
//
///* *************************************************************** */
//
//void rwmeter::Utils::xml_transform_element_attributes_to_string_properties(IXMLNode & xNode, rwmeter::mxProperties & outAttributes)
//{
//
//  std::string attrib_name;
//  std::string attrib_value;
//
//  int nAttribs = xNode.nAttribute();
//  for (int i = 0; i < nAttribs; ++i)
//  {
//    attrib_name.clear();
//    attrib_value.clear();
//
//    attrib_name = xNode.getAttributeName(i);
//    attrib_value = xNode.getAttributeValue(i);
//
//    if (attrib_name.empty())
//      continue;
//
//    outAttributes.setStringProperty(attrib_name, attrib_value);
//  } // END loop over attributes
//
//  // read clear element
//  
//  //(xNode.nClear() > 0) ? xNode.getClear().sValue : rwmeter::EMPTY_STRING; // <![CDATA[ some text ]]>
//
//  std::string cdata_string = rwmeter::EMPTY_STRING;
//  std::string cdata_open_string = rwmeter::EMPTY_STRING;
//  int nClearElements = xNode.nClear();
//    
//  for (int i1 = 0; i1 < nClearElements; ++i1)
//  {
//    cdata_open_string = xNode.getClear().sOpenTag;
//    cdata_string = xNode.getClear().sValue;
//    if (rwmeter::ATTRIB_OPEN_CDATA.compare(cdata_open_string) == 0) // if we find a "clear()" that starts with CDATA then we store its content. That way we skip comments
//    {
//      outAttributes.setStringProperty(rwmeter::ELEMENT_CDATA, cdata_string);
//      break; // exit loop
//    }
//  }
//
//
//} // xml_transform_element_attributes_to_string_properties
//
///* *************************************************************** */
//
//void rwmeter::Utils::set_properties_based_on_source_and_property_list(std::map<std::string, int>& inListAttributes, rwmeter::mxProperties & inSourceValues, rwmeter::mxProperties & outAttributes)
//{
//  // loop over "inListAttributes" map
//  // get value from source and apply into outAttributes
//  std::string errMsg;
//  errMsg.clear();
//
//  for (auto lis : inListAttributes) // first=key, second = type (int)
//  {
//    std::string key = lis.first;
//
//    if (inSourceValues.hasProperty(key))
//    {
//      std::string sourceValue = inSourceValues.getPropertyValue_asString(key, errMsg);
//#if defined DEBUG || defined _DEBUG
//      if (!errMsg.empty())
//        Log::logMsgErr("[set_properties_based_on_source_and_property_list] " + errMsg);
//#endif
//      outAttributes.setPropertyByTypeNum(key, sourceValue, lis.second);
//    }
//
//  }
//}
//
///* *************************************************************** */
//
///* *************************************************************** */
//
//bool rwmeter::Utils::xml_search_and_set_attribute_in_IXMLNode(IXMLNode & inNode, std::string inAttribName, std::string attribValue, std::string inModifyByElementName)
//{
//  bool flag_same_name_for_element_and_search_name = false; // This parameter is relevant only when inElementName has value
//
//  if (!inNode.isEmpty() && !inAttribName.empty())
//  {
//    std::string currentElementTagName = inNode.getName();
//    if (currentElementTagName.compare(inModifyByElementName) == 0)
//      flag_same_name_for_element_and_search_name = true;
//
//    // check all attributes in current Node level. If no attribute found then recurse to lower level
//    int atCounter = inNode.nAttribute();
//    bool flag_canChange = false; // will be used after attribute loop.
//    for (int i1 = 0; i1 < atCounter; ++i1) // loop over all attributes of current Node
//    {      
//      std::string attribName = inNode.getAttributeName(i1);
//      bool flag_attribFound = (inAttribName.compare(attribName) == 0);
//
//      // decide if we can set the attribute we found. 
//      if (flag_attribFound && inModifyByElementName.empty())
//        flag_canChange = true;
//      else if (flag_attribFound && !inModifyByElementName.empty() && flag_same_name_for_element_and_search_name)
//        flag_canChange = true;
//
//      if (flag_canChange)
//      {
//        //inNode.deleteAttribute(i1);
//        inNode.updateAttribute( attribValue.c_str(), inAttribName.c_str(), i1);
//        return true;
//      }
//    }
//
//    // *********************************************
//    // SPECIAL CASE:
//    // Did not find attribute but on same element as the search one. In this case we will create the attribute
//    if (!flag_canChange && flag_same_name_for_element_and_search_name)
//    {
//      inNode.addAttribute(inAttribName.c_str(), attribValue.c_str());
//      return true;
//    }    
//    //// END Special Case **************************
//
//    // if we reach here, it means we did not found the attribute. Loop over all Node children and recurse 
//    int nChilds = inNode.nChildNode();
//    for (int i1 = 0; i1 < nChilds; ++i1)
//    {
//      IXMLNode cNode = inNode.getChildNode(i1);
//      if (Utils::xml_search_and_set_attribute_in_IXMLNode(cNode, inAttribName, attribValue, inModifyByElementName))
//        return true;
//    }
//
//  } // check if node is valid
//
//
//  return false;
//}
//
/* *************************************************************** */
//
//bool rwmeter::Utils::xml_add_node_to_element_IXMLNode(IXMLNode & rootNode, IXMLNode & inNodeToAdd, std::string nameOfParentElementToAddTo )
//{
//  IXMLNode dummy;
//
//  if (nameOfParentElementToAddTo.empty())
//  {
//    dummy = rootNode.addChild(inNodeToAdd);
//    if (dummy.isEmpty())
//      return false;
//
//    return true;
//  }
//
//  std::string tagName = rootNode.getName();
//  if (tagName.compare(nameOfParentElementToAddTo) == 0)
//  {
//    dummy = rootNode.addChild(inNodeToAdd);
//    if (dummy.isEmpty())
//      return false;
//
//    return true;
//  }
//
//  int nChilds = rootNode.nChildNode();
//  for (int i1 = 0; i1 < nChilds; ++i1)
//  {
//    IXMLNode cNode = rootNode.getChildNode(i1);
//    if (xml_add_node_to_element_IXMLNode(cNode, inNodeToAdd, nameOfParentElementToAddTo))
//      return true;
//  }
//    
//   return false;
//}
//
///* *************************************************************** */
//
//IXMLNode rwmeter::Utils::xml_get_node_randomly_by_name_IXMLNode(IXMLNode & rootNode, std::string inTagToSearch, std::string & outErr, bool flag_removePicked /*default false*/)
//{
//  outErr.empty();
//  IXMLNode result = IXMLNode().emptyIXMLNode;
//  
//  int nChilds = rootNode.nChildNode(inTagToSearch.c_str());
//  if (nChilds > 0)
//  {
//    int r = Utils::getRandomIntNumber(0, nChilds - 1);
//    result = rootNode.getChildNode(inTagToSearch.c_str(), r);
//  }
//  else
//  {
//    outErr = "[utils random pick] Failed to find tag: <" + inTagToSearch + "> to pick from root node: " + rootNode.getName() + ". Check your XML file.";
//  }
//
//  if (flag_removePicked)
//    return result; // this is a pointer of the node, if we add the node to other element it will ditach it from original node.
//
//  return result.deepCopy();
//}
//
//IXMLNode rwmeter::Utils::xml_get_node_randomly_by_name_and_distance_IXMLNode(IXMLNode & rootNode, std::string inTagToSearch, double inLat, double inLon, std::string & outErr, double inMinDistance, double inMaxDistance, bool flag_removePicked)
//{
//  outErr.empty();
//  std::vector<IXMLNode> vecFilteredResults;
//  IXMLNode result = IXMLNode().emptyIXMLNode;
//  IXMLNode closestResult_ptr = IXMLNode().emptyIXMLNode;
//  double prevDistance = 0.0;
//  
//  vecFilteredResults.clear();
//
//  // if min/max defined
//  if (inMinDistance < 0.0 || inMinDistance == inMaxDistance)
//    inMinDistance = 0.0;
//
//  if (inMaxDistance < 0.0)
//    inMaxDistance = 0.0;
// 
//  if (inMinDistance > inMaxDistance && inMaxDistance > 0.0)
//  {
//    double tmp = inMaxDistance;
//    inMaxDistance = inMinDistance;
//    inMinDistance = tmp;
//  }
//
//
//  // if no distance restrictions
//  if (inMinDistance == 0.0 && inMaxDistance == 0.0)
//  {
//    result = Utils::xml_get_node_randomly_by_name_IXMLNode(rootNode, inTagToSearch, outErr, flag_removePicked);
//    return result;
//  }
//
//  /// filter points 
//  double lat_d = 0.0, lon_d = 0.0;
//  int nChilds = rootNode.nChildNode(inTagToSearch.c_str());
//  for (int i1 = 0; i1 < nChilds; ++i1)
//  {
//    IXMLNode cNode = rootNode.getChildNode(i1);
//    if (cNode.isEmpty())
//      continue;
//
//    lat_d = Utils::readNumericAttrib(cNode, rwmeter::ATTRIB_LAT, 0.0);
//    lon_d = Utils::readNumericAttrib(cNode, rwmeter::ATTRIB_LONG, 0.0);
//    
//    double distance = Utils::calcDistanceBetween2Points_nm(inLat, inLon, lat_d, lon_d);
//
//    // store closest result even if not in restricted distance
//    if (i1 == 0)
//    {
//      closestResult_ptr = cNode;
//      prevDistance = distance;
//    }
//    else if (distance < prevDistance)
//    {
//      prevDistance = distance;
//      closestResult_ptr = cNode;
//    }
//
//    if (distance >= inMinDistance && distance <= inMaxDistance || (distance >= inMinDistance && inMaxDistance == 0.0))
//      vecFilteredResults.push_back(cNode);
//  }
//
//#if defined DEBUG || defined _DEBUG
//  Log::logAttention("\t[get node randomly] after filtering distance.");
//#endif
//
//  int vecSize = (int)vecFilteredResults.size();
//  if (vecSize == 0)
//    result = closestResult_ptr;
//  else
//  {
//    int random_i = Utils::getRandomIntNumber(0, vecSize - 1);
//
//#if defined DEBUG || defined _DEBUG
//    Log::logAttention("\t[get node randomly] Pick index: " + Utils::formatNumber<int>(random_i));
//#endif
//    if (random_i >= vecSize)
//      random_i = vecSize - 1;
//
//    result = vecFilteredResults.at(random_i);
//  }
//
//  if (flag_removePicked)
//    return result; // this is a pointer of the node, if we add the node to other element it will ditach it from original node.
//
//  if (result.isEmpty())
//    return IXMLNode().emptyIXMLNode;
//
//  return result.deepCopy();
//}
//
///* *************************************************************** */
//
//void rwmeter::Utils::xml_delete_empty_nodes(IXMLNode & Node/*, std::deque<IXMLNode *> & nodesToDelete*/)
//{
//  /****
//  Function searches for <point> elements and check their validity.
//  If lat or long attributes are empty, the are flagges as 
//  ****/
//  std::string err;
//  err.clear();
//
//  if (Node.isEmpty())
//    return;
//
//  if (ELEMENT_POINT.compare(Node.getName()) == 0)
//  {
//    rwmeter::mxProperties prop;
//    Utils::xml_transform_element_attributes_to_string_properties(Node, prop);
//    std::string lat = prop.getPropertyValue(rwmeter::ATTRIB_LAT, err);
//    std::string lon = prop.getPropertyValue(rwmeter::ATTRIB_LONG, err);
//    if (lat.empty() || lon.empty())
//      Node.deleteNodeContent(); // DELETE Node
//
//  } // if in point
//  else if (ELEMENT_ITEM.compare(Node.getName()) == 0)
//  {
//    rwmeter::mxProperties prop;
//    Utils::xml_transform_element_attributes_to_string_properties(Node, prop);
//    std::string barcode = prop.getPropertyValue(rwmeter::ATTRIB_BARCODE, err);
//    if (barcode.empty())
//      Node.deleteNodeContent(); // DELETE Node
//  }
//  else
//  {
//    // Drill Down
//    int nChilds = Node.nChildNode();
//    for (int i1 = 0; i1 < nChilds; ++i1)
//    {
//      IXMLNode cNode = Node.getChildNode(i1);
//      Utils::xml_delete_empty_nodes(cNode);
//    }
//  }
//
//} // xml_delete_empty_point_nodes
//
//std::string rwmeter::Utils::xml_get_attribute_value_drill(IXMLNode & node, std::string inAttribName, bool &flag_found, std::string inTagName)
//{
//  flag_found = false;
//  /****
//  Function searches for specific attribute value, eaither in specific element or the first it finds.
//  ****/
//  std::string errMsg;
//  errMsg.clear();
//
//  if (node.isEmpty())
//    return rwmeter::EMPTY_STRING;
//
//  bool flag_canSearch = false;
//  if (inTagName.empty())
//    flag_canSearch = true;
//  else
//  {
//    std::string tagName = node.getName();
//    flag_canSearch = (inTagName.compare(tagName) == 0);
//  }
//
//  if (flag_canSearch)
//  {
//    rwmeter::mxProperties prop;
//    Utils::xml_transform_element_attributes_to_string_properties(node, prop);
//    if (prop.hasProperty(inAttribName))
//    {
//      flag_found = true;
//      return prop.getPropertyValue_asString(inAttribName, errMsg);
//    }
//      
//
//  }
//  else
//  {
//    // Drill Down
//    int nChilds = node.nChildNode();
//    for (int i1 = 0; i1 < nChilds; ++i1)
//    {
//      IXMLNode cNode = node.getChildNode(i1);
//      std::string val = Utils::xml_get_attribute_value_drill(cNode, inAttribName, flag_found, inTagName);
//      if (flag_found)
//        return val;
//    }
//  }
//
//  return rwmeter::EMPTY_STRING;
//}
//
//bool rwmeter::Utils::xml_copy_node_attributes(IXMLNode & sNode, IXMLNode & tNode, bool flag_includeClearData)
//{
//
//  if (sNode.isEmpty())
//    return false;
//
//  if (tNode.isEmpty())
//    tNode = IXMLNode();
//
//  int nAttribs = sNode.nAttribute();
//  for (int i1 = 0; i1 < nAttribs; ++i1)
//  {
//    IXMLAttr attrib = sNode.getAttribute(i1);
//    tNode.addAttribute(attrib.sName, attrib.sValue);
//  }
//
//  if (flag_includeClearData)
//  {
//    int nClear = sNode.nClear();
//    for (int i1 = 0; i1 < nClear; ++i1)
//    {
//      IXMLClear c = sNode.getClear(i1);
//      tNode.addClear(c.sValue, c.sOpenTag, c.sCloseTag);
//    }
//  } // end add clear
//
//  return true;
//}
//
///* *************************************************************** */
//
//IXMLNode rwmeter::Utils::xml_get_node_from_node_tree_IXMLNode(IXMLNode & pNode, std::string inSearchedElementName, bool flag_returnCopy)
//{
//  IXMLNode node = IXMLNode();
//
//  // check root node is valid
//  if (pNode.isEmpty())
//    return IXMLNode().emptyIXMLNode;
//
//  // Check if child node exists
//  node = pNode.getChildNode(inSearchedElementName.c_str());
//  if (!node.isEmpty())
//  {
//    if (flag_returnCopy)
//      return node.deepCopy();
//    else
//      return node;
//  }
//    
//
//
//  // search in child nodes recursivly 
//  int nChilds = pNode.nChildNode();
//  for (int i1 = 0; i1 < nChilds; ++i1)
//  {
//    IXMLNode cNode = pNode.getChildNode(i1);
//    node = Utils::xml_get_node_from_node_tree_IXMLNode(cNode, inSearchedElementName, flag_returnCopy);
//    if (!node.isEmpty())
//    {
//      if (flag_returnCopy)
//        return node.deepCopy();
//      else
//        return node;
//    }
//  }
//
//  return node.deepCopy(); // return empty node
//}
//
//IXMLNode rwmeter::Utils::xml_get_node_from_node_tree_by_attrib_name_and_value_IXMLNode(IXMLNode &pNode, const std::string inSearchedElementName, const std::string inAttribName, std::string inAttribValue, bool flag_returnCopy)
//{
//  //IXMLNode node;
//  bool flag_found = false;
//  
//  // check root node is valid
//  if (pNode.isEmpty())
//    return IXMLNode().emptyIXMLNode;
//
//  // Check if parent node itself is the correct node
//  // check attributes
//  std::string attrib_value = Utils::xml_get_attribute_value(pNode, inAttribName, flag_found);
//  if (flag_found)
//  {
//    if (inAttribValue.compare(attrib_value) == 0)
//    {
//      if (flag_returnCopy)
//        return pNode.deepCopy();
//      else
//        return pNode;
//    }
//  }
//
//
//  //node = pNode.getChildNode(inSearchedElementName.c_str());
//  //if (!node.isEmpty())
//  //{
//  //  // check attributes
//  //  std::string attrib_value = Utils::xml_get_attribute_value(node, inAttribName, flag_found);
//  //  if (flag_found)
//  //  {
//  //    if (inAttribValue.compare(attrib_value) == 0)
//  //    {
//  //      if (flag_returnCopy)
//  //        return node.deepCopy();
//  //      else
//  //        return node;
//  //    }
//  //  }
//  //} // end if node element is valid
//
//
//
//  // search in child nodes recursivly 
//  int nChilds = pNode.nChildNode();
//  for (int i1 = 0; i1 < nChilds; ++i1)
//  {
//    IXMLNode cNode = pNode.getChildNode(i1);
//    IXMLNode node = Utils::xml_get_node_from_node_tree_by_attrib_name_and_value_IXMLNode(cNode, inSearchedElementName, inAttribName, inAttribValue, flag_returnCopy);
//    if (!node.isEmpty())
//        return node;
//  }
//
//
//  return IXMLNode();
//}
//
//std::string rwmeter::Utils::xml_get_attribute_value(IXMLNode & pNode, std::string attribName, bool & flag_found)
//{
//  flag_found = false;
//  std::string value; value.clear();
//
//  int nAttrib = pNode.nAttribute();
//  for (int i1 = 0; i1 < nAttrib; ++i1)
//  {
//    IXMLAttr attrib = pNode.getAttribute(i1);
//    if (attribName.compare(attrib.sName) == 0)
//    {
//      flag_found = true;
//      value = attrib.sValue;
//
//      return value;
//    }
//  }
//
//
//  return value;
//}
//
//int rwmeter::Utils::xml_find_node_location(IXMLNode & pNode, std::string tagNameToSearch)
//{
//  int nChilds = pNode.nChildNode();
//  for (int i1 = 0; i1 < nChilds; ++i1)
//  {
//    IXMLNode cNode = pNode.getChildNode(i1);
//    if (cNode.isEmpty())
//    {
//      std::string name = cNode.getName();
//      if (tagNameToSearch.compare(tagNameToSearch) == 0)
//        return i1;
//    }
//  }
//
//  return 0;
//}

/* *************************************************************** */
//
//bool rwmeter::Utils::xml_add_cdata(IXMLNode & node, std::string cdataString)
//{
//  if (node.isEmpty())
//    return false;
//
//  /// v3.0.219.13 delete all clear elements before adding CDATA
//  int nClear = node.nClear();
//  for (int i1=0; i1<nClear; ++i1)
//    node.deleteClear(i1);
//
//  IXMLClear *c = node.addClear(cdataString.c_str()); // , "<![CDATA[", "]]>");
//  return true;
//}
//
//
///* *************************************************************** */
//int rwmeter::Utils::getRandomIntNumber(int inMin, int inMax)
//{
//  // Seed with a real random value, if available
//  std::random_device rd;
//  std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
//  std::uniform_int_distribution<> dis(inMin, inMax);
//
//  return dis(gen);
//}
//
//void rwmeter::Utils::xml_delete_attribute(IXMLNode & node, std::set<std::string>& inSetAttributes, std::string inTagName)
//{
//
//  if (node.isEmpty())
//    return;
//
//  std::string tagNameToSearch = inTagName;
//  bool flag_canSearchAndDelete = true;
//  std::string tagName = node.getName();
//
//  if (inTagName.empty())
//  {
//    tagNameToSearch = tagName;
//    flag_canSearchAndDelete = true;
//  }
//    
//  else if (inTagName.compare(tagName) == 0)
//    flag_canSearchAndDelete = true;
//
//  // Can we conduct search and delete ?
//  if (flag_canSearchAndDelete)
//  {
//    std::multimap <std::string, int> multiDelAttribList;
//    multiDelAttribList.clear();
//    bool hasMore = true;
//    int counter = 0;
//    int nAttribs = node.nAttribute();
//
//    while (hasMore && counter < nAttribs)
//    {
//      std::string name = node.getAttributeName(counter);
//      if (inSetAttributes.find(name) != inSetAttributes.end())
//      {
//        
//        node.deleteAttribute(name.c_str());
//        // do not change counter location since the stack decreased in size
//      }
//      else
//        ++counter;
//
//      nAttribs = node.nAttribute(); // This is a MUST. we need to update attribute counter since we delete as we test
//
//    }
//
//  } // end if can search and delete
//
//
//  //// Drill down to childrens if tag name is different than inTagName
//  if (inTagName.compare(tagName) != 0)
//  {
//    int nChilds = node.nChildNode();
//    for (int i1 = 0; i1 < nChilds; ++i1)
//    {
//      IXMLNode cNode = node.getChildNode(i1);
//      Utils::xml_delete_attribute(cNode, inSetAttributes, inTagName);
//    }
//  } // end drill to element that their tag is different than searched tag.
//
//
//
//}

/* *************************************************************** */

double rwmeter::Utils::getRandomRealNumber(double inMin, double inMax)
{
  // Seed with a real random value, if available
  std::random_device rd;
  std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
  std::uniform_real_distribution<> dis(inMin, inMax);

  return dis(gen);
}

/* *************************************************************** */

//bool rwmeter::Utils::xml_copy_attributes_from_one_node_to_the_other(IXMLNode & sourceNode, IXMLNode & targetNode)
//{
//  if (sourceNode.isEmpty() || targetNode.isEmpty())
//    return false;
//
//  int nAttrib = sourceNode.nAttribute();
//  for (int i1 = 0; i1 < nAttrib; ++i1)
//  {
//    IXMLAttr attribData = sourceNode.getAttribute(i1);
//    targetNode.addAttribute(attribData.sName, attribData.sValue);
//  }
//
//
//  return true;
//}


/* *************************************************************** */
std::string rwmeter::Utils::convert_string_to_24_min_numbers(std::string inTimeIn24Hfortmat, int & outHour, int &outMinutes, int& outCycles)
{
  outCycles = -1; // -1 means that plugin will have to calculate by itself

  std::string err;
  err.clear();

  // split inTimeIn24Hfortmat
  std::vector<std::string> vecTimeSplit = Utils::splitStringToNumbers<string>(inTimeIn24Hfortmat, ":");
  int nVals = (int)vecTimeSplit.size();
  std::string hours_s, min_s, cycles_s;
  if (nVals > 0)
  {
    hours_s = vecTimeSplit.front(); // vec[0]
    if (!hours_s.empty())
      outHour = Utils::stringToNumber<int>(hours_s);
  }
    
  if (nVals > 1)
  {
    min_s = vecTimeSplit.at(1); // vec[1]
    if (!min_s.empty())
      outMinutes = Utils::stringToNumber<int>(min_s);
  }

  if (nVals > 2)
  {
    cycles_s = vecTimeSplit.at(2);
    if (!cycles_s.empty())
      outCycles = Utils::stringToNumber<int>(cycles_s);
  }
    
  if (hours_s.empty())
    err = "Hours provided is not in 24H format, please fix. Current format: " + inTimeIn24Hfortmat + ". Supported format: \"[h24]:[min]:[cycles]\" ";


  return err;
}
/* *************************************************************** */




