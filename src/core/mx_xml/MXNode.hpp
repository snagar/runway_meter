#pragma once
#ifndef MXNODE_H_
#define MXNODE_H_

#include "../../data/mxProperties.hpp"

using namespace missionx;
using namespace mxconst;

namespace missionx
{
  // Node holds NAME of the element
  // Also holds all its attributes in format: attrib_name
  class MXNode : public missionx::mxProperties
  {
  public:
    MXNode()
    {
      attributeTypes.clear();
      id = -1; // not set
      root_element_id = -1; // not set
      parent_element_id = -1; // not set

    }

    ~MXNode()
    {
    }


    std::map <std::string, missionx::mx_property_type> attributeTypes;
    std::map <std::string, std::string> attributes;
    int id;
    int root_element_id;
    int parent_element_id;
  };

}

#endif // !ELEMENTCLASSTEMPLATE_H_

