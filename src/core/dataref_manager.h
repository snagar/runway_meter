#ifndef DATAREF_MANAGER_H_
#define DATAREF_MANAGER_H_

#include "dataref_const.h"
#include "Utils.h"
#include "../logic/dataref_param.h"
#include "coordinate/Point.hpp"
#include "../io/Log.hpp"

using namespace missionx;
using namespace mxconst;

namespace missionx
{

class dataref_manager
{
private:
  static Point planePoint; // holds plane coordinate location
  static void storePlanePoint(); // flc

public:
  //static dataref_const drefConst;

  dataref_manager();
  virtual
  ~dataref_manager();

 
  static XPLMDataTypeID  dataRefType;
  static XPLMDataRef getDataRef (std::string inDrefName); // v2.1.0
  static void    writeToDataRef(std::string full_name, double inValue);

  // get dataref value Template (for int, float, double only values )
  template <class N>
  static N  getDataRefValue ( std::string full_name )
  {
    XPLMDataRef data_id;
    XPLMDataTypeID type_id;

    data_id = XPLMFindDataRef ( full_name.c_str() ) ;
    if ( data_id ) // if exists
    {
      type_id = XPLMGetDataRefTypes ( data_id );
      switch ( type_id )
      {
        case xplmType_Int:
          {
            return (N)XPLMGetDatai( data_id );
          }
          break;
        case xplmType_Float:
          {
            return (N)XPLMGetDataf( data_id );
          }
          break;
          case ( xplmType_Float | xplmType_Double ):
          {
            return (N)XPLMGetDatad( data_id );
          }
          break;
          default:
            {
              Log::logMsg("Can't get DataRef " + full_name );
            }
          break;
        }// end switch
      } // end if

    return 0;
  } // end template



  // special members
  static bool isSimPause();
  static bool isPlaneOnGround();

  static double getLat ();
  static double getLong();
  static double getElevation ();
  static float  getHeadingPsi(); //v3.0.217.3 for <start_location> in random implementation
  static float  getTotalRunningTimeSec(); // time in seconds from the start of the sim. can be used in Timers

  static int getLocalDateDays();
  static float getLocalTimeSec();

  static void flc(); // every loop back fetch information once to use in all shared.dref->xx code; example: read pause pointState.
  static void init();

  static void setQuaternion(float w, float x, float y, float z);


  // members
  static Point getPlanePointLocation();
  static Point getCurrentPlanePointLocation(bool inStoreLocation = false);


  //static void    setDataRefI ( dataref_param* inRef, int inValue );
  //static void    setDataRefF ( dataref_param* inRef, float inValue );
  //static void    setDataRefD ( dataref_param* inRef, double inValue );
  //static int     getDataRefI (dataref_param* inRef);
  //static float   getDataRefF ( dataref_param* inRef );
  //static double  getDataRefD ( dataref_param* inRef );
  //static std::string getDataRefValueAsDouble ( dataref_param* inRef, double *outResult);

};

}

#endif 
